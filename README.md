## Change the following in "web.config" file
============================================

<appSettings>

	<add key="apiDataPath" value="{{change the path here}}\RSWP_MVC\RSWP_UI\apiData\" />
	
</appSettings>

============================================

<connectionStrings>

	<add name="RSWPConn" connectionString="Data Source={{change data source}}; Integrated Security=false;
		Initial Catalog= RS_DB; uid={{change user id}}; Password={{change password}}; " providerName="System.Data.SqlClient" />

	<add name="RS_DBEntities" 
		connectionString="metadata=res://*/Models.RSDBModel.csdl|res://*/Models.RSDBModel.ssdl|res://*/Models.RSDBModel.msl;
		provider=System.Data.SqlClient;provider connection string=&quot;
		data source={{change data source}};initial catalog=RS_DB;integrated security=False;
		user id={{change user id}};password={{change password}};MultipleActiveResultSets=True;App=EntityFramework&quot;" 
		providerName="System.Data.EntityClient" />

</connectionStrings>

============================================
