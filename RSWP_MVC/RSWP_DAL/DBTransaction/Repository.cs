﻿using RSWP_DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DAL.DBTransaction
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private RSWPEntities context;

        private DbSet<T> dbSet;
        public Repository()
        {
            context = new RSWPEntities();
            dbSet = context.Set<T>();
        }

        public IEnumerable<T> GetAll(ref Exception errMsg)
        {
            try
            {
                errMsg = null;
                return dbSet.ToList();
            }
            catch (Exception ex)
            {
                errMsg = ex;
                return null;
            }
        }


        public T GetById(object Id, ref Exception errMsg)
        {
            try
            {
                errMsg = null;
                return dbSet.Find(Id);

            }
            catch (Exception ex)
            {
                errMsg = ex;
                return null;
            }
            
        }


        public bool Delete(object Id, ref Exception errMsg)
        {
            using (DbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    T entityToDelete = dbSet.Find(Id);
                    Delete(entityToDelete);
                    transaction.Commit();
                    errMsg = null;
                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    errMsg = ex;
                    return false;
                }
            }
                
            
        }

        public void Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
                dbSet.Remove(entityToDelete);
            }
        }

        public T Insert(T obj, ref Exception errMsg)
        {
            using (DbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    dbSet.Add(obj);
                    context.SaveChanges();
                    transaction.Commit();
                    errMsg = null;
                    return obj;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    errMsg = ex;
                    return null;
                }
            }
                
        }

        public T Update(T obj, ref Exception errMsg)
        {
            using (DbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    dbSet.Attach(obj);
                    context.Entry(obj).State = EntityState.Modified;
                    context.SaveChanges();
                    transaction.Commit();
                    errMsg = null;
                    return obj;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    errMsg = ex;
                    return null;
                }
            }
            
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }


        
    }
}
