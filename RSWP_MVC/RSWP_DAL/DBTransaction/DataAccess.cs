﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using RSWP_DOM.Common;

namespace RSWP_DAL.DBTransaction
{
    /// <summary>
    /// Class for responsible all database operations
    /// </summary>
    public class DataAccess
    {
        private string strProcName;
        private static string ConnSql = ConfigurationManager.ConnectionStrings["RSWPConn"].ConnectionString;
        private SqlConnection objConn = new SqlConnection(ConnSql);
        private SqlCommand objCmd;
        private SqlDataAdapter objDataAdapter;
        private SqlTransaction objTrans;



        /// <summary>
        /// Controller for database connection open and closed
        /// </summary>
        private void Connect()
        {
            try
            {
                objConn.Open();
            }
            catch
            {
                Disconnect();
            }
        }


        private void Disconnect()
        {
            try
            {
                if (objConn != null)
                {
                    if (objConn.State.ToString() == "Open")
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            catch
            {
                objConn.Close();
                objConn.Dispose();
            }
        }


        /// <summary>
        /// Return store procedures name
        /// </summary>
        /// <param name="strMapValue"> Call method name</param>
        /// <returns></returns>
        private string GetProcName(string strMapValue)
        {
            if (strMapValue == "GetQuestionsGeneral")
                return "spGetQuestionsGeneral";
            else if (strMapValue == "Usp_GetQuestionsChart")
                return "Usp_GetQuestionsChart";
            else if (strMapValue == "GetQuestionsOtherLinks")
                return "spGetQuestionsOtherLinks";
            else if (strMapValue == "Usp_GetQuestionsCalender")
                return "Usp_GetQuestionsCalender";
            else if (strMapValue == "GetQuestionsArchivesGroupingOfMinistries")
                return "spGetQuestionsArchivesGroupingOfMinistries";
            else if (strMapValue == "GetMinistryNodalOfficers")
                return "spGetMinistryNodalOfficers";
            else if (strMapValue == "MinistryWiseNodalOfficersPDF")
                return "spMinistryWiseNodalOfficersPDF";
            else if (strMapValue == "GetRecruitmentRules")
                return "spGetRecruitmentRules";
            else if (strMapValue == "GetForthcomingExamination")
                return "spGetForthcomingExamination";
            else if (strMapValue == "GetSchemeOfExamination")
                return "spGetSchemeOfExamination";
            else if (strMapValue == "GetSyllabusOfExamination")
                return "spGetSyllabusOfExamination";
            else if (strMapValue == "GetLatestAdvertisments")
                return "spGetLatestAdvertisments";
            else if (strMapValue == "GetStatusOfApplication")
                return "spGetStatusOfApplication";
            else if (strMapValue == "GetCareers")
                return "spGetCareers";
            else if (strMapValue == "GetAdmitCards")
                return "spGetAdmitCards";
            else if (strMapValue == "GetExaminationSchedule")
                return "spGetExaminationSchedule";
            else if (strMapValue == "GetCandidatesInstruction")
                return "spGetCandidatesInstruction";
            else if (strMapValue == "GetResult")
                return "spGetResult";
            else if (strMapValue == "GetDemandGrants")
                return "spGetDemandGrants";
            else if (strMapValue == "GetTelephoneDirectories")
                return "spGetTelephoneDirectories";
            else if (strMapValue == "GetSenctionedPosition")
                return "spGetSenctionedPosition";
            else if (strMapValue == "GetManualOfficeProcedure")
                return "spGetManualOfficeProcedure";
            else if (strMapValue == "GetAnnualAdministrativeReports")
                return "spGetAnnualAdministrativeReports";
            else if (strMapValue == "GetAnnualAdministrativeReports")
                return "spGetAnnualAdministrativeReports";
            else
                return "";
        }

        /// <summary>
        /// Method for get single value from database
        /// </summary>
        public bool FetchSingleValue(string ProcedureMapName, Hashtable hashParams, ref string ErrMsg, ref string strResult)
        {
            strProcName = GetProcName(ProcedureMapName);
            Connect();
            if (objConn == null)
            {
                ErrMsg = "Error in Connection";
                return false;
            }

            try
            {
                objCmd = new SqlCommand(strProcName, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandTimeout = 0;
                foreach (DictionaryEntry enTry in hashParams)
                    objCmd.Parameters.Add(new SqlParameter(enTry.Key.ToString(), enTry.Value.ToString()));
                object obj = objCmd.ExecuteScalar();
                objConn.Close();
                ErrMsg = "SUCCESS";
                strResult = Convert.ToString(obj);
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Source + " : " + ex.Message;
                return false;
            }
            finally
            {
                Disconnect();
            }
        }

        /// <summary>
        /// Method for get single table from database
        /// </summary>
        public bool FetchValues_Proc(string ProcedureMapName, Hashtable hashParams, ref DataTable dtTable, ref string ErrMsg)
        {
            strProcName = GetProcName(ProcedureMapName);
            objDataAdapter = new SqlDataAdapter();
            dtTable = new DataTable();
            Connect();
            if (objConn == null)
            {
                ErrMsg = "Error in Connection";
                return false;
            }

            try
            {
                objCmd = new SqlCommand(strProcName, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;

                foreach (DictionaryEntry enTry in hashParams)
                    objCmd.Parameters.Add(new SqlParameter(enTry.Key.ToString(), enTry.Value.ToString()));
                objDataAdapter.SelectCommand = objCmd;
                objCmd.CommandTimeout = 0;
                objDataAdapter.Fill(dtTable);
                ErrMsg = "SUCCESS";
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Source + " : " + ex.Message;
                return false;
            }
            finally
            {
                Disconnect();
            }
        }


        /// <summary>
        /// Method for get multiple table from database
        /// </summary>
        public bool FetchMultipleValues_Proc(string ProcedureName, Hashtable hashParams, ref DataSet ds, ref string ErrMsg)
        {
            strProcName = GetProcName(ProcedureName);
            objDataAdapter = new SqlDataAdapter();
            Connect();
            if (objConn == null)
            {
                ErrMsg = "Error in Connection";
                return false;
            }

            try
            {
                objCmd = new SqlCommand(strProcName, objConn);
                objCmd.CommandType = CommandType.StoredProcedure;
                foreach (DictionaryEntry enTry in hashParams)
                    objCmd.Parameters.Add(new SqlParameter(enTry.Key.ToString(), enTry.Value.ToString()));
                objDataAdapter.SelectCommand = objCmd;
                objCmd.CommandTimeout = 0;
                objDataAdapter.Fill(ds);
                ErrMsg = "SUCCESS";
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                return false;
            }
            finally
            {
                Disconnect();
            }
        }

        private string GetProcedureFromConfig(string procedureName)
        {
            if (!string.IsNullOrEmpty(procedureName))
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[procedureName]))
                {
                    return ConfigurationManager.AppSettings[procedureName];
                }  
            }
            return ConfigurationManager.AppSettings["DefaultProc"];
        }




        /// <summary>
        /// Method for save data into database
        /// </summary>
        public bool SaveData_Proc(string ProcedureMapName, Hashtable hashParams, ref string ErrMsg)
        {
            strProcName = GetProcName(ProcedureMapName);
            Connect();
            objTrans = objConn.BeginTransaction();

            if (objConn == null)
            {
                ErrMsg = "Error in Connection";
                return false;
            }

            try
            {
                SqlCommand cmdSave = new SqlCommand(strProcName, objConn, objTrans);
                cmdSave.CommandType = CommandType.StoredProcedure;

                foreach (DictionaryEntry enTry in hashParams)
                    // cmdSave.Parameters.Add(New SqlParameter(CType(enTry.Key,enTry.Value, String)))
                    cmdSave.Parameters.Add(new SqlParameter(enTry.Key.ToString(), enTry.Value.ToString()));
                cmdSave.CommandTimeout = 0;
                cmdSave.ExecuteNonQuery();
                objTrans.Commit();
                ErrMsg = "SUCCESS";
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.StackTrace.ToString();
                objTrans.Rollback();
                return false;
            }
            finally
            {
                Disconnect();
            }
        }


        /// <summary>
        /// Method for save data into database and return ID
        /// </summary>
        public bool SaveData_And_ReturnIdentity_Proc(string ProcedureMapName, Hashtable hashParams, ref string ErrMsg, ref int IdentityColValue)
        {
            strProcName = GetProcName(ProcedureMapName);
            Connect();
            objTrans = objConn.BeginTransaction();

            if (objConn == null)
            {
                ErrMsg = "Error in Connection";
                return false;
            }

            try
            {
                SqlCommand cmdSave = new SqlCommand(strProcName, objConn, objTrans);
                cmdSave.CommandType = CommandType.StoredProcedure;

                foreach (DictionaryEntry enTry in hashParams)
                    // cmdSave.Parameters.Add(New SqlParameter(CType(enTry.Key,enTry.Value, String)))
                    cmdSave.Parameters.Add(new SqlParameter(enTry.Key.ToString(), enTry.Value.ToString()));
                cmdSave.CommandTimeout = 0;
                cmdSave.ExecuteScalar();
                cmdSave.CommandType = CommandType.Text;
                cmdSave.CommandText = "Select @@Identity";
                IdentityColValue = Convert.ToInt32(cmdSave.ExecuteScalar());
                objTrans.Commit();
                ErrMsg = "SUCCESS";
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.StackTrace.ToString();
                objTrans.Rollback();
                return false;
            }
            finally
            {
                Disconnect();
            }
        }


    }
}




