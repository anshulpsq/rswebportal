﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DAL.DBTransaction
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll(ref Exception errMsg);
        T GetById(object Id, ref Exception errMsg);
        T Insert(T obj, ref Exception errMsg);
        bool Delete(object Id, ref Exception errMsg);
        T Update(T obj, ref Exception errMsg);
    }
}
