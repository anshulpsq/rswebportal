﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class MinistryNodalOfficers
    {
        public int Id { get; set; }
        public string Ministry { get; set; }
        public string NameOfOfficer { get; set; }
        public string Designation { get; set; }
        public string OfficeTelephoneNo { get; set; }
        public string ResidenceTelephoneNo { get; set; }
        public string Email { get; set; }
        public string OfficeAddress { get; set; }
    }
}
