﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class QuestionDocumentsToShow
    {
        public string Description { get; set; }
        public string FileUrl { get; set; }
        public string FileSize { get; set; }
    }
}
