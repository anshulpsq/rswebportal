﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class SessionsMasters
    {
        public string Id { get; set; }
        public string sessionNo { get; set; }
        public string CrteatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Description { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
