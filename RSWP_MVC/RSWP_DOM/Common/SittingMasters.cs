﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class SittingMasters
    {
        public string Id { get; set; }
        public string sessionNo { get; set; }
        public string CrteatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string SittingDate { get; set; }
        public string LastEditedBy { get; set; }
        public string LastEditedOn { get; set; }
    }
}
