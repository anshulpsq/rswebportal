﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class Documents
    {
        public string Id { get; set; }
        public string MenuId { get; set; }
        public string Section { get; set; }
        public string SubSection { get; set; }
        public string Ministry { get; set; }
        public string Metadata { get; set; }
        public string session { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string FileLocation { get; set; }
        public string FileUrl { get; set; }
        public string FileVersion { get; set; }
        public string FileSequence { get; set; }
        public string FileStatus { get; set; }
        public string Language { get; set; }
        public bool IsArchived { get; set; }
        public string IsDownloadable { get; set; }
        public string IsPlayable { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string PublishedOn { get; set; }
        public string PublishedBy { get; set; }
        public string ApprovedOn { get; set; }
        public string ApprovedBy { get; set; }
        public bool isApproved { get; set; }
        public bool isPublished { get; set; }
        public string FileSize { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string Tiltle { get; set; }
        public string selectedDate { get; set; }
        public string PublishDate { get; set; }
        public string Time { get; set; }
        public string ArchivedBy { get; set; }
        public string ArchivedOn { get; set; }
    }
}
