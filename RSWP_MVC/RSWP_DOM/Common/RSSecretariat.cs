﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class RSSecretariat
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string FinancialYear { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string UploadDate { get; set; }
        public string PublishDate { get; set; }
        public string Date { get; set; }
        public string Year { get; set; }
        public bool IsPublished { get; set; }
        public string IsPublishedDate { get; set; }
        public bool IsArchived { get; set; }
        public string IsArchivedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public string UpdateDate { get; set; }
    }
}
