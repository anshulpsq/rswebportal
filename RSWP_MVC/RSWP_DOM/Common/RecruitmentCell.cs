﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_DOM.Common
{
    public class RecruitmentCell
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string ExamDescription { get; set; }
        public string ExamDescription2 { get; set; }
        public int NoofPosts { get; set; }
        public string LastDateofApplication { get; set; }
        public string AdvertisementsNo { get; set; }
        public string URL { get; set; }
        public string UploadDate { get; set; }
        public bool IsPublished { get; set; }
        public string IsPublishedDate { get; set; }
        public bool IsArchived { get; set; }
        public string IsArchivedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public string UpdateDate { get; set; }

        public string Flag { get; set; }
    }
}
