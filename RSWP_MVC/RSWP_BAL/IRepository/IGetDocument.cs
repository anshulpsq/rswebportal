﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSWP_DAL;
using RSWP_DOM.Common;

namespace RSWP_BAL.IRepository
{
    interface IGetDocument
    {
        bool GetGeneralInformation(Documents documents,ref IEnumerable<Document> list, ref Exception errMsg);
    }
}
