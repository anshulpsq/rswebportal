﻿using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.IRepository
{
    interface IGetQuestionsDocument
    {
        bool GetQuestionsGeneral(ref DataSet dsGetQuestionsGeneral, Documents documents, ref string ErrorMsg);

        bool GetQuestionsOtherLinks(ref DataSet dsGetQuestionsOtherLinks, Documents documents, ref string ErrorMsg);

        bool GetQuestionsArchivesGroupingOfMinistries(ref DataSet dsGetQuestionsArchivesGroupingOfMinistries, Documents documents, ref string ErrorMsg);

        bool GetMinistryNodalOfficers(ref DataSet dsGetMinistryNodalOfficers, ref string ErrorMsg);

        bool MinistryWiseNodalOfficersPDF(ref DataSet dsMinistryWiseNodalOfficersPDF, Documents documents, ref string ErrorMsg);

    }
}
