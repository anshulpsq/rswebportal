﻿using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.IRepository
{
    interface IGetRSSecretariat
    {
        bool GetDemandGrants(ref DataSet dsDemandGrants, RSSecretariat rssecretariat, ref string ErrorMsg);
        bool GetTelephoneDirectories(ref DataSet dsTelephoneDirectories, RSSecretariat rssecretariat, ref string ErrorMsg);

        bool GetSenctionedPosition(ref DataSet dsSenctionedPosition, RSSecretariat rssecretariat, ref string ErrorMsg);

        bool GetManualOfficeProcedure(ref DataSet dsManualOfficeProcedure, RSSecretariat rssecretariat, ref string ErrorMsg);

        bool GetAnnualAdministrativeReports(ref DataSet dsAnnualAdministrativeReports, RSSecretariat rssecretariat, ref string ErrorMsg);
    }
}
