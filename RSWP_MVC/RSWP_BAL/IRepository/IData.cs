﻿using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.IRepository
{
    public interface IData
    {
        DataSet GetData(Documents documents, ref string ErrorMsg);



    }
}
