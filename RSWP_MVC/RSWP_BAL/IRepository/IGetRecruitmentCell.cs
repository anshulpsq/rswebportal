﻿using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.IRepository
{
    interface IGetRecruitmentCell
    {
        bool GetRecruitmentRules(ref DataSet dsGetRecruitmentRules, RecruitmentCell recruitmentCell, ref string ErrorMsg);
        bool GetForthcomingExamination(ref DataSet dsGetForthcomingExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetSchemeOfExamination(ref DataSet dsGetSchemeOfExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg);
        bool GetSyllabusOfExamination(ref DataSet dsGetSyllabusOfExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg);
        bool GetLatestAdvertisments(ref DataSet dsGetLatestAdvertisments, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetStatusOfApplication(ref DataSet dsGetStatusOfApplication, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetCareers(ref DataSet dsGetCareers, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetAdmitCards(ref DataSet dsGetAdmitCards, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetExaminationSchedule(ref DataSet dsExaminationSchedule, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetCandidatesInstruction(ref DataSet dsCandidatesInstruction, RecruitmentCell recruitmentCell, ref string ErrorMsg);

        bool GetResult(ref DataSet dsResult, RecruitmentCell recruitmentCell, ref string ErrorMsg);
    }
}
