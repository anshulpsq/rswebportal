﻿using RSWP_BAL.IRepository;
using RSWP_DAL.DBTransaction;
using RSWP_DOM.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.Repository
{
    /// <summary>
    /// Responsible for Rajya Sabha Secretariat all operations
    /// </summary>
    public class GetRSSecretariat : IGetRSSecretariat
    {
        Hashtable hsParam = new Hashtable();
        DataAccess objDataAccess = new DataAccess();

        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Demand Grants
        /// </summary>
        public bool GetDemandGrants(ref DataSet dsDemandGrants, RSSecretariat rssecretariat, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("IsPublished", rssecretariat.IsPublished);
            hsParam.Add("IsArchived", rssecretariat.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetDemandGrants", hsParam, ref dsDemandGrants, ref ErrorMsg))
                return true;
            else
                return false;
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Telephone Directories
        /// </summary>
        public bool GetTelephoneDirectories(ref DataSet dsTelephoneDirectories, RSSecretariat rssecretariat, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Section", rssecretariat.Type);
            hsParam.Add("SubSection", rssecretariat.SubType);
            hsParam.Add("IsPublished", rssecretariat.IsPublished);
            hsParam.Add("IsArchived", rssecretariat.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetTelephoneDirectories", hsParam, ref dsTelephoneDirectories, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Senctioned Position
        /// </summary>
        public bool GetSenctionedPosition(ref DataSet dsSenctionedPosition, RSSecretariat rssecretariat, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("IsPublished", rssecretariat.IsPublished);
            hsParam.Add("IsArchived", rssecretariat.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetSenctionedPosition", hsParam, ref dsSenctionedPosition, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Manual Office Procedure
        /// </summary>
        public bool GetManualOfficeProcedure(ref DataSet dsManualOfficeProcedure, RSSecretariat rssecretariat, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("IsPublished", rssecretariat.IsPublished);
            hsParam.Add("IsArchived", rssecretariat.IsArchived);
            hsParam.Add("Type", rssecretariat.Type);
            if (objDataAccess.FetchMultipleValues_Proc("GetManualOfficeProcedure", hsParam, ref dsManualOfficeProcedure, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Annual Administrative Reports
        /// </summary>
        public bool GetAnnualAdministrativeReports(ref DataSet dsAnnualAdministrativeReports, RSSecretariat rssecretariat, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("IsPublished", rssecretariat.IsPublished);
            hsParam.Add("IsArchived", rssecretariat.IsArchived);
            hsParam.Add("Type", rssecretariat.Type);
            if (objDataAccess.FetchMultipleValues_Proc("GetAnnualAdministrativeReports", hsParam, ref dsAnnualAdministrativeReports, ref ErrorMsg))
                return true;
            else
                return false;
        }
    }
}
