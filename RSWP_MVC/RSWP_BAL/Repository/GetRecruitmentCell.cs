﻿using RSWP_BAL.IRepository;
using RSWP_DAL.DBTransaction;
using RSWP_DOM.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.Repository
{
    /// <summary>
    /// Responsible for Recruitment Cells all operations
    /// </summary>
    public class GetRecruitmentCell : IGetRecruitmentCell
    {
        Hashtable hsParam = new Hashtable();
        DataAccess objDataAccess = new DataAccess();

        /// <summary>
        /// Responsible for Recruitment Rules operations
        /// </summary>
        public bool GetRecruitmentRules(ref DataSet dsGetRecruitmentRules, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetRecruitmentRules", hsParam, ref dsGetRecruitmentRules, ref ErrorMsg))
                return true;
            else
                return false;
        }


        /// <summary>
        /// Responsible for Forthcoming Examination operations
        /// </summary>
        public bool GetForthcomingExamination(ref DataSet dsGetForthcomingExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("SubType", recruitmentCell.SubType);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetForthcomingExamination", hsParam, ref dsGetForthcomingExamination, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Scheme of Examination operations
        /// </summary>
        public bool GetSchemeOfExamination(ref DataSet dsGetSchemeOfExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("SubType", recruitmentCell.SubType);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetSchemeOfExamination", hsParam, ref dsGetSchemeOfExamination, ref ErrorMsg))
                return true;
            else
                return false;
        }


        /// <summary>
        /// Responsible for Get Syllabus of Examination operations
        /// </summary>
        public bool GetSyllabusOfExamination(ref DataSet dsGetSyllabusOfExamination, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("SubType", recruitmentCell.SubType);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetSyllabusOfExamination", hsParam, ref dsGetSyllabusOfExamination, ref ErrorMsg))
                return true;
            else
                return false;
        }


        /// <summary>
        /// Responsible for Get Latest Advertisments operations
        /// </summary>
        public bool GetLatestAdvertisments(ref DataSet dsGetLatestAdvertisments, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetLatestAdvertisments", hsParam, ref dsGetLatestAdvertisments, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Status Of Application operations
        /// </summary>
        public bool GetStatusOfApplication(ref DataSet dsGetStatusOfApplication, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetStatusOfApplication", hsParam, ref dsGetStatusOfApplication, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Careers operations
        /// </summary>
        public bool GetCareers(ref DataSet dsGetCareers, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetCareers", hsParam, ref dsGetCareers, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Admit Cards operations
        /// </summary>
        public bool GetAdmitCards(ref DataSet dsGetAdmitCards, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetAdmitCards", hsParam, ref dsGetAdmitCards, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Examination Schedule operations
        /// </summary>
        public bool GetExaminationSchedule(ref DataSet dsExaminationSchedule, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetExaminationSchedule", hsParam, ref dsExaminationSchedule, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Candidates Instruction operations
        /// </summary>
        public bool GetCandidatesInstruction(ref DataSet dsCandidatesInstruction, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetCandidatesInstruction", hsParam, ref dsCandidatesInstruction, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for Get Result operations
        /// </summary>
        public bool GetResult(ref DataSet dsResult, RecruitmentCell recruitmentCell, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("Type", recruitmentCell.Type);
            hsParam.Add("IsPublished", recruitmentCell.IsPublished);
            hsParam.Add("IsArchived", recruitmentCell.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("GetResult", hsParam, ref dsResult, ref ErrorMsg))
                return true;
            else
                return false;
        }
    }
}
