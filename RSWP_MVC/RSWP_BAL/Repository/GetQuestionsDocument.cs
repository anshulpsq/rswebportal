﻿using RSWP_BAL.IRepository;
using RSWP_DAL.DBTransaction;
using RSWP_DOM.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.Repository
{
    public class GetQuestionsDocument : IGetQuestionsDocument
    {
        Hashtable hsParam = new Hashtable();
        DataAccess objDataAccess = new DataAccess();


        /// <summary>
        /// Responsible for Get Ministry Nodal Officers data from database
        /// </summary>
        public bool GetMinistryNodalOfficers(ref DataSet dsGetMinistryNodalOfficers, ref string ErrorMsg)
        {
            hsParam.Clear();
            if (objDataAccess.FetchMultipleValues_Proc("GetMinistryNodalOfficers", hsParam, ref dsGetMinistryNodalOfficers, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for get general questions Archives Grouping of Ministriesdata from database
        /// </summary>
        public bool GetQuestionsArchivesGroupingOfMinistries(ref DataSet dsGetQuestionsArchivesGroupingOfMinistries, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("Type", documents.Type);
            hsParam.Add("isPublished", documents.isPublished);
            hsParam.Add("IsArchived", documents.IsArchived);
            hsParam.Add("Language", documents.Language);
            if (objDataAccess.FetchMultipleValues_Proc("GetQuestionsArchivesGroupingOfMinistries", hsParam, ref dsGetQuestionsArchivesGroupingOfMinistries, ref ErrorMsg))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Responsible for get general questions data from database
        /// </summary>
        public bool GetQuestionsGeneral(ref DataSet dsGetQuestionsGeneral, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("Type", documents.Type);
            hsParam.Add("isPublished", documents.isPublished);
            hsParam.Add("IsArchived", documents.IsArchived);
            hsParam.Add("Language", documents.Language);
            if (objDataAccess.FetchMultipleValues_Proc("GetQuestionsGeneral", hsParam, ref dsGetQuestionsGeneral, ref ErrorMsg))
                return true;
            else
                return false;
        }
        public bool GetQuestionsCalender(ref DataSet dsGetQuestionsCalender, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("Language", documents.Language);
            if (objDataAccess.FetchMultipleValues_Proc("Usp_GetQuestionsCalender", hsParam, ref dsGetQuestionsCalender, ref ErrorMsg))
                return true;
            else
                return false;
        }
        public bool GetQuestionsChart(ref DataSet dsGetQuestionsChart, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("Language", documents.Language);
            if (objDataAccess.FetchMultipleValues_Proc("Usp_GetQuestionsChart", hsParam, ref dsGetQuestionsChart, ref ErrorMsg))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Responsible for get other links questions data from database
        /// </summary>
        public bool GetQuestionsOtherLinks(ref DataSet dsGetQuestionsOtherLinks, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("isPublished", documents.isPublished);
            hsParam.Add("IsArchived", documents.IsArchived);
            hsParam.Add("Language", documents.Language);
            if (objDataAccess.FetchMultipleValues_Proc("GetQuestionsOtherLinks", hsParam, ref dsGetQuestionsOtherLinks, ref ErrorMsg))
                return true;
            else
                return false;
        }


        /// <summary>
        /// Responsible for Ministry Nodal Officers pdf file from database
        /// </summary>
        public bool MinistryWiseNodalOfficersPDF(ref DataSet dsMinistryWiseNodalOfficersPDF, Documents documents, ref string ErrorMsg)
        {
            hsParam.Clear();
            hsParam.Add("section", documents.Section);
            hsParam.Add("subsection", documents.SubSection);
            hsParam.Add("isPublished", documents.isPublished);
            hsParam.Add("IsArchived", documents.IsArchived);
            if (objDataAccess.FetchMultipleValues_Proc("MinistryWiseNodalOfficersPDF", hsParam, ref dsMinistryWiseNodalOfficersPDF, ref ErrorMsg))
                return true;
            else
                return false;
        }
    }
}
