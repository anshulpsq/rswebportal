﻿using RSWP_BAL.IRepository;
using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSWP_DAL.DBTransaction;
using RSWP_DAL;
using System.Collections;
using System.Data;

namespace RSWP_BAL.Repository
{
    public class GetDocument : IGetDocument
    {
        Hashtable hsParam = new Hashtable();
        DataAccess objDataAccess = new DataAccess();
        private IRepository<Document> _Repository = null;
        public GetDocument()
        {
            this._Repository = new Repository<Document>();
        }
        public bool GetGeneralInformation(Documents documents, ref IEnumerable<Document> list,ref Exception errMsg)
        {
            list = _Repository.GetAll(ref errMsg);
            if (errMsg == null)
                return true;
            else
                return false;
        }

        public bool GetList(ref DataSet dsGetList, ref string ErrorMsg)
        {
            hsParam.Clear();
            //hsParam.Add("feature_name", feature_name);
            if (objDataAccess.FetchMultipleValues_Proc("GetList", hsParam, ref dsGetList, ref ErrorMsg))
                return true;
            else
                return false;
        }
    }
}

