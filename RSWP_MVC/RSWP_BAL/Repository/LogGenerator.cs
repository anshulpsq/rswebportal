﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSWP_BAL.Repository
{
    public static class LogGenerator
    {
        
        //the log file date.
        private const string LOG_DATE = "DATE:    ";
        //the log file message.
        private const string LOG_MESSAGE = "MESSAGE: ";
        //the log file source.
        private const string LOG_SOURCE = "SOURCE:  ";
        //the log file target.
        private const string LOG_TARGET = "TARGET:  ";
        //the log file target.
        private const string LOG_STACK = "STACK:  ";
        //the log file line number.
        private const string LOG_LINE_NO = "Line Number : ";
        //the log file method name.
        private const string LOG_METHOD = "Method Name : ";
        //the log file inner exception.
        private const string LOG_INN_EXP = "Inner Exception=============";

        public static void GenerateLog(string exception, string methodName, string lineNumber)
        {
            string fileName = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["logPath"]) + DateTime.Now.ToString("MM-dd-yyyy").Replace("-", "") + "Log.txt";
            FileInfo objFileInfo = new FileInfo(fileName);

            if (objFileInfo.Exists)
            {
                objFileInfo.Delete();
            }
            using (StreamWriter sw = objFileInfo.CreateText())
            {
                sw.WriteLine("=============================================================");
                sw.WriteLine("==========================Exception==========================");
                sw.WriteLine("=============================================================");
                sw.WriteLine(LOG_DATE + DateTime.Now.ToString() + Environment.NewLine);
                sw.WriteLine(LOG_MESSAGE + exception + Environment.NewLine + Environment.NewLine);
                sw.WriteLine(LOG_METHOD + methodName);
                sw.WriteLine(LOG_LINE_NO + lineNumber);
            }
            
        }
    }
}
