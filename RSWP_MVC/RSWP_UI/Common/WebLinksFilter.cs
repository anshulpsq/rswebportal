﻿using RSWP_UI.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Common
{
    public class WebLinksFilter : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            RS_DBEntities db = new RS_DBEntities();

            List<tblRecruitmentCell> lstWebLinks = db.tblRecruitmentCells.Where(t => t.Type == "Web Links").ToList();            
            Dictionary<string, string> dictionaryWebLinks = new Dictionary<string, string>();

            Dictionary<string, string> dictionaryWebLinksImage = new Dictionary<string, string>();
            foreach (var item in lstWebLinks)
            {
                if (!dictionaryWebLinks.Keys.Contains(item.Title))
                    dictionaryWebLinks.Add(item.Title, item.URL);
                else
                    dictionaryWebLinks[item.Title] = item.URL;

                if (item.FilePath!=null)
                {
                    dictionaryWebLinksImage.Add(item.Title, item.FileName);
                }
            }
            HttpContext.Current.Session["WebLinks"] = dictionaryWebLinks;

            HttpContext.Current.Session["WebLinksImage"] = dictionaryWebLinksImage;

            base.OnActionExecuting(actionContext);
        }        
    }
}