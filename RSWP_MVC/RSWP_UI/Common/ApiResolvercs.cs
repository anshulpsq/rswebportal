﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
namespace RSWP_UI.Common
{
    public class ApiResolvercs
    {
        public void ReadApi()
        {

            Dictionary<string, string> dictionary = this.ApiList;

            foreach (string key in dictionary.Keys)
            {
                //HttpClient client = new HttpClient();
                //HttpResponseMessage response = await client.GetAsync(dictionary[key]);
                //response.EnsureSuccessStatusCode();
                //string responseBody = await response.Content.ReadAsStringAsync();

                WebRequest req = WebRequest.Create(dictionary[key]);

                var resp = req.GetResponse();

                var stream = resp.GetResponseStream();

                System.IO.StreamReader sr = new System.IO.StreamReader(stream);

                var responseBody = sr.ReadToEnd();
                string folderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/apiData/");
                ///string folderPath = System.Configuration.ConfigurationManager.AppSettings["apiDataPath"];
                string filepath = System.IO.Path.Combine(folderPath, key + ".js");
                //var x=Environment.CurrentDirectory;
                //string filepath = HttpContext.Current.Server.MapPath("~/apiData/" + key + ".js");
                if (System.IO.File.Exists(filepath))
                    System.IO.File.Delete(filepath);

                System.IO.File.WriteAllText(@filepath, "var " + key + "=" + responseBody);
            }
        }

        public Dictionary<string, string> ApiList
        {
            get
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                //result.Add("GetCurrentMember_All", "http://164.100.47.5/api_new/Memberweb/GetCurrentMember_All");
                result.Add("GetFormerMember_All", "http://164.100.47.5/api_new/Memberweb/GetFormerMember_All");
                //result.Add("GetMember_GeneralInfo", "http://164.100.47.5/api_new/Memberweb/GetMember_GeneralInfo");

                result.Add("getmemberall", "http://164.100.47.5/api_new/MemberGetData/getmemberall");
                result.Add("GetWomenMember", "http://164.100.47.5/api_new/MemberGetData/GetWomenMember");
                result.Add("GetCouncilMinister", "http://164.100.47.5/api_new/MemberGetData/GetCouncilMinister");
                result.Add("GetStateList", "http://164.100.47.5/api_new/MemberGetData/GetStateList");
                result.Add("GetMPTermWise01", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=1");
                result.Add("GetMPTermWise02", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=2");
                result.Add("GetMPTermWise03", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=3");
                result.Add("GetMPTermWise04", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=4");
                result.Add("GetMPTermWise05", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=5");
                result.Add("GetMPTermWise06", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=6");
                result.Add("GetMPTermWise07", "http://164.100.47.5/api_new/MemberGetData/GetMPTermWise?mp_term=7");
                return result;
            }
        }
    }
}