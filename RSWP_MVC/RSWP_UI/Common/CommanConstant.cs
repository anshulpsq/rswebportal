﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Common
{
    public class CommanConstant
    {
        // public const string urlContent = "http://164.100.47.5";
        public const string urlContent = "http://10.246.71.37";
        public const string TableOffice = "TableOffice";
        public const string Bulletin2 = "Bulletin2";
        public const string English = "English";
        public const string Hindi = "Hindi";
        public const string date = "date";
        //Satements
        public const string BillsPassedByTheHouseOfParliament = "Bills Passed by the House of Parliament";
        public const string BillsPendingAtTheEndOfSession = "Bills Pending at the End of Session";
        public const string GovernmentLegBusinessToBeTakenUp = "Government leg. Business to be Taken Up";
        public const string WeeklyProgressOfBills = "Weekly Progress of Bills";
        public const string StatuaryOrderLaid = "Statuary Order laid";
        //End

        public const string Lobby = "Lobby";
        public const string SessionJournal = "SessionJournal";


        public const string PleaseSelect = "Please Select";

        public const string en = "en";
        public const string hi = "hi";

        public const string Date = "Date";
        public const string Ministry = "Ministry";
        public const string ReportType = "ReportType";
        public const string Success = "Success";

        public const string BillOffice = "BillOffice";
        public const string LatestBills = "LatestBills";

        public const string Committee = "Committee";
        public const string Introduction = "Introduction";
        public const string SummaryOfWork = "SummaryOfWork";

        public const string Procedure = "Procedure";
        public const string ProcedureForSubmission = "ProcedureForSubmission";
        public const string HandBookForMembers = "HandBookForMembers";
        public const string PracticeAndProcedure = "PracticeAndProcedure";
        public const string GovernmentInstructions = "GovernmentInstructions";
        public const string PrivilegeDigest = "PrivilegeDigest";
        public const string RulingsAndObservation = "RulingsAndObservation";
        public const string RajyaSabhaAtWork = "RajyaSabhaAtWork";

        public const string Section_Legislative = "LegislativeSection";
        public const string SubSection_LegislativeRules = "LegislativeRules";
        public const string SubSection_LegislativeDirection = "LegislativeDirection";

        public const string Debates = "Debates";
        public const string OfficialDebatesDatewise = "OfficialDebatesDatewise";
        public const string Floor = "Floor";

        public const string VerbatimDebates = "VerbatimDebates";
        public const string Verbatim = "Verbatim";

        public const string Discussions = "Discussions";
        public const string CallingAttention = "Calling Attention";
        public const string ShortDurationDiscussion = "Short Duration Discussion";
        public const string MattersRaisedWithPermission_ZeroHour = "Matters Raised With Permission (Zero Hour)";
        public const string PrivateMembersResolution = "Private Members Resolution";
        public const string CA = "CA";
        public const string SDD = "SDD";
        public const string ZH = "ZH";
        public const string PMR = "PMR";

        public const string PaperLaidInRajyaSabha = "PaperLaidInRajyaSabha";
        public const string TableBusiness = "TableBusiness";
        public const string Questions = "Questions";
        public const string QuestionsList = "QuestionsList";
        public const string Starred = "Starred";
        public const string UnStarred = "UnStarred";

        public const string SynopsisUpload = "SynopsisUpload";
        public const string Current = "Current";
        public const string Previous = "Previous";
        public const string Next = "Next";

        public const string ListOfBusiness = "ListOfBusiness";
        public const string PaperToBeLaid = "PaperToBeLaid";
        public const string TableBulletin1 = "TableBulletin1";
        public const string Synopsis = "Synopsis";
        public const string UnCorrectedDebates = "UnCorrectedDebates";

        public const string StarredQuestionList = "StarredQuestionList";
        public const string UnStarredQuestionList = "UnStarredQuestionList";
        public const string Larrdis_Section = "LarrdisSection";
    }
}