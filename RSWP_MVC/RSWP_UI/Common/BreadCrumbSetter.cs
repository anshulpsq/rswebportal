﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Common
{
    public static class BreadCrumbSetter
    {
        static string BreadCrumbFile = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString(ConfigurationManager.AppSettings["BreadCrumbFile"]));
        public static List<SelectListItem> GetBreadCrumb(string jsonSuperKey,string jsonSubKey)
        {
            dynamic array = JsonConvert.DeserializeObject(File.ReadAllText(@BreadCrumbFile + "BreadCrumb.json"));
            List<SelectListItem> breadcrumb = new List<SelectListItem>();
            foreach (var item in array[jsonSuperKey][jsonSubKey])
            {
                var iteminfo = new SelectListItem() { Text = item.text, Value = item.value };
                breadcrumb.Add(iteminfo);
            }
            return breadcrumb;
        }
    }
}