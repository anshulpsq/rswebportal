﻿using RSWP_DOM.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.MemberViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace RSWP_UI.Common
{
    public class Helpers
    {
        RS_DBEntities dBEntities;

        public Helpers()
        {
            dBEntities = new RS_DBEntities();
        }
        public DocumentModelView GetDocument(Documents dt)
        {
            DocumentModelView odvm = new DocumentModelView();
            var d = dBEntities.vwDocuments.Where(m => m.InternalKey.ToString() == dt.MenuId && m.Language == dt.Language && m.IsArchived == dt.IsArchived);

          
                var document = d.OrderByDescending(x => x.FileUrl).FirstOrDefault();
                if (document != null)
                {

                    MapPublicProperties<vwDocument, DocumentModelView>(document, odvm);
                }
          
            return odvm;
        }


        internal static void MapPublicProperties<source, destination>(source src, destination dest)
        {
            var destinationProperties = dest.GetType().GetProperties();
            var sourceProperties = src.GetType().GetProperties();
            foreach (var destProp in destinationProperties)
            {
                foreach (var srcProp in sourceProperties)
                {

                    if (srcProp.Name == destProp.Name)
                    {
                        if (destProp.CanWrite)
                        {
                            if (destProp.PropertyType.Equals(srcProp.PropertyType))
                            {
                                destProp.SetValue(dest, srcProp.GetValue(src));
                            }
                          
                        }
                        break;
                    }


                }


            }

        }


        internal static void MapPublicPropertiesviaFormCollection<source, destination>(source src, destination dest) where source : NameValueCollection
        {
            var destinationProperties = dest.GetType().GetProperties();
            var sourceProperties = src.AllKeys;
            foreach (var destProp in destinationProperties)
            {
                foreach (var srcProp in sourceProperties)
                {

                    if (srcProp == destProp.Name)
                    {
                        if (destProp.CanWrite)
                        {
                            if (destProp.PropertyType == typeof(Int32))
                            {
                                if (!string.IsNullOrEmpty(src[srcProp]))
                                {
                                    destProp.SetValue(dest, Convert.ToInt32(src[srcProp]));
                                }
                                break;
                            }

                            if (destProp.PropertyType == typeof(Boolean))
                            {
                                var split = src[srcProp].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length == 0)
                                {
                                    destProp.SetValue(dest, false);
                                }
                                else
                                    destProp.SetValue(dest, Convert.ToBoolean(split[0]));
                                break;
                            }
                            if (destProp.PropertyType == typeof(Nullable<Boolean>))
                            {
                                var split = src[srcProp].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length == 0)
                                {
                                    destProp.SetValue(dest, false);
                                }
                                else
                                    destProp.SetValue(dest, Convert.ToBoolean(split[0]));
                                break;
                            }
                            if (destProp.PropertyType == typeof(Nullable<int>))
                            {
                                var val = Convert.ToInt32(src[srcProp]);
                                destProp.SetValue(dest, val);
                                break;
                            }
                            else
                                destProp.SetValue(dest, src[srcProp]);

                        }

                    }


                }


            }

        }




        internal void ProcessFile(DocumentModelView model)
        {

            model.FileSize = FileSizeCalculator.FileSize(model.FileSize);
        }

        internal List<DocumentModelView> GetOrientationDocuments(Documents document)
        {
            var d = dBEntities.vwOrientationDocuments.Where(m => m.InternalKey.ToString() == document.MenuId && m.Language == document.Language && m.IsArchived == document.IsArchived);
            List<DocumentModelView> docs = new List<DocumentModelView>();
            foreach (var item in d)
            {
                DocumentModelView model = new DocumentModelView();
                if (document != null)
                {
                    MapPublicProperties<vwOrientationDocument, DocumentModelView>(item, model);
                }
                this.ProcessFile(model);
                if (!model.FileUrl.Contains("http"))
                {
                    model.FileUrl = $"{ConfigurationManager.AppSettings["DocUrl"]}{model.FileUrl}";
                }
                docs.Add(model);
            }
            return docs;
        }



        internal List<DocumentModelView> GetDocuments(Documents document)
        {
            var d = dBEntities.vwDocuments.Where(m => m.InternalKey.ToString() == document.MenuId && m.Language == document.Language && m.IsArchived == document.IsArchived);
            List<DocumentModelView> docs = new List<DocumentModelView>();
            foreach (var item in d)
            {
                DocumentModelView model = new DocumentModelView();
                if (document != null)
                {
                    MapPublicProperties<vwDocument, DocumentModelView>(item, model);
                }
                this.ProcessFile(model);
                if (!model.FileUrl.Contains("http"))
                {
                    model.FileUrl = $"{ConfigurationManager.AppSettings["DocUrl"]}{model.FileUrl}";
                }
                docs.Add(model);
            }
            return docs;
        }



        internal Menu GetDocumentMenuDetailsByInternalKey(int menuId)
        {
            var menu = dBEntities.Menus.Where(x => x.InternalKey == menuId).FirstOrDefault();

            return menu;
        }
    }
}