﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Common
{
    public static class FileSizeCalculator
    {
        public static int FileSize(object size)
        {
            int newSize = 0;
            if (size != null)
                newSize = Convert.ToInt32(size) / 1000;
            return newSize;
        }
    }
}