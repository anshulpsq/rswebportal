﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class POVCModelView
    {
        public int Id { get; set; }
        public string CName { get; set; }
        public string DCName { get; set; }
        public string CLink { get; set; }
        public string DLink { get; set; }
         
        public List<tblPanelOfVCMember> VCMmembers { set; get; }
    }
   
}