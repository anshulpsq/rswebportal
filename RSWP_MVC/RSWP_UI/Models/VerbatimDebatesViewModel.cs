﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class VerbatimDebatesViewModel
    {
        public VerbatimDebatesViewModel()
        {
            Sessions = new List<SelectListItem>();
            Dates = new List<SelectListItem>();
            VerbatimDebatesList = new List<DocumentModelView>();
        }
        public List<DocumentModelView> VerbatimDebatesList { get; set; }
        public List<SelectListItem> Sessions { get; set; }
        public List<SelectListItem> Dates { get; set; }
        public int Session { get; set; }
        public DateTime SelectedDate { get; set; }
    }
}