﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class SectionNameViewModel
    {
        public string Section_Code { get; set; }
        public string Section_Name { get; set; }
        public string email { get; set; }
        public string hsection_name { get; set; }
    }
}