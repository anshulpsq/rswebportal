﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class DiscussionsListVM
    {
        public DiscussionsListVM()
        {
            SessionList = new List<SelectListItem>();
            DatesList = new List<SelectListItem>();
            TypeList = new List<SelectListItem>();
            FirstMemberList = new List<SelectListItem>();
            AssociatedMemberList = new List<SelectListItem>();
            DiscussionsList = new List<DiscussionsViewModel>();
        }
        public List<SelectListItem> SessionList { get; set; }
        public List<SelectListItem> DatesList { get; set; }
        public List<SelectListItem> TypeList { get; set; }
        public List<SelectListItem> FirstMemberList { get; set; }
        public List<SelectListItem> AssociatedMemberList { get; set; }
        public List<DiscussionsViewModel> DiscussionsList { get; set; }
    }
}