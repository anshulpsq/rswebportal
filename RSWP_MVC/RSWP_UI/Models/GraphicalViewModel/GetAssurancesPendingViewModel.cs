﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.GraphicalViewModel
{
    public class GetAssurancesPendingViewModel
    {
        public int drop_count { get; set; }
        public int pending_count { get; set; }
        public int assurance_count { get; set; }
        public int year1 { get; set; }
    }

}