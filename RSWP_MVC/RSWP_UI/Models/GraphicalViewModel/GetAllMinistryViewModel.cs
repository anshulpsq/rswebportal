﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.GraphicalViewModel
{
    public class GetAllMinistryViewModel
    {
        public int min_code { get; set; }
        public string min_name { get; set; }
        public string smin { get; set; }
        public string min_curr { get; set; }
    }

}