﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.GraphicalViewModel
{
    public class GetAllNoticesViewModel
    {
        public int strarrednoticessubited { get; set; }
        public int unstrarrednoticessubmitted { get; set; }
        public int strarrednoticesadmitted { get; set; }
        public int unstrarrednoticesadmitted { get; set; }
        public int unstrarrednoticesdisallowed { get; set; }
        public int Lapsed { get; set; }
    }

}