﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.GraphicalViewModel
{
    public class GetBillsViewModel
    {
        public int GovtBill { get; set; }
        public int PrivateBill { get; set; }
    }

}