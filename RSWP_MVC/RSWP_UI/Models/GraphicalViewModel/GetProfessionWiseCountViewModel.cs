﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.GraphicalViewModel
{
    public class GetProfessionWiseCountViewModel
    {
        public int PROF_CODE { get; set; }
        public string PROF_NAME { get; set; }
        public object HPROF_NAME { get; set; }
        public int count { get; set; }
    }
}