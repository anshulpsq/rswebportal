﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class ExterLinkViewModel
    {
        public ExterLinkViewModel()
        {
            SectionList = new List<SelectListItem>();
            SubSectionList = new List<SelectListItem>();
            ModuleList = new List<SelectListItem>();
        }
        public int Id { get; set; }
        public int SId { get; set; }

        [Required]
        [Display(Name = "Section")]
        public List<SelectListItem> SectionList { get; set; }//Section
        public string SectionName { get; set; }
        public int SectionId { get; set; }
        [Required]
        [Display(Name = "Sub Section")]
        public List<SelectListItem> SubSectionList { get; set; }//Sub section
        public string SubSectionName { get; set; }
        public int SubSectionId { get; set; }
        [Display(Name = "Module")]
        public List<SelectListItem> ModuleList { get; set; }//Module 
        [Required]
        public string ModuleName { get; set; }
        public int ModuleId { get; set; }
        [Required]
        [Display(Name = "URL")]
        public string URL { get; set; }
    }
}