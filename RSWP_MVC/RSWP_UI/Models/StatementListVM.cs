﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class StatementListVM
    {
        public StatementListVM()
        {
            StatementList = new List<StatementsViewModel>();
        }
        public List<StatementsViewModel> StatementList { get; set; }
        public string Type { get; set; }
        public int SessionNumber { get; set; }

    }
}