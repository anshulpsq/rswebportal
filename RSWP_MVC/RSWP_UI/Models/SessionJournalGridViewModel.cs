﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class SessionJournalGridViewModel
    {
        public SessionJournalGridViewModel()
        {
            DocumentList = new List<DocumentModelView>();
        }
        public string HTMLContent { get; set; }
        public int SessionNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<DocumentModelView> DocumentList { get; set; }
        public string  SessionNoInWords { get; set; }
        public DateTime ContentDate { get; set; }
    }
}