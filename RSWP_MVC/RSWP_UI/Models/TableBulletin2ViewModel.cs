﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class TableBulletin2ViewModel
    {
        public TableBulletin2ViewModel()
        {
            Bulletins = new List<BulletinListVM>();
        }
        public List<BulletinListVM> Bulletins = new List<BulletinListVM>();
        public List<SelectListItem> BulletinNoList = new List<SelectListItem>();
        public List<SelectListItem> SectionList = new List<SelectListItem>();
    }
}