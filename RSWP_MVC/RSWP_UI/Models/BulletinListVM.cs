﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class BulletinListVM
    {
        public BulletinListVM()
        {
            EnglishDocument = new DocumentModelView();
            HindiDocument = new DocumentModelView();
            BulletinList = new List<Bulletin2ViewModel>();          
        }       
        public DocumentModelView EnglishDocument { get; set; }
        public DocumentModelView HindiDocument { get; set; }
        public DateTime SelectedDate { get; set; }
        public List<Bulletin2ViewModel> BulletinList = new List<Bulletin2ViewModel>();

    }
}