﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.RajyaSabhaSecretariatViewModel
{
    public class GetSecretaryGeneralViewModel
    {
        public int OfficerID { get; set; }
        public string OfficerName { get; set; }
        public int ODesigID { get; set; }
        public string DesigVar { get; set; }
        public string Address { get; set; }
        public object WorkResponse { get; set; }
        public string EmailID { get; set; }
        public string TeleNo { get; set; }
        public string Fax { get; set; }
        public string Intercom { get; set; }
        public string Pstn { get; set; }
        public int CategoryID { get; set; }
        public int priority { get; set; }
        public string PhotoName { get; set; }
        public string ODesigName { get; set; }
        public string ODAbr { get; set; }
        public string wr { get; set; }
        public bool OExist { get; set; }
    }

}