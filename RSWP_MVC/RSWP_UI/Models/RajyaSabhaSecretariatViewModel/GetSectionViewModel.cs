﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.RajyaSabhaSecretariatViewModel
{
    public class GetSectionViewModel
    {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public string RoomNo { get; set; }
        public string Location { get; set; }
        public string TelephoneNo { get; set; }
        public string EmailID { get; set; }
        public string incharge { get; set; }
        public int priority { get; set; }
    }

}