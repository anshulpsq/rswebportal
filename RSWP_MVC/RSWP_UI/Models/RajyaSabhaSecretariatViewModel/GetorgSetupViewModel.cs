﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.RajyaSabhaSecretariatViewModel
{
    public class GetorgSetupViewModel
    {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public string eo_lo_co_po { get; set; }
        public string assistdirector { get; set; }
        public string us_ad { get; set; }
        public string deputydirector { get; set; }
        public string jointdirector { get; set; }
        public string ds_jd { get; set; }
        public string director { get; set; }
        public string ad { get; set; }
        public string osd_js { get; set; }
        public string as_secratary { get; set; }
        public int priority { get; set; }
    }

}