﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class DucumentListVM
    {
        public DucumentListVM()
        {
            DocumentList = new List<DocumentModelView>();
        }
        public List<DocumentModelView> DocumentList { get; set; }
        public bool Archive { get; set; }
        public string Section { get; set; }
        public string SubSection { get; set; }
    }
}