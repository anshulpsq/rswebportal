﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class StatementsViewModel
    {
        public StatementsViewModel()
        {
            BulletinData = new Bulletin2ViewModel();
        }
        public string LegislativeBusiness { get; set; }
        public int SessionNo { get; set; }
        public string BulletinNo { get; set; }
        public DateTime? WeekEndingDate { get; set; }
        public Bulletin2ViewModel BulletinData { get; set; }

    }
}