﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class PaperLaidInRSViewModel
    {
        public PaperLaidInRSViewModel()
        {
            SessionList = new List<SelectListItem>();
            SittingDates = new List<SelectListItem>();
            MinistryList = new List<SelectListItem>();
            ReportTypeList = new List<SelectListItem>();
            PaperLaidInRSList = new List<PaperLaindInRSGridViewModel>();
        }
        public int Id { get; set; }
        public List<SelectListItem> SessionList { get; set; }
        public List<SelectListItem> SittingDates { get; set; }
        public List<SelectListItem> MinistryList { get; set; }
        public List<SelectListItem> ReportTypeList { get; set; }
       public List<PaperLaindInRSGridViewModel> PaperLaidInRSList { get; set; }
        public string Message { get; set; }
    }
}