﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.LegislationViewModel
{
    public class GetAll_PassbillViewModel
    {
        public string bill_no { get; set; }
        public int bill_year { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string intro { get; set; }
        public string ministry { get; set; }
        public int bill_id { get; set; }
        public int house_number { get; set; }
        public string as_introduced_file { get; set; }
        public string as_passedrajyasabha_file { get; set; }
        public string as_passedloksabha_file { get; set; }
        public string as_passedbothhouses_file { get; set; }
        public string errata_lsfile { get; set; }
        public string house { get; set; }
        public int sessionno { get; set; }
        public int IntroSession { get; set; }
        public object RetSession { get; set; }
        public DateTime passing_dt { get; set; }
        public string passing { get; set; }
        public string H_as_introduced_file { get; set; }
        public string H_as_passedrajyasabha_file { get; set; }
        public string H_as_passedloksabha_file { get; set; }
        public string H_as_passedbothhouses_file { get; set; }
        public string H_errata_lsfile { get; set; }
        public object return_second_house { get; set; }
        public DateTime introduction { get; set; }
    }

}