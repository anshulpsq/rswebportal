﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.LegislationViewModel
{
    public class GetAll_BillSearchViewModel
    {
        public string num_year { get; set; }
        public string number { get; set; }
        public int year { get; set; }
        public int hs_num { get; set; }
        public int texts { get; set; }
        public string short_title { get; set; }
        public string type { get; set; }
        public int mp_no { get; set; }
        public string intro_by { get; set; }
        public string ministry { get; set; }
        public object act_number { get; set; }
        public string status { get; set; }
        public string House { get; set; }
        public string HS { get; set; }
        public int Bill_ID { get; set; }
        public string category { get; set; }
        public object Dbsl { get; set; }
        public object Debate_Date2 { get; set; }
        public object Dbsl2 { get; set; }
        public object gaz_pdf { get; set; }
        public object synop_pdf { get; set; }
        public object Comm_rep_on { get; set; }
        public object Comm_report_url { get; set; }
        public DateTime introduction { get; set; }
        public object passed_in_loksabha { get; set; }
        public object passed_in_rajyasabha { get; set; }
        public object assent { get; set; }
        public string id1 { get; set; }
        public string id2 { get; set; }
        public string id3 { get; set; }
        public string id4 { get; set; }
        public string id5 { get; set; }
        public string id6 { get; set; }
        public string id7 { get; set; }
        public string id8 { get; set; }
        public string id9 { get; set; }
        public string id10 { get; set; }
        public object return_second_house { get; set; }
        public object passing_dt { get; set; }
        public object Comm_ref_date { get; set; }
    }

}