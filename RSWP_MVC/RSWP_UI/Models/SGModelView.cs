﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class SGModelView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string ImgPath { get; set; }
        public string Profile { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
    }
}