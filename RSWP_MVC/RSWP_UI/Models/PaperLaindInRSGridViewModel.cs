﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class PaperLaindInRSGridViewModel
    {      
        public int SessionNo { get; set; }
        public DateTime SittingDate { get; set; }
        public string MinistryName { get; set; }
        public int ReportTypeId { get; set; }
        public string ReportType { get; set; }
        public string Language { get; set; }
        //Document properties:
        public string DocumentName { get; set; }
        public string Tiltle { get; set; }
        public Nullable<int> FileSize { get; set; }
        public string FileLocation { get; set; }
        public string FileUrl { get; set; }

    }
}