﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class Bulletin2ViewModel
    {

        public string BulletinNo { get; set; }
        public DateTime Date { get; set; }
        public string SectionName { get; set; }
        public string SubjectName { get; set; }
        public string HtmlText { get; set; }
        public string SGName { get; set; }
        public string SGDesignation { get; set; }
        public string FileName { get; set; }
        public string FileLanguage { get; set; }
       
    }
}