﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class TodayAtGlanceViewModel
    {
        public TodayAtGlanceViewModel()
        {
            TodayAtGlanceDocumentList = new Dictionary<string, DocumentModelView>();
        }
       
      public  Dictionary<string, DocumentModelView> TodayAtGlanceDocumentList  { get; set; }
}
}