﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class BussinessDocumentModel
    {
        public BussinessDocumentModel()
        {
            Sessions = new List<int>();
            SittingMonths = new List<SelectListItem>();
            Documents = new List<Document>();
            SessionList = new List<sessionsMaster>();
        }
        public List<int> Sessions { get; set; }
        public List<sessionsMaster> SessionList { get; set; }
        public List<SelectListItem> SittingMonths { get; set; }
        public List<Document> Documents { get; set; }
        public string LegislativeIntroductionUrl { get; set; }
        public bool IsApproved { get; set; }
    }
}