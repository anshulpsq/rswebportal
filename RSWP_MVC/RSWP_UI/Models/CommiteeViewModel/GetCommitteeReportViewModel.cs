﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommitteeReportViewModel
    {
        public int repid { get; set; }
        public int reportno { get; set; }
        public int comid { get; set; }
        public object subcomid { get; set; }
        public int minid { get; set; }
        public string txtsub { get; set; }
        public string adp_date { get; set; }
        public string Pres_date { get; set; }
        public string publish { get; set; }
        public string rptfilename { get; set; }
        public string insertdate { get; set; }
        public string updatedate { get; set; }
        public object rpttype { get; set; }
        public string cflag { get; set; }
        public string vmaincomname { get; set; }
        public object vsubcomname { get; set; }
        public string rptfilepath_h { get; set; }
        public string rptfilename_h { get; set; }
        public string txtsub_h { get; set; }
        public string vmaincomname_h { get; set; }
        public object vsubcomname_h { get; set; }
        public object Dept_Name_hindi { get; set; }
        public string vdeptname { get; set; }
        public string vcatname { get; set; }
        public string vcatname_hindi { get; set; }
        public int ncatid { get; set; }
        public int nMstcomid { get; set; }
        public string vMstcomName { get; set; }
        public string vMstcomName_hindi { get; set; }
        public string vHouse { get; set; }
        public DateTime adporgdate { get; set; }
        public DateTime presorgdate { get; set; }
        public DateTime insertorgdate { get; set; }
        public string rptfilepath { get; set; }
    }

}