﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommiteeSubjectViewModel
    {
        public int nsubid { get; set; }
        public int ncomid { get; set; }
        public int ndeptid { get; set; }
        public string subject { get; set; }
        public string remark { get; set; }
        public DateTime crdate { get; set; }
        public string cflag { get; set; }
        public string vmaincomname { get; set; }
        public string vmaincomname_h { get; set; }
        public string vshortName { get; set; }
        public string vMstcomName { get; set; }
        public string vMstcomName_hindi { get; set; }
        public string vHouse { get; set; }
        public string vcatname { get; set; }
        public string vcatname_hindi { get; set; }
        public int nsrno { get; set; }
        public string vdeptname { get; set; }
        public string Min_Name { get; set; }
        public string Min_Name_hindi { get; set; }
        public string Dept_Name_hindi { get; set; }
        public string Dept_Name { get; set; }
        public int nmastercomid { get; set; }
        public string subject_h { get; set; }
        public string remark_h { get; set; }
    }

}