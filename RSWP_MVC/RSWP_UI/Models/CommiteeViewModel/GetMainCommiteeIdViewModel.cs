﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetMainCommiteeIdViewModel
    {
        public int nMstcomid { get; set; }
        public int ncatid { get; set; }
        public string vMstcomName { get; set; }
        public string vMstcomName_hindi { get; set; }
        public string vHouse { get; set; }
        public string vcatname { get; set; }
        public string catshortName { get; set; }
        public string vcatname_hindi { get; set; }
        public int nsrno { get; set; }
        public int nmaincomid { get; set; }
        public string vmaincomname { get; set; }
        public string vmaincomname_h { get; set; }
        public string vshortName { get; set; }
        public int nls { get; set; }
        public int nrs { get; set; }
        public DateTime ddof { get; set; }
        public object ddocess { get; set; }
        public string vmeetfileno { get; set; }
        public string vtourfileno { get; set; }
        public int nYear { get; set; }
        public string cFlag { get; set; }
    }

}