﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommMemberListViewModel
    {
        public int nmemid { get; set; }
        public int nmaincomid { get; set; }
        public string vhouse { get; set; }
        public string vmaincomname { get; set; }
        public string vmember_id { get; set; }
        public string vmember_name { get; set; }
        public string vstate { get; set; }
        public string vConstituency { get; set; }
        public string vParty { get; set; }
        public DateTime ddoj { get; set; }
        public object ddodemitt { get; set; }
        public string vstatus { get; set; }
        public int srno { get; set; }
        public int nmastercomid { get; set; }
    }

}