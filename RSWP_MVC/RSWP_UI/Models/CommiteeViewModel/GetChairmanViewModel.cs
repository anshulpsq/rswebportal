﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetChairmanViewModel
    {
        public int nchairmnid { get; set; }
        public int nmaincomid { get; set; }
        public string vhouse { get; set; }
        public string vchair_id { get; set; }
        public string vchair_name { get; set; }
        public string vstate { get; set; }
        public string vconstituency { get; set; }
        public string vGender { get; set; }
        public string vparty { get; set; }
        public DateTime ddoj { get; set; }
        public object ddodemitt { get; set; }
        public string chrimage { get; set; }
        public string cflag { get; set; }
        public DateTime dof { get; set; }
    }

}