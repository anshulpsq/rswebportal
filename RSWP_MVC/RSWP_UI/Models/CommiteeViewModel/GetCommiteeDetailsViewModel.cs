﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommiteesList
    {
        public List<GetCommiteeDetailsViewModel> commiteesList { get; set; }
    }
    public class GetCommiteeDetailsViewModel
    {
        public int nmaincomid { get; set; }
        public string vmaincomname { get; set; }
        public string vmaincomname_h { get; set; }
        public string introduction { get; set; }
        public string introduction_h { get; set; }
        public string rules { get; set; }
        public string rules_h { get; set; }
        public string contactus { get; set; }
        public string contactus_h { get; set; }
        public string cFlag { get; set; }
        public int nMstcomid { get; set; }
        public int ncatid { get; set; }
    }

}