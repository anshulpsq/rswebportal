﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommitteeMeetingViewModel
    {
        public int nmeetid { get; set; }
        public string agenda { get; set; }
        public int nmeetno { get; set; }
        public string dmeetdate { get; set; }
        public string meet_time { get; set; }
        public int nvenueid { get; set; }
        public string vvenuename { get; set; }
        public int nmaincomid { get; set; }
        public string comName { get; set; }
        public bool multiple { get; set; }
        public int nYear { get; set; }
        public string cFlag { get; set; }
    }

}