﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommitteeEngagementViewModel
    {
        public int TourId { get; set; }
        public int ComId { get; set; }
        public string ComName { get; set; }
        public string FrmDt { get; set; }
        public string ToDt { get; set; }
        public string agenda { get; set; }
        public string Remark { get; set; }
        public string TourNo { get; set; }
        public int nyear { get; set; }
        public string cFlag { get; set; }
        public string noticeprintdate { get; set; }
        public string publish { get; set; }
        public string noticedate { get; set; }
        public int nsignatory { get; set; }
        public string tourofficer { get; set; }
        public string vofficerid { get; set; }
        public string name { get; set; }
        public string desg { get; set; }
        public string telno { get; set; }
        public string mobileno { get; set; }
        public string vAddress { get; set; }
        public int sectionid { get; set; }
        public string SecName { get; set; }
        public string secadd { get; set; }
        public string sectelno { get; set; }
        public string secemail { get; set; }
        public string vcatname { get; set; }
        public string host { get; set; }
        public string hostorgname { get; set; }
        public object printtext { get; set; }
        public string vtourfileno { get; set; }
        public int nls { get; set; }
        public int nrs { get; set; }
        public object guestrelation { get; set; }
        public object guestattended { get; set; }
        public object totalmember { get; set; }
        public DateTime Expr1 { get; set; }
        public string TourStatus { get; set; }
        public DateTime frmorgdt { get; set; }
        public DateTime toorgdt { get; set; }
        public string state_visited_tour_id { get; set; }
        public string state_visited_tour_name { get; set; }
        public string Place_visited_tour_id { get; set; }
        public string Place_visited_tour_name { get; set; }
        public object tourreviseddate { get; set; }
        public object proposeplantext { get; set; }
    }

}