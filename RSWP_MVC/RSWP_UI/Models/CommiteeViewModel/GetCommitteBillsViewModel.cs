﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetCommitteBillsViewModel
    {
        public int Billid { get; set; }
        public string Billno { get; set; }
        public int comid { get; set; }
        public object subcomid { get; set; }
        public string txttitle { get; set; }
        public string txtpress { get; set; }
        public string pub_date { get; set; }
        public string sugg_date { get; set; }
        public string Feedback { get; set; }
        public string publish { get; set; }
        public string rptbillfilename { get; set; }
        public string rptbillfilepath { get; set; }
        public string rptpressfilename { get; set; }
        public string rptpressfilepath { get; set; }
        public DateTime insertdate { get; set; }
        public DateTime updatedate { get; set; }
        public string cFlag { get; set; }
        public string vmaincomname { get; set; }
        public object vsubcomname { get; set; }
        public string rptlegissynopname { get; set; }
        public string rptlegissynop_path { get; set; }
        public string Archive { get; set; }
        public string vcatname { get; set; }
        public string vcatname_hindi { get; set; }
        public int ncatid { get; set; }
        public int nMstcomid { get; set; }
        public string vMstcomName { get; set; }
        public string vMstcomName_hindi { get; set; }
        public string vHouse { get; set; }
        public object txttitle_hindi { get; set; }
        public object txtpress_hindi { get; set; }
        public object Feedback_hindi { get; set; }
        public object rptbillfilename_hindi { get; set; }
        public object rptbillfilepath_hindi { get; set; }
        public object rptpressfilename_hindi { get; set; }
        public object rptpressfilepath_hindi { get; set; }
        public object rptlegissynopname_hindi { get; set; }
        public object rptlegissynop_path_hindi { get; set; }
        public string vmaincomname_h { get; set; }
    }

}