﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetPressReleaseViewModel
    {
        public int pressid { get; set; }
        public int comid { get; set; }
        public object subcomid { get; set; }
        public string txtpress { get; set; }
        public string pub_date { get; set; }
        public DateTime pub_date_order { get; set; }
        public string sugg_date { get; set; }
        public string Feedback { get; set; }
        public string publish { get; set; }
        public string rptpressfilename { get; set; }
        public string rptpressfilepath { get; set; }
        public DateTime insertdate { get; set; }
        public object updatedate { get; set; }
        public string cFlag { get; set; }
        public string vmaincomname { get; set; }
        public object vsubcomname { get; set; }
        public object Otherid { get; set; }
        public string Archive { get; set; }
        public int nmastercomid { get; set; }
        public object rptpressfilename_hindi { get; set; }
        public object rptpressfilepath_hindi { get; set; }
        public object Feedback_hindi { get; set; }
        public object txtpress_hindi { get; set; }
        public string vmaincomname_h { get; set; }
        public string vshortName { get; set; }
    }

}