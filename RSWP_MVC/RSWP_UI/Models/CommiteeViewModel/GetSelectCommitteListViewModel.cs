﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    //public class GetSelectCommitteListViewModel
    //{
    //    public int nMstcomid { get; set; }
    //    public int ncatid { get; set; }
    //    public string vMstcomName { get; set; }
    //    public string vMstcomName_hindi { get; set; }
    //    public string vHouse { get; set; }
    //    public string status { get; set; }
    //    public object URL_loksabha { get; set; }
    //    public object URL_loksabha_hindi { get; set; }
    //    public object category_mst { get; set; }
    //    public object[] Main_comm { get; set; }
    //}
    

    public class GetSelectCommitteListViewModel
    {
        public int nmaincomid { get; set; }
        public string vMstcomName { get; set; }
        public string vcatname { get; set; }
        public string vhouse { get; set; }
        public object vchair_id { get; set; }
        public object vchair_name { get; set; }
        public string constituteddate { get; set; }
        public int ncatid { get; set; }
        public int nMstcomid { get; set; }
        public string URL_loksabha { get; set; }
    }


}