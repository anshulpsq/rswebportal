﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{
    public class GetMoreInfoViewModel
    {
        public int nid { get; set; }
        public int ncomid { get; set; }
        public string title { get; set; }
        public int norder { get; set; }
        public string filename { get; set; }
        public string filepath { get; set; }
        public object hyperlink { get; set; }
        public DateTime crdate { get; set; }
        public string cflag { get; set; }
        public int nmastercomid { get; set; }
        public string vmaincomname { get; set; }
        public string vmaincomname_h { get; set; }
        public string vMstcomName { get; set; }
        public int ncatid { get; set; }
        public string vMstcomName_hindi { get; set; }
        public string vcatname { get; set; }
        public string vcatname_hindi { get; set; }
    }

}