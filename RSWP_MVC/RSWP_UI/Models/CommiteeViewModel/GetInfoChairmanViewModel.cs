﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.CommiteeViewModel
{

    public class GetInfoChairmanViewModel
    {
        public int nmaincomid { get; set; }
        public string vMstcomName { get; set; }
        public string vcatname { get; set; }
        public string vhouse { get; set; }
        public string vchair_id { get; set; }
        public string vchair_name { get; set; }
        public string constituteddate { get; set; }
        public int ncatid { get; set; }
        public int nMstcomid { get; set; }
        public object URL_loksabha { get; set; }
        public string vstate { get; set; }
        public string vParty { get; set; }
        public string vConstituency { get; set; }
    }

}