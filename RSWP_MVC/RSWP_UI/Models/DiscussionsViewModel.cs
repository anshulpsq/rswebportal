﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class DiscussionsViewModel
    {
        
        public bool HasAssociatedMembers { get; set; }
        public string FileName { get; set; }
        public string Title { get; set; }
        public int Session { get; set; }
        public DateTime SelectedDate { get; set; }
        public string SelectedType { get; set; }
        public string SelectedFirstMember { get; set; }
        public string SelectedAssociatedMember { get; set; }
        public int Id { get; set; }
        public int SNo { get; set; }
        public string Prefix { get; set; }
        public string Language { get; set; }
        public int DocId { get; set; }
      
        public string FileUrl { get; set; }
        public string Name { get; set; }
        public int? FileSize { get; set; }
    }
}