﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
  


    public class Orientation
    {
     
        public int Id { get; set; }

        public string RowType { get; set; }
        public int DocumentId { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public string Venue { get; set; }
    }



    public class OrientationDocumentViewModel
    {

        public int Id { get; set; }

        public string RowType { get; set; }
        public int DocumentId { get; set; }

        public int MenuId { get; set; }

        public string FileLocation { get; set; }
        public string FileUrl { get; set; }

        public string FileSize { get; set; }
        public string Name { get; set; }
        public Nullable<bool> IsArchived { get; set; }
        public string Tiltle { get; set; }
        public Nullable<int> Year { get; set; }

        public string Venue { get; set; }
        public string Time { get; set; }

        public string Duration { get; set; }

        public string Language { get; set; }

        public string IsArchievalNeeded { get; }
    }





    public class LookupViewModel
    {
        public string LookupText { get; set; }
        public string LookupValue { get; set; }
        public string LookupInternalValue { get; set; }

        public string LookupName { get; set; }

    }
}