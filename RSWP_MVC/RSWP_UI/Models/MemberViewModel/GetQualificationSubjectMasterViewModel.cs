﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetQualificationSubjectMasterViewModel
    {
        public int LEVEL_CODE { get; set; }
        public int DEGREE_CODE { get; set; }
        public int SUBJECT_CODE { get; set; }
        public string SUBJECT_NAME { get; set; }
        public string HSUBJECT_NAME { get; set; }
    }

}