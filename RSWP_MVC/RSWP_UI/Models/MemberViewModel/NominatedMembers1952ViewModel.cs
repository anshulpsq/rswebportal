﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class NominatedMembers1952ViewModel
    {
        public int MP_CODE { get; set; }
        public string MP_INIT { get; set; }
        public object name { get; set; }
        public string MP_FNAME { get; set; }
        public string MP_LNAME { get; set; }
        public string MP_PARTY_CODE { get; set; }
        public string STATE_NAME { get; set; }
        public DateTime NOTIFICATION_DATE { get; set; }
        public DateTime EXPIRATION_DATE { get; set; }
        public string n_date { get; set; }
        public string e_date { get; set; }
        public string mp_name { get; set; }
        public string MPFULLNAME { get; set; }
    }

}