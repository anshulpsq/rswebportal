﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetTermWiseMemberViewModel
    {
        public int MP_CODE { get; set; }
        public string MP_NAME { get; set; }
        public object GENDER { get; set; }
        public string STATE_NAME { get; set; }
        public string PARTY_NAME { get; set; }
        public object PARTY_CODE { get; set; }
        public object DATE_BIRTH { get; set; }
        public object email { get; set; }
        public object PTELE { get; set; }
        public string MP_INIT { get; set; }
        public object PHOTO { get; set; }
        public object LADD { get; set; }
        public object LTELE { get; set; }
        public object PADD { get; set; }
        public int totalcount { get; set; }
        public DateTime NOTIFICATION_DATE { get; set; }
        public DateTime EXPIRATION_DATE { get; set; }
        public string MPFULLNAME { get; set; }
        public object reason { get; set; }
    }


}