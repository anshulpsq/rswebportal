﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetStateViewModel
    {
        public int STATE_COUNT { get; set; }
        public string STATE_NAME { get; set; }
        public string STATE_CODE { get; set; }
        public int NO_SEATS { get; set; }
    }

}