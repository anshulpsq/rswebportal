﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetQualiMasterDegreeViewModel
    {
        public int LEVEL_CODE { get; set; }
        public int DEGREE_CODE { get; set; }
        public string DEGREE_NAME { get; set; }
        public string HDEGREE_NAME { get; set; }
    }

}