﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetQualificationViewModel
    {
        public int LEVEL_CODE { get; set; }
        public string LEVEL_NAME { get; set; }
        public object HLEVEL_NAME { get; set; }
    }
}