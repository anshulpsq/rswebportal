﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetSessionViewModel
    {
        public string sessionno { get; set; }
        public object dates { get; set; }
        public string sessionname { get; set; }
        public DateTime period { get; set; }
        public DateTime period2 { get; set; }
        public object period3 { get; set; }
        public object period4 { get; set; }
        public int totaldays { get; set; }
        public object name { get; set; }
        public int divno { get; set; }
        public object C_MP_STATE_CODE { get; set; }
        public int noofsittings { get; set; }
    }

}