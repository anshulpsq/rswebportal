﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetSecretaryGeneralViewModel
    {
        public int CODE { get; set; }
        public string INIT { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public DateTime TERM_FROM { get; set; }
        public string TERM_TO { get; set; }
        public Boolean CURRENT_FLAG { get; set; }
        public string BIODATA { get; set; }
        public string HINIT { get; set; }
        public string HFNAME { get; set; }
        public string HLNAME { get; set; }
        public string HBiodata { get; set; }
        public string PhotoName { get; set; }
    }
}