﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetWomenMemberViewModel
    {
        public string PARTY_CODE { get; set; }
        public string PARTY_NAME { get; set; }
        public int MP_CODE { get; set; }
        public string MP_NAME { get; set; }
        public string GENDER { get; set; }
        public string STATE_NAME { get; set; }
        public string DATE_BIRTH { get; set; }
        public string email { get; set; }
        public string PTELE { get; set; }
        public object MP_INIT { get; set; }
        public object PHOTO { get; set; }
        public string LADD { get; set; }
        public object LTELE { get; set; }
        public string PADD { get; set; }
        public int totalcount { get; set; }
        public DateTime NOTIFICATION_DATE { get; set; }
        public DateTime EXPIRATION_DATE { get; set; }
        public string MPFULLNAME { get; set; }
    }

}