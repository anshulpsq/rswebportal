﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetExpenditureIncurredViewModel
    {
        public int MP_CODE { get; set; }
        public int pageno { get; set; }
        public int constituencyallow { get; set; }
        public int officeallow { get; set; }
        public int billyear { get; set; }
        public int billmonth { get; set; }
        public int mpsal { get; set; }
        public int pasal { get; set; }
        public int tada { get; set; }
        public string billmonthtext { get; set; }
    }

    public class GetExpenditureOPViewModel
    {
        public int MP_CODE { get; set; }
        public object pageno { get; set; }
        public int constituencyallow { get; set; }
        public int sumpallow { get; set; }
        public int billyear { get; set; }
        public int billmonth { get; set; }
        public int da { get; set; }
        public int basicsal { get; set; }
        public int tada { get; set; }
        public string billmonthtext { get; set; }
    }
    public class GetOppositionleaderViewModel
    {
        public int MP_CODE { get; set; }
        public DateTime TERM_FROM { get; set; }
        public object TERM_TO { get; set; }
        public bool CURRENT_FLAG { get; set; }
    }


    public class GetDeputyChairmanViewModel
    {
        public int MP_CODE { get; set; }
        public DateTime TERM_FROM { get; set; }
        public DateTime TERM_TO { get; set; }
        public bool CURRENT_FLAG { get; set; }
        public int reason_code { get; set; }
    }

}