﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.MemberViewModel
{
    public class GetCurrentMemberBiodataViewModel
    {
        public int MP_CODE { get; set; }
        public string FATH_NAME { get; set; }
        public string MOTH_NAME { get; set; }
        public DateTime DATE_BIRTH { get; set; }
        public string PLACE_BIRTH { get; set; }
        public string MARITIAL { get; set; }
        public string DATE_MARRIAGE { get; set; }
        public string SPOUSE_NAME { get; set; }
        public string NO_SONS { get; set; }
        public string NO_DAUGHTER { get; set; }
        public string QUALIFICATION { get; set; }
        public string OTHER_PROF_DETAIL { get; set; }
        public string PREVIOUS_MEMBERSHIP { get; set; }
        public string DET_PUB_OFFICE_HELD { get; set; }
        public bool FREE_STRUGGLE { get; set; }
        public string BOOKS { get; set; }
        public string ACTIVITY { get; set; }
        public string COUNTRY_VISITED { get; set; }
        public string HOBBIES { get; set; }
        public string ESSENTIAL_INFO { get; set; }
        public string MP_INIT { get; set; }
        public string MP_FNAME { get; set; }
        public string MP_LNAME { get; set; }
        public string PARTY_NAME { get; set; }
        public string STATE_NAME { get; set; }
        public string C_PADDRESS { get; set; }
        public string C_PPIN { get; set; }
        public string C_PTELE { get; set; }
        public string OTH_C_PADDRESS { get; set; }
        public string OTH_C_PPIN { get; set; }
        public string OTH_C_PTELE { get; set; }
        public string C_LADDRESS { get; set; }
        public string C_LPIN { get; set; }
        public string C_LTELE { get; set; }
        public string OTH_C_LADDRESS { get; set; }
        public string OTH_C_LPIN { get; set; }
        public string OTH_C_LTELE { get; set; }
        public string prof1 { get; set; }
        public string prof2 { get; set; }
        public string prof3 { get; set; }
        public bool MP_CURRENT { get; set; }
        public string C_EMAIL_ID { get; set; }
        public object MP_MobileNo2 { get; set; }
        public object MP_MobileNo { get; set; }
        public DateTime oath { get; set; }
    }

}