﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class OfficialDebatesViewModel
    {
        public OfficialDebatesViewModel()
        {
            FloorVersionDocument = new DocumentModelView();
            HindiVersionDocument = new DocumentModelView();
        }
        public DocumentModelView FloorVersionDocument { get; set; }
        public DocumentModelView HindiVersionDocument { get; set; }
        public DateTime SelectedDate { get; set; }
        public int SessionNo { get; set; }
    }
}