﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class SessionJournalViewModel
    {
        public SessionJournalViewModel()
        {
            Sessions = new List<SelectListItem>();
            GridData = new SessionJournalGridViewModel();
        }
        public List<SelectListItem> Sessions { get; set; }
        public SessionJournalGridViewModel GridData { get; set; }
    }
}