﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Models
{
    public class OfficialDebatesListVM
    {
        public OfficialDebatesListVM()
        {
            OfficialDebatesList = new List<OfficialDebatesViewModel>();
        }
        public List<OfficialDebatesViewModel> OfficialDebatesList { get; set; }
        public List<SelectListItem> Sessions { get; set; }
       
        
    }
}