﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class AdvanceSearchViewModel
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string BulletinNo { get; set; }
        public string Section { get; set; }
        public string SearchHtml { get; set; }
        public string SearchTitle { get; set; }
        public string SortOn { get; set; }

        public string SessionNo { get; set; }
        public string Ministry { get; set; }
        public string ReportType { get; set; }
        public string DocTitleSearch { get; set; }
        public bool DisplayAllRecords { get; set; }//True if not filtering aplied
        public string DiscussionType { get; set; }
        public string AllMember { get; set; }
    }
}