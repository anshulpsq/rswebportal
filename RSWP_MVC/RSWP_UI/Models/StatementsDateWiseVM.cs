﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class StatementsDateWiseVM
    {
        public StatementsDateWiseVM()
        {
            DateWiseStatement = new List<StatementListVM>();
        }
        public List<StatementListVM> DateWiseStatement { get; set; }
        public string Type { get; set; }
    }
}