﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.TodayInRSViewModel
{
    public class GetGovBillsViewModel
    {
        public int Sno { get; set; }
        public DateTime Sitting_Date { get; set; }
        public string Bill_No { get; set; }
        public string Bill_Title { get; set; }
        public string Type { get; set; }
        public int sessionno { get; set; }
        public string Year { get; set; }
        public string Bill_ID { get; set; }
        public string House { get; set; }
        public string ministry { get; set; }
        public string link { get; set; }
    }

}