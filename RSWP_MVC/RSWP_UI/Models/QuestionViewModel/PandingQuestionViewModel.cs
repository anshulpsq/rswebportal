﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class PandingQuestionViewModel
    {
        public string ans_date { get; set; }
        public string qtype { get; set; }
        public int min_code { get; set; }
        public string minname { get; set; }
        public int Total { get; set; }
        public int Received { get; set; }
        public object Pending { get; set; }
        public object dep_code { get; set; }
        public object depname { get; set; }
    }
}