﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class GetAllIntegratedSearchViewModel
    {
        public float qslno { get; set; }
        public string qtitle { get; set; }
        public string qtype { get; set; }
        public string ans_date { get; set; }
        public DateTime adate { get; set; }
        public string shri { get; set; }
        public float qno { get; set; }
        public string name { get; set; }
        public string min_name { get; set; }
        public string qn_text { get; set; }
        public string ans_text { get; set; }
        public int ses_no { get; set; }
        public int depc { get; set; }
        public string status { get; set; }
        public string P_flag { get; set; }
        public object files { get; set; }
        public object hindifiles { get; set; }
    }

}