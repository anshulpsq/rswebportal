﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class GetAllQuestionTypeWiseViewModel
    {
        public object MP_CODE { get; set; }
        public float qslno { get; set; }
        public string qtitle { get; set; }
        public string qtype { get; set; }
        public string ans_date { get; set; }
        public DateTime adate { get; set; }
        public string shri { get; set; }
        public float qno { get; set; }
        public string name { get; set; }
        public string MIN_NAME { get; set; }
        public string qn_text { get; set; }
        public string ans_text { get; set; }
        public int ses_no { get; set; }
        public object depc { get; set; }
        public string Status { get; set; }
        public string p_flag { get; set; }
        public string eng_file { get; set; }
        public string hindi_file { get; set; }
    }

}