﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class GetSupplementrySessionViewModel
    {
        public string ans_date { get; set; }
        public string mp_name { get; set; }
        public string MIN_NAME { get; set; }
        public int slno { get; set; }
        public int ses_no { get; set; }
        public int qno { get; set; }
        public string qtitle { get; set; }
        public List<SessionDate> date { get; set; }
        public int mp_code { get; set; }
        public int min_code { get; set; }
    }

}