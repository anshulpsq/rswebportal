﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class GetAllMinistryWiseViewModel
    {
        public float qslno { get; set; }
        public string qtitle { get; set; }
        public string qtype { get; set; }
        public string ans_date { get; set; }
        public object adate { get; set; }
        public object shri { get; set; }
        public float qno { get; set; }
        public string name { get; set; }
        public string min_name { get; set; }
        public string qn_text { get; set; }
        public string ans_text { get; set; }
        public int ses_no { get; set; }
        public object depc { get; set; }
        public object status { get; set; }
        public object P_flag { get; set; }
        public string files { get; set; }
        public string hindifiles { get; set; }
    }

    //Get Ministry Name
    public class MinistryNameViewModel:SessionViewModel
    {
        public int MIN_CODE { get; set; }
        public string MIN_NAME { get; set; }
        public bool MIN_Status { get; set; }
    }
}
