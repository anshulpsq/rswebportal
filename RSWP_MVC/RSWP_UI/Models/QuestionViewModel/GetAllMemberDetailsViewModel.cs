﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class GetAllMemberDetailsViewModel
    {
        public int MP_CODE { get; set; }
        public string MP_NAME { get; set; }
        public string hMP_NAME { get; set; }
        public string GENDER { get; set; }
        public string STATE_NAME { get; set; }
        public string PARTY_NAME { get; set; }
        public string LADD { get; set; }
        public string LTELE { get; set; }
        public string PADD { get; set; }
        public string PTELE { get; set; }
        public string EMAIL_ID { get; set; }
        public bool MP_CURRENT { get; set; }
        public string MP_LNAME { get; set; }
        public string PARTY_CODE { get; set; }
        public string MP_FNAME { get; set; }
        public string MP_INIT { get; set; }
        public bool C_MINISTER { get; set; }
        public string STATE_CODE { get; set; }
        public int PROF_CODE1 { get; set; }
        public int PROF_CODE2 { get; set; }
        public int PROF_CODE3 { get; set; }
        public string OTHER_PROF_DETAIL { get; set; }
        public bool FREE_STRUGGLE { get; set; }
        public string NO_SONS { get; set; }
        public string NO_DAUGHTER { get; set; }
        public int DIVISION_NO { get; set; }
        public string MPNAME { get; set; }
        public string hMPNAME { get; set; }
        public string OLADD { get; set; }
        public string OLTELE { get; set; }
        public string OPADD { get; set; }
        public string OPTELE { get; set; }
        public string DATE_BIRTH { get; set; }
        public string ALPHABET { get; set; }
        public float MP_MobileNo { get; set; }
        public float MP_MobileNo2 { get; set; }
        public string MPFULLNAME { get; set; }
    }

}