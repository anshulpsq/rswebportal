﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class QuestionNumberWiseViewModel
    {
        public float qslno { get; set; }
        public string qtitle { get; set; }
        public string qtype { get; set; }
        public string ans_date { get; set; }
        public object adate { get; set; }
        public object shri { get; set; }
        public float qno { get; set; }
        public string name { get; set; }
        public string min_name { get; set; }
        public object qn_text { get; set; }
        public object ans_text { get; set; }
        public object ses_no { get; set; }
        public object depc { get; set; }
        public object status { get; set; }
        public object P_flag { get; set; }
        public object files { get; set; }
        public object hindifiles { get; set; }
    }
}
