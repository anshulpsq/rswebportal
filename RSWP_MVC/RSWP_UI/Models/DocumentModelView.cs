﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models
{
    public class DocumentModelView
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public string Section { get; set; }
        public string SubSection { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Ministry { get; set; }
        public string Metadata { get; set; }
        public string session { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public string FileLocation { get; set; }
        public string FileUrl { get; set; }
        public int FileVersion { get; set; }
        public int FileSequence { get; set; }
        public string FileStatus { get; set; }
        public string Language { get; set; }
        public Nullable<bool> IsArchived { get; set; }
        public Nullable<bool> IsDownloadable { get; set; }
        public Nullable<bool> IsPlayable { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> PublishedOn { get; set; }
        public string PublishedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<bool> isApproved { get; set; }
        public Nullable<bool> isPublished { get; set; }
        public int FileSize { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public string Tiltle { get; set; }
        public Nullable<System.DateTime> selectedDate { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }
        public string Time { get; set; }
        public string ArchivedBy { get; set; }
        public Nullable<System.DateTime> ArchivedOn { get; set; }
        public int? SNo { get; set; }
        public string Subject { get; set; }
        public string Source { get; set; }
        public int? PageFrom { get; set; }
        public int? PageTo { get; set; }
        public string EditedBy { get; set; }
        //public string HindiVersionUrl { get; set; }
        //public string FloorVersionUrl { get; set; }
        public string Version { get; set; }
        public string RowType { get; set; }
        
        public string Year { get; set; }
    }
}