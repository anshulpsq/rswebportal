﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSWP_UI.Models.BusinessDataViewModel
{
    public class GetSMListViewModel
    {
        public int mp_code { get; set; }
        public string mp_name { get; set; }
        public int sr_no { get; set; }
        public string subject { get; set; }
        public int sess_no { get; set; }
        public object SM_Text { get; set; }
        public int Admis_Sno { get; set; }
        public string Made { get; set; }
        public string made_Date { get; set; }
        public string Ministry { get; set; }
        public object dt_iss { get; set; }
        public object proposed_dt { get; set; }
        public string MIN_NO { get; set; }
        public string Permitted { get; set; }
        public string Date_Permission { get; set; }
        public int sno_made { get; set; }
    }

}