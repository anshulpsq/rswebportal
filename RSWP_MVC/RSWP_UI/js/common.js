$(document).ready(function(){
    //Gallery Slider
    
    $(function(){
       $('.gallery-slider').bxSlider({
           minSlides: 4,
           maxSlides: 4,
           slideWidth: 400,
           slideMargin: 4,
           moveSlides: 1,
           auto: true
       }); 
        
    });
    
//Scroll Top Page 
   
    
    $(window).on('scroll',function(){
       var scrollTopcount = $(window).scrollTop();  
        if(scrollTopcount > 100) {
            $("#ScrollpageTotop").show();
        }
        else {
            $("#ScrollpageTotop").hide();
        }
    
    });
    
   
    $("#ScrollpageTotop").click(function(){
        $('html, body').animate({scrollTop:0},500);
    });
    
    
    //Increase and Descrease font size
    
    var $affectedElements = $("p ,li, a, body, h1, h2, h3, h4, h5, h6"); // Can be extended, ex. $("div, p, span.someClass")

// Storing the original size in a data attribute so size can be reset
    $affectedElements.each( function(){
        var $this = $(this);
        $this.data("orig-size", $this.css("font-size") );
    });

    $("#btn-increase").click(function(){
        changeFontSize(1);
    })

    $("#btn-decrease").click(function(){
        changeFontSize(-1);
    })

    $("#btn-orig").click(function(){
        $affectedElements.each( function(){
            var $this = $(this);
            $this.css( "font-size" , $this.data("orig-size") );
        });
    })

    function changeFontSize(direction){
        $affectedElements.each( function(){
            var $this = $(this);
            $this.css( "font-size" , parseInt($this.css("font-size"))+direction );
        });
    }

//Extend Middle Section of the page
    
  $("#toggle-btn").click(function(){
        $("div#business-center ,#members,#today-center").toggleClass("extend");
        $('.boxSearchMid h3').toggleClass("active-s");
        
    });
    
   $("#toggle-btn").click(function(){
        $("div#commitees-center").toggleClass("extend-c");
        $('.boxSearchMid h3').toggleClass("active-s");
        
    });
    
 $("#toggle-btn").click(function(){
        $("div#question-center").toggleClass("extend-q");
        $('.boxSearchMid h3').toggleClass("active-s");
        $('.custom-pdf').toggleClass("custom-pdf-css");
        
    });
    
//RS TV Home page Tab
    
    $('.rstv-bt').click(function() { 
         $('#rstv-live').show();
         $('#calender-live').hide();
    });
       
    $('.session-bt').click(function() { 
         $('#rstv-live').hide();
         $('#calender-live').show();
    });
    
    
//Theme change
    $('.fontSizeAnc .a-size-new').click( function() {
        $('#bodybg').css('background-color', '#000');
        $('h5,p,span,h2,h4,li').css('color', '#f6ff14');
        $('ul.list-unstyled.links a').css('color', '#f6ff14');
        $('i.app-icon').css('color', '#f6ff14');
        $('ul.nav.navbar-right a').css('color', '#000');
        $('body.default header .top_row').css('background-color', '#000');
        $('ul.nav.navbar-right a').css('color', '#f6ff14');
        $('img#main-logo').attr('src', 'images/dark-theme-logo.png');
        $('p.h-p').css('color', '#f6ff14');
        $('p.d-p').css('color', '#f6ff14');
        $('span.pignose-calender-top-value').css('color', '#777777');
        $('p.pignose-calender-top-month').css('color', '#000');
        $('.pignose-calender').css('background-color', '#fff');
        $('ul.notice-ul a').css('color', '#f6ff14');
        $('ul.d-block a').css('color', '#f6ff14');
        $('ul#internal-ul a').css('color', '#f6ff14');
        $('div#main-buttons button').removeClass('btn-default');
        $('div#main-buttons button').addClass('btn-link-n');
                     
        });
     
    $('.fontSizeAnc .a-size-white').click( function() {
            $('#bodybg').css('background-color', '#fff');
            $('ul.nav.navbar-nav a').css('color', '#fff');
            $('h5,h4').css('color', '#084388');
            $('ul.list-unstyled.links a').css('color', '#fff');
            $('li.main-li.li-border a').css('color', '#fff');
            $('ul.list-unstyled.links li a, ul.list-unstyled.links li').css('color', '#fff');
            $('.notice-ul li a').css('color', '#000');
            $('ul.d-block li a').css('color', '#000');
            $('ul#internal-ul li a').css('color', '#000');
            $('.discover_heading a').css('color', '#4a4a4a');
            $('p,h2').css('color', '#000');
            $('p#bottom-p').css('color', '#fff');
            $('div.box-r-s > a').css('color', '#000');
            $('div.discover_heading.r-s-app span,div.discover_heading.r-s-app i').css('color', '#737373');
            $('body.default header .top_row').css('background-color', '#fff');
            $('ul.nav.navbar-right a').css('color', '#000');
            $('img#main-logo').attr('src', 'images/new-logo-rs.png');
            $('div#main-buttons button').css('color','black');
        
        
            $(".media-body ul  a").hover(function() {
                $(this).css("color","#1A97DC;");
            });
            $('p.h-p').css('color', '#084388');
            $('p.d-p').css('color', 'red');
            $('ul.notice-ul a').css('color', '#4a4a4a');
            $('ul.d-block a').css('color', '#4a4a4a');
            $('ul#internal-ul a').css('color', '#4a4a4a');
            $('div#main-buttons button').removeClass('btn-link-n');
            $('div#main-buttons button').addClass('btn-default');

        });
    
    //Calender JS
    
    $('.calender').pignoseCalender({
			select: function(date, obj) {
				/*obj.calender.parent().next().show().text('You selected ' + 
				(date[0] === null? 'null':date[0].format('YYYY-MM-DD')) +
				'.');*/
			}
		});
    
    //
    
    // get the number of .child elements
    var totalitems = $("#internal-ul .main-li").length;
    // get the height of .child
    var scrollval = $('.main-li').height();
    // work out the total height.
    var totalheight = (totalitems*scrollval)-($("#parent-ul").height());
    
    $(document).on("click", "#down",function(){
        var currentscrollval = $('#internal-ul').scrollTop();
        
        $('#internal-ul').scrollTop(scrollval+currentscrollval);

        // hide/show buttons
        
    });
     $(document).on("click", "#up",function(){
         var currentscrollval = parseInt($('#internal-ul').scrollTop());
         
        $('#internal-ul').scrollTop(currentscrollval-scrollval);
         
        
    });
     
     
$(document).on("click", "#down-1",function(){
        var currentscrollval = $('#internal-ul-left').scrollTop();
        
        $('#internal-ul-left').scrollTop(scrollval+currentscrollval);

        // hide/show buttons
        
    });
     $(document).on("click", "#up-1",function(){
         var currentscrollval = parseInt($('#internal-ul-left').scrollTop());
         
        $('#internal-ul-left').scrollTop(currentscrollval-scrollval);
         
        
    });
     
// Question Pending Session selecton
    
$("#select-sesson select").on('change',function(){
   $("#session-wise").show(); 
});

       
});