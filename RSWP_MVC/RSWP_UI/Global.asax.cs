﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RSWP_UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private System.Timers.Timer EngineTimer;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //EngineTimer = new System.Timers.Timer(86400000);
            //EngineTimer = new System.Timers.Timer(2500000);
            ////EngineTimer = new System.Timers.Timer(60000);
            //EngineTimer.Elapsed += EngineTimer_Elapsed;
            //EngineTimer.Enabled = true;
        }

        private void EngineTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //call you function here
            RSWP_UI.Common.ApiResolvercs _apiResolver = new Common.ApiResolvercs();
            
            //_apiResolver.ReadApi();
            //EngineTimer = new System.Timers.Timer(86400000);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];
            if (cookie != null && cookie.Value != null)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
                
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
                
            }
        }

        protected void Session_Start()
        {
            Session.Timeout = 5000;
            Session["WebLinks"] = "";
            Session["WebLinksImage"] = "";

            if (EngineTimer == null)
            {
                //EngineTimer = new System.Timers.Timer(86400000);
                EngineTimer = new System.Timers.Timer(25000);
                //EngineTimer = new System.Timers.Timer(60000);
                EngineTimer.Elapsed += EngineTimer_Elapsed;
                EngineTimer.Enabled = true;
            }
        }
    }
}
