﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.BusinessDataViewModel;
using RSWP_UI.Models.LegislationViewModel;
using RSWP_UI.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class BusinessController : BaseController
    {
        RS_DBEntities db;
        string[] months;
        public BusinessController()
        {
            db = new RS_DBEntities();
            months = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        }
        public ActionResult Index()
        {
            return RedirectToAction("ListOfBussiness");
        }
        public ActionResult ListOfBussiness()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();
            var sittingMasters = db.sittingMasters.ToList();
            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();

            foreach (var item in sittingMasters)
            {
                if (!model.Sessions.Contains(item.sessionNo))

                    model.Sessions.Add(item.sessionNo);
            }
            if (sittingMasters.Count > 0)
            {
                model.Sessions = model.Sessions.OrderByDescending(x => x).ToList();
                model.SittingMonths = GetSessionWiseSittings(model.Sessions.First(), "TableOffice", "TableBusiness");
                if (model.SittingMonths.Count > 0)
                {
                    model.Documents = GetDocuments(Convert.ToInt32(model.SittingMonths.First().Value), model.Sessions.First().ToString(), "TableOffice", "TableBusiness");
                }
            }
            return View(model);
        }
        public ActionResult GetJsonSessionSittings(int sessionNo, string section, string subSection)
        {
            return Json(GetSessionWiseSittings(sessionNo, section, subSection), JsonRequestBehavior.AllowGet);
        }
        public List<SelectListItem> GetSessionWiseSittings(int sessionNo, string section, string subSection)
        {
            List<SelectListItem> sittingMonths = new List<SelectListItem>();
            List<int> selectedMonths = new List<int>();
            List<sittingMaster> sessionWiseSittings = db.sittingMasters.Where(x => x.sessionNo == sessionNo).ToList();
            foreach (var item in sessionWiseSittings)
            {
                if (!selectedMonths.Contains(item.SittingDate.Month))
                {

                    selectedMonths.Add(item.SittingDate.Month);
                    sittingMonths.Add(new SelectListItem
                    {
                        Value = item.SittingDate.Month.ToString(),
                        Text = months[item.SittingDate.Month - 1]
                    });
                }
            }
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var month in sittingMonths)
            {
                List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo.ToString() &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
                if (documents.Any(x => x.selectedDate.Value.Month == Convert.ToInt32(month.Value)))
                    result.Add(month);
            }
            return result.OrderByDescending(x => Convert.ToInt32(x.Value)).ToList();
        }
        public ActionResult GetJsonDocuments(int month, string sessionNo, string section, string subSection)
        {
            return Json(GetDocuments(month, sessionNo, section, subSection), JsonRequestBehavior.AllowGet);
        }
        public List<Document> GetDocuments(int month, string sessionNo, string section, string subSection)
        {
            List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
            List<Document> docsByMonth = new List<Document>();
            foreach (var doc in documents)
            {
                if (doc.selectedDate.HasValue && doc.selectedDate.Value.Month == month)
                    docsByMonth.Add(doc);
            }
            List<Document> resultDocs = new List<Document>();
            if (section.Trim() == "TableOffice" && subSection.Trim() == "TableBusiness")
            {
                List<string> FileTypes = new List<string> { "LOB", "RLOB", "SLOB", "SLOB-2" };
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ThenBy(x => FileTypes.IndexOf(x.FileType)).ToList();
            }
            else if (section.Trim() == "Synopsis" && subSection.Trim() == "SynopsisUpload")
            {
                List<string> FileTypes = new List<string> { "synopsis", "supplement-1", "supplement-2" };
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ThenBy(x => FileTypes.IndexOf(x.FileType)).ToList();
            }
            else
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ToList();

            return resultDocs;
        }
        public ActionResult PaperToBeLaid()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();
            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();

            var sittingMasters = db.sittingMasters.ToList();
            foreach (var item in sittingMasters)
            {
                if (!model.Sessions.Contains(item.sessionNo))
                    model.Sessions.Add(item.sessionNo);
            }
            if (sittingMasters.Count > 0)
            {
                model.Sessions = model.Sessions.OrderByDescending(x => x).ToList();
                model.SittingMonths = GetSessionWiseSittings(model.Sessions.First(), "TableOffice", "TableLaid");
                if (model.SittingMonths.Count > 0)
                {
                    model.Documents = GetDocuments(Convert.ToInt32(model.SittingMonths.First().Value), model.Sessions.First().ToString(), "TableOffice", "TableLaid");
                }
            }
            return View(model);
        }
        public ActionResult BulletinPart1()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();
            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();

            var sittingMasters = db.sittingMasters.ToList();
            foreach (var item in sittingMasters)
            {
                if (!model.Sessions.Contains(item.sessionNo))
                    model.Sessions.Add(item.sessionNo);
            }
            if (sittingMasters.Count > 0)
            {
                model.Sessions = model.Sessions.OrderByDescending(x => x).ToList();
                model.SittingMonths = GetSessionWiseSittings(model.Sessions.First(), "TableOffice", "TableBulletin1");
                if (model.SittingMonths.Count > 0)
                {
                    model.Documents = GetDocuments(Convert.ToInt32(model.SittingMonths.First().Value), model.Sessions.First().ToString(), "TableOffice", "TableBulletin1 ");
                }
            }
            return View(model);
        }
        public ActionResult Synopsis()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();
            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();

            var sittingMasters = db.sittingMasters.ToList();
            foreach (var item in sittingMasters)
            {
                if (!model.Sessions.Contains(item.sessionNo))
                    model.Sessions.Add(item.sessionNo);
            }
            if (sittingMasters.Count > 0)
            {
                model.Sessions = model.Sessions.OrderByDescending(x => x).ToList();
                model.SittingMonths = GetSessionWiseSittings(model.Sessions.First(), "Synopsis", "SynopsisUpload");
                if (model.SittingMonths.Count > 0)
                {
                    model.Documents = GetDocuments(Convert.ToInt32(model.SittingMonths.First().Value), model.Sessions.First().ToString(), "Synopsis", "SynopsisUpload ");
                }
            }
            return View(model);
        }
        public ActionResult GetJsonDocs(string sessionNo, string section, string subSection)
        {
            return Json(GetDocs(sessionNo, section, subSection), JsonRequestBehavior.AllowGet);
        }
        public List<Document> GetDocs(string sessionNo, string section, string subSection)
        {
            List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
            return documents;
        }
        public ActionResult SessionalResume()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();

            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();

            if (model.SessionList.Count > 0)
                model.Documents = GetDocs(model.SessionList.First().sessionNo.ToString(), "TableOffice", "TableResume");

            return View(model);
        }
        public ActionResult VeledectoryAddress()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();

            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();
            if (model.SessionList.Count > 0)
                model.Documents = GetDocs(model.SessionList.First().sessionNo.ToString(), "TableOffice", "TableAddress");

            return View(model);
        }
        public ActionResult StatisticalInformation()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();

            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();
            if (model.SessionList.Count > 0)
                model.Documents = GetDocs(model.SessionList.First().sessionNo.ToString(), "TableOffice", "TableInformation");

            return View(model);
        }
        public ActionResult ProvisionalCalender()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();

            model.SessionList = db.sessionsMasters.ToList().OrderByDescending(x => x.sessionNo).ToList();
            if (model.SessionList.Count > 0)
                model.Documents = GetDocs(model.SessionList.First().sessionNo.ToString(), "TableOffice", "TableCalender");

            return View(model);
        }


        public ActionResult GetLegislativeIntroductionUrl()
        {
            string introductionUrl = null;
            try
            {
                introductionUrl = db.Documents.Where(t => t.Section.Equals("BillOffice") && t.SubSection.Equals("Introduction")
                                          && t.IsArchived == false).FirstOrDefault()?.FileUrl;

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went Wrong";
            }
            return Json(new { IntroductionUrl = introductionUrl ?? "#" }, JsonRequestBehavior.AllowGet);
        }

        #region Bulletin2
        public ActionResult BulletinPart2()
        {
            TableBulletin2ViewModel model = new TableBulletin2ViewModel();

            try
            {
                model = GetBulletin2Data();
                var bulletinList = GetBulletinList();
                model.BulletinNoList = bulletinList.Select(x => new SelectListItem { Text = x.BulletinNo, Value = x.BulletinNo }).ToList();

                List<SectionNameViewModel> sectionNameList = GetAllSectionName();

                if (sectionNameList != null && sectionNameList.Count > 0)
                    model.SectionList = sectionNameList.Select(x => new SelectListItem() { Text = x.Section_Name, Value = x.Section_Name }).ToList();

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/BulletinPart2", line);
            }
            return View(model);
        }
        /// <summary>
        /// Fetching bulletin 2 data and grouping them by date
        /// </summary>
        /// <param name="month"></param>For 0 as default vlue all records will be fetched
        /// <param name="year"></param>For 0 as default vlue all records will be fetched
        /// <param name="filter"></param>For null as default vlue all records will be fetched and no filter will be applied
        /// <returns></returns>
        /// 
        private TableBulletin2ViewModel GetBulletin2Data(int month = 0, int year = 0, AdvanceSearchViewModel filterModel = null)
        {
            TableBulletin2ViewModel model = new TableBulletin2ViewModel();
            try
            {
                List<BULL_MAIN> bullentinList = new List<BULL_MAIN>();

                if (month > 0 && year > 0)
                {
                    //Month and year filter applied
                    bullentinList = db.BULL_MAIN.Where(x => x.date.Month == month && x.date.Year == year).OrderByDescending(x => x.date).AsQueryable().ToList();
                }
                else if (filterModel == null)
                {
                    //no filter
                    bullentinList = db.BULL_MAIN.OrderByDescending(x => x.date).AsQueryable().ToList();
                }
                else
                {
                    //Advance filter applied
                    bullentinList = GetFilterBulletin2Data(filterModel);
                }


                if (bullentinList == null)
                {
                    bullentinList = new List<BULL_MAIN>();
                }


                var data = bullentinList.GroupJoin(db.Documents.Where(x => x.Section == CommanConstant.TableOffice && x.SubSection == CommanConstant.Bulletin2 && x.IsArchived == false).ToList()
                  , doc => doc.date, b => b.selectedDate, (bulletinList, documents) => new { Document = documents, Bulletin = bulletinList }).GroupBy(z => z.Bulletin.date).AsQueryable();


                foreach (var item in data)
                {
                    BulletinListVM bulletinVM = new BulletinListVM();
                    bool engFlag = false;
                    bool hinFlag = false;
                    foreach (var innerItem in item)
                    {
                        var englishDoc = innerItem.Document.Where(x => x.Language == CommanConstant.English).Select(x => new DocumentModelView() { FileSize = x.FileSize > 0 ? x.FileSize / 1000 : 0, Name = x.Name, Language = x.Language, FileUrl = x.FileUrl, Section = x.Section, SubSection = x.SubSection, Id = x.Id, selectedDate = x.selectedDate }).FirstOrDefault();
                        var hindiDoc = innerItem.Document.Where(x => x.Language == CommanConstant.Hindi).Select(x => new DocumentModelView() { FileSize = x.FileSize > 0 ? x.FileSize / 1000 : 0, Name = x.Name, Language = x.Language, FileUrl = x.FileUrl, Section = x.Section, SubSection = x.SubSection, Id = x.Id, selectedDate = x.selectedDate }).FirstOrDefault();
                        if (englishDoc != null && engFlag == false)
                        {
                            engFlag = true;
                            bulletinVM.EnglishDocument = englishDoc;
                        }
                        if (hindiDoc != null && hinFlag == false)
                        {
                            hinFlag = true;
                            bulletinVM.HindiDocument = hindiDoc;
                        }
                        bulletinVM.SelectedDate = innerItem.Bulletin.date;
                    }
                    bulletinVM.BulletinList.AddRange(item.Select(x => new Bulletin2ViewModel { BulletinNo = x.Bulletin.Item_No, Date = x.Bulletin.date, FileName = x.Bulletin.file_name, HtmlText = x.Bulletin.Item_Text, SubjectName = x.Bulletin.Item_Title, SectionName = x.Bulletin.Section, SGName = x.Bulletin.sgname, SGDesignation = x.Bulletin.desgn }));

                    model.Bulletins.Add(bulletinVM);
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetBulletin2Data", line);
            }
            return model;
        }

        [HttpPost]
        public ActionResult GetBulletinPart2TableData(int month = 0, int year = 0, AdvanceSearchViewModel filterModel = null)
        {
            TableBulletin2ViewModel model = new TableBulletin2ViewModel();
            try
            {
                model = GetBulletin2Data(month, year, filterModel);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetBulletinPart2TableData", line);
            }
            return PartialView("_bulletin2Table", model);
        }

        public List<Bulletin2ViewModel> GetBulletinList()
        {
            List<Bulletin2ViewModel> bulletinList = new List<Bulletin2ViewModel>();
            try
            {
                bulletinList = db.BULL_MAIN.Select(x => new Bulletin2ViewModel
                {
                    BulletinNo = x.Item_No.Trim(),
                    Date = x.date,
                    FileName = x.file_name,
                    HtmlText = x.Item_Text,
                    SubjectName = x.Item_Title.Trim(),
                    SectionName = x.Section.Trim(),
                    SGName = x.sgname.Trim(),
                    SGDesignation = x.desgn.Trim()
                }).AsQueryable().ToList();
                if (bulletinList == null)
                    bulletinList = new List<Bulletin2ViewModel>();
                return bulletinList;
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetBulletinList", line);
            }
            return bulletinList;
        }

        /// <summary>
        /// Advance search ,fetch filtered data
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        public List<BULL_MAIN> GetFilterBulletin2Data(AdvanceSearchViewModel filterModel)
        {
            List<BULL_MAIN> bulletinFilteredData = new List<BULL_MAIN>();
            try
            {
                SqlParameter from = new SqlParameter("@fromDate", (object)filterModel.From ?? DBNull.Value);
                SqlParameter to = new SqlParameter("@toDate", (object)filterModel.To ?? DBNull.Value);
                SqlParameter bulletinNo = new SqlParameter("@bulletinNo", (object)filterModel.BulletinNo ?? DBNull.Value);
                SqlParameter sectionName = new SqlParameter("@sectionName", (object)filterModel.Section ?? DBNull.Value);
                SqlParameter searchInTitle = new SqlParameter("@searchInTitle", (object)filterModel.SearchTitle ?? DBNull.Value);
                SqlParameter searchInText = new SqlParameter("@searchInText", (object)filterModel.SearchHtml ?? DBNull.Value);
                SqlParameter sortBy = new SqlParameter("@sortBy", (object)filterModel.SortOn ?? DBNull.Value);
                bulletinFilteredData = db.Database.SqlQuery<BULL_MAIN>("sp_GetFilteredBulletin2Data @fromDate,@toDate,@bulletinNo,@sectionName,@searchInTitle,@searchInText,@sortBy",
                      from, to, bulletinNo, sectionName, searchInTitle, searchInText, sortBy).ToList();
                if (bulletinFilteredData == null)
                {
                    bulletinFilteredData = new List<BULL_MAIN>();
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetFilterBulletin2Data", line);
            }
            return bulletinFilteredData;
        }

        private List<SectionNameViewModel> GetAllSectionName()
        {
            List<SectionNameViewModel> sectionNameList = new List<SectionNameViewModel>();
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string apiUrlGetSectionName = ConfigurationManager.AppSettings["ApiUrl"] + "RssOrgChart/GetSectionName";
                //Getting all Section Name from API
                HttpResponseMessage response = client.GetAsync(apiUrlGetSectionName).Result;
                if (response != null && response.IsSuccessStatusCode)
                {
                    sectionNameList = JsonConvert.DeserializeObject<List<SectionNameViewModel>>(response.Content.ReadAsStringAsync().Result, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    if (sectionNameList == null)
                        sectionNameList = new List<SectionNameViewModel>();
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetAllSectionName", line);
            }
            return sectionNameList;
        }

        #endregion

        public ActionResult GetExternalLink()
        {
            try
            {
                List<ExterLinkViewModel> externalLinks = new List<ExterLinkViewModel>();
                SqlParameter paramId = new SqlParameter("@Id", DBNull.Value);
                externalLinks = db.Database.SqlQuery<ExterLinkViewModel>("sp_GETExternalLinks @Id", paramId).ToList();
                if (externalLinks == null)
                {
                    externalLinks = new List<ExterLinkViewModel>();
                }
                Dictionary<string, string> dictionaryExternalLinks = new Dictionary<string, string>();
                foreach (var item in externalLinks)
                {
                    string dictionaryKey = item.SectionName.Trim().ToLower().Replace(" ", "") + "_" + item.SubSectionName.Trim().ToLower().Replace(" ", "") + "_" + item.ModuleName.Trim().ToLower().Replace(" ", "");
                    dictionaryExternalLinks.Add(dictionaryKey, item.URL);
                }
                return Json(new { ExternalLinkData = dictionaryExternalLinks }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Statements
        public ActionResult Statements(string type)
        {
            StatementListVM sessionWiseModel = new StatementListVM();
            StatementsDateWiseVM dateWiseModel = new StatementsDateWiseVM();
            try
            {
                var data = db.Statements.Where(x => x.IsDeleted == false && x.LegislativeBusiness == type).ToList().AsQueryable().Join(db.BULL_MAIN.ToList().AsQueryable(), s => s.BulletinNo.Trim(), b => b.Item_No.Trim(),
               (statement, bulletin) => new StatementsViewModel()
               {
                   LegislativeBusiness = statement.LegislativeBusiness,
                   BulletinNo = statement.BulletinNo,
                   WeekEndingDate = statement.WeekEndingDate,
                   SessionNo = statement.SessionNo,
                   BulletinData = new Bulletin2ViewModel()
                   {
                       BulletinNo = bulletin.Item_No,
                       Date = bulletin.date,
                       SectionName = bulletin.Section,
                       SGName = bulletin.sgname,
                       SGDesignation = bulletin.desgn,
                       SubjectName = bulletin.Item_Title,
                       HtmlText = bulletin.Item_Text,
                   }

               }).AsQueryable().ToList();
                if (type == CommanConstant.StatuaryOrderLaid)
                {
                    dateWiseModel.Type = Resource.StatuaryOrderlaid;
                    sessionWiseModel.Type = Resource.StatuaryOrderlaid;
                }
                else if (type == CommanConstant.WeeklyProgressOfBills)
                {
                    dateWiseModel.Type = Resource.WeeklyProgressofBills;
                    sessionWiseModel.Type = Resource.WeeklyProgressofBills;
                }
                else if (type == CommanConstant.GovernmentLegBusinessToBeTakenUp)
                {
                    dateWiseModel.Type = Resource.GovernmentlegBusinesstobeTakenUp;
                    sessionWiseModel.Type = Resource.GovernmentlegBusinesstobeTakenUp;

                }
                else if (type == CommanConstant.BillsPendingAtTheEndOfSession)
                {
                    dateWiseModel.Type = Resource.BillsPendingattheEndofSession;
                    sessionWiseModel.Type = Resource.BillsPendingattheEndofSession;
                }
                else if (type == CommanConstant.BillsPassedByTheHouseOfParliament)
                {
                    dateWiseModel.Type = Resource.BillsPassedbytheHouseofParliament;
                    sessionWiseModel.Type = Resource.BillsPassedbytheHouseofParliament;
                }

                if (type == CommanConstant.StatuaryOrderLaid || type == CommanConstant.WeeklyProgressOfBills)
                {
                    //Grouping data session wise
                    var groupedData = data.GroupBy(x => x.SessionNo).ToList().AsQueryable();
                    foreach (var group in groupedData)
                    {
                        StatementListVM statement = new StatementListVM();
                        statement.SessionNumber = group.Key;
                        statement.StatementList.AddRange(group);
                        dateWiseModel.DateWiseStatement.Add(statement);
                    }
                }
                else
                {
                    sessionWiseModel.StatementList = data;
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/Statements", line);
            }
            if (type == CommanConstant.StatuaryOrderLaid || type == CommanConstant.WeeklyProgressOfBills)
            {
                return PartialView("_StatementsDateWise", dateWiseModel);
            }
            else
            {
                return PartialView("_StatementSessionWise", sessionWiseModel);
            }
        }

        #endregion

        #region Session's Journal
        public ActionResult SessionJournal(int sessionNo = 0)
        {
            SessionJournalViewModel model = new SessionJournalViewModel();
            try
            {
                model = GetSessionJournalGridData(sessionNo);
                if (model == null)
                    model = new SessionJournalViewModel();
                model.Sessions = GetSessionNoList();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/SessionJournal", line);
            }
            return View(model);
        }

        public ActionResult GetSessionJournalGridContent(int sessionNo)
        {
            SessionJournalViewModel model = GetSessionJournalGridData(sessionNo);
            try
            {
                if (model == null)
                    model = new SessionJournalViewModel();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetSessionJournalGridContent", line);
            }
            return PartialView("_sessionJournalGrid", model.GridData);
        }

        private SessionJournalViewModel GetSessionJournalGridData(int sessionNo)
        {
            SessionJournalViewModel model = new SessionJournalViewModel();
            try
            {
                sessionsMaster sessionData = new sessionsMaster();
                if (sessionNo == 0)
                {
                    //Getting data of latest session
                    sessionData = db.sessionsMasters.FirstOrDefault(x => x.sessionNo == db.sessionsMasters.Max(y => y.sessionNo));
                }
                else
                {
                    sessionData = db.sessionsMasters.FirstOrDefault(x => x.sessionNo == sessionNo);
                }
                model.GridData = db.MainPageSessionJournals.Where(x => x.SessionNo == sessionData.sessionNo).Select(x => new SessionJournalGridViewModel() { HTMLContent = x.Content }).FirstOrDefault();
                if (model.GridData != null)
                {
                    model.GridData.SessionNo = sessionData.sessionNo;
                    model.GridData.FromDate = sessionData.StartDate;
                    model.GridData.ToDate = sessionData.EndDate;
                    model.GridData.SessionNoInWords = Utility.NumberToWords(sessionData.sessionNo)?.ToUpper();
                }
                else
                {
                    model.GridData = new SessionJournalGridViewModel();
                    model.GridData.SessionNo = sessionData.sessionNo;
                }
                model.GridData.DocumentList = db.Documents.Where(x => x.session == sessionData.sessionNo.ToString() && x.Section == CommanConstant.Lobby & x.SubSection == CommanConstant.SessionJournal && x.IsArchived == false).OrderBy(x=>x.selectedDate).Select(x => new DocumentModelView()
                {
                    selectedDate = x.selectedDate,
                    Name = x.Name,
                    FileUrl = x.FileUrl,
                    Language = x.Language,
                    session = x.session,
                    FileSize = x.FileSize
                }).ToList();

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetSessionJournalGridData", line);
            }

            return model;
        }

        public List<SelectListItem> GetSessionNoList()
        {
            List<SelectListItem> SessionList = new List<SelectListItem>();
            try
            {
                SessionList = db.sessionsMasters.OrderByDescending(x => x.sessionNo).Select(x => new SelectListItem() { Text = x.sessionNo.ToString(), Value = x.sessionNo.ToString() }).AsQueryable().ToList();
                if (SessionList == null)
                    SessionList = new List<SelectListItem>();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetSessionNoList", line);
            }
            return SessionList;
        }

        #endregion


        #region Paper Laid In Rajyasabh
        public ActionResult PaperToBeLaidRajyaSabha()
        {
            PaperLaidInRSViewModel model = new PaperLaidInRSViewModel();
            try
            {
                model.SessionList = GetSessionNoList();
                if (model.SessionList == null)
                    model.SessionList = new List<SelectListItem>();
                model.SessionList.Insert(0, new SelectListItem() { Text = Resource.All, Value = "" });
                model.ReportTypeList = GetReportTypes();
                model.MinistryList = GetMinistryNames();
                //int maxSessionNo = db.sessionsMasters.Max(x => x.sessionNo);
                //var datesList = GetJsonSittingDatesBySessionNo(maxSessionNo)?.Data;

                //if (datesList != null)
                //{
                //    model.SittingDates = JsonConvert.DeserializeObject<List<SelectListItem>>(JsonConvert.SerializeObject(datesList));
                //}
                model.PaperLaidInRSList = GetPaperLaidInRSData(true, LanguageMang.CurrentCultureLanguage, null, null, null, null, null, null, null);
                if (model.PaperLaidInRSList == null)
                    model.PaperLaidInRSList = new List<PaperLaindInRSGridViewModel>();

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/PaperToBeLaidRajyaSabha", line);
            }
            return View(model);
        }
        private List<SelectListItem> GetReportTypes()
        {
            List<SelectListItem> reportTypes = new List<SelectListItem>();
            try
            {
                using (RS_DBEntities db = new RS_DBEntities())
                {

                    string language = LanguageMang.CurrentCultureLanguage;
                    reportTypes = db.ReportTypes.Where(x => x.IsDeleted == false && x.Language == language).Select(x => new SelectListItem() { Text = x.Name, Value = x.Name }).ToList();
                    if (reportTypes == null)
                        reportTypes = new List<SelectListItem>();
                    reportTypes.Insert(0, new SelectListItem() { Text = Resource.Select, Value = "" });
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetReportTypes", line);
            }
            return reportTypes;
        }

        private List<SelectListItem> GetMinistryNames()
        {
            List<SelectListItem> ministryNames = new List<SelectListItem>();
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string apiUrlGetSectionName = ConfigurationManager.AppSettings["ApiUrl"] + "question/GetAllministary";
                //Getting all Section Name from API
                HttpResponseMessage response = client.GetAsync(apiUrlGetSectionName).Result;
                if (response != null && response.IsSuccessStatusCode)
                {
                    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(response.Content.ReadAsStringAsync().Result, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                    ministryNames = data.Select(x => new SelectListItem() { Text = x.MIN_NAME, Value = x.MIN_NAME }).ToList();
                }
                if (ministryNames == null)
                    ministryNames = new List<SelectListItem>();
                ministryNames.Insert(0, new SelectListItem() { Text = Resource.Select, Value = "" });
            }
            catch (Exception ex)
            {

                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetMinistryNames", line);
            }
            return ministryNames;
        }

        public JsonResult GetJsonSittingDatesBySessionNo(int sessionNo)
        {
            List<SelectListItem> sittingdateList = new List<SelectListItem>();
            try
            {
                using (RS_DBEntities db = new RS_DBEntities())
                {
                    var SittingDateLists = db.sittingMasters.Where(m => m.sessionNo == sessionNo).ToList();
                    sittingdateList = SittingDateLists.Select(m => new SelectListItem()
                    {
                        Text = m.SittingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Value = m.SittingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                    }).ToList();
                    if (sittingdateList == null)
                        sittingdateList = new List<SelectListItem>();
                    sittingdateList.Insert(0, new SelectListItem() { Text = Resource.Select, Value = "" });
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetJsonSittingDates", line);
            }
            return Json(sittingdateList, JsonRequestBehavior.AllowGet);
        }

        private List<PaperLaindInRSGridViewModel> GetPaperLaidInRSData(Nullable<bool> displayAllRecords, string language, string sessionNo, Nullable<System.DateTime> fromDate, Nullable<DateTime> toDate, string ministry, string reportType, string titleToSearch, string sortBy)
        {
            List<PaperLaindInRSGridViewModel> paperLaidData = new List<PaperLaindInRSGridViewModel>();
            try
            {
                using (RS_DBEntities db = new RS_DBEntities())
                {
                    SqlParameter paramDisplayRecord = new SqlParameter("@DisplayAllRecords", displayAllRecords);
                    SqlParameter paramLanguage = new SqlParameter("@Language", (object)language ?? DBNull.Value);
                    SqlParameter paramSessionNo = new SqlParameter("@SessionNo", (object)sessionNo ?? DBNull.Value);
                    SqlParameter paramFromDate = new SqlParameter("@FromDate", (object)fromDate ?? DBNull.Value);
                    SqlParameter paramToDate = new SqlParameter("@ToDate", (object)toDate ?? DBNull.Value);
                    SqlParameter paramMinistry = new SqlParameter("@Ministry", (object)ministry ?? DBNull.Value);
                    SqlParameter paramReportType = new SqlParameter("@ReportType", (object)reportType ?? DBNull.Value);
                    SqlParameter paramTitleToSearch = new SqlParameter("@TitleToSearch", (object)titleToSearch ?? DBNull.Value);
                    SqlParameter paramSortBy = new SqlParameter("@SortBy", (object)sortBy ?? DBNull.Value);
                    paperLaidData = db.Database.SqlQuery<PaperLaindInRSGridViewModel>("sp_GetPaperLaidRecords @DisplayAllRecords,@Language,@SessionNo,@FromDate,@ToDate,@Ministry,@ReportType,@TitleToSearch,@SortBy",
                         paramDisplayRecord, paramLanguage, paramSessionNo, paramFromDate, paramToDate, paramMinistry, paramReportType, paramTitleToSearch, paramSortBy).ToList();
                    if (paperLaidData == null)
                        paperLaidData = new List<PaperLaindInRSGridViewModel>();
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/GetPaperLaidInRSData", line);

            }
            return paperLaidData;
        }

        public ActionResult GetPaperToBeLaidRajyaSabhaGridData(AdvanceSearchViewModel filterModel)
        {
            PaperLaidInRSViewModel model = new PaperLaidInRSViewModel();
            try
            {

                model.PaperLaidInRSList = GetPaperLaidInRSData(filterModel.DisplayAllRecords, LanguageMang.CurrentCultureLanguage, filterModel.SessionNo, filterModel.From, filterModel.To, filterModel.Ministry, filterModel.ReportType, filterModel.DocTitleSearch, filterModel.SortOn);
                if (model.PaperLaidInRSList == null)
                    model.PaperLaidInRSList = new List<PaperLaindInRSGridViewModel>();

            }
            catch (Exception ex)
            {
                model.Message = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/PaperToBeLaidRajyaSabha", line);
            }
            return PartialView("_PaperLaidInRSGrid", model);
        }

        public ActionResult MatterRaisedWithPermission()
        {
            using (RS_DBEntities db = new RS_DBEntities())
            {
                var documentList = db.Documents.Where(x => x.Section.Equals("Debates") && x.SubSection.Equals("Discussions") && x.Type.Equals("Matters Raised With Permission (Zero Hour)") && x.IsArchived == false).ToList();
                ViewBag.List = documentList;
            }
            return View();
        }
        #endregion


        #region API Integrate for LEGISLATION & Special Mentions
        // GET: Session Value For DropDown
        public string firstSession = "";
        public string checkData = "";
        public void GetSessionVal()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                firstSession = data[0].Session_value.ToString();
                TempData["SessionList"] = new SelectList(data, "session_code", "Session_value");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Member Name For DropDown
        public void GetMemberList()
        {
            List<GetAllMemberDetailsViewModel> data = new List<GetAllMemberDetailsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["MemList"] = new SelectList(data, "MP_CODE", "MPFULLNAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Ministry Name For DropDown
        public void GetMinistryList()
        {
            List<MinistryNameViewModel> data = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm = client.GetAsync("api_new/question/GetAllministary").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["MinistryList"] = new SelectList(data, "MIN_CODE", "MIN_NAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Bill Introduced(During Session)
        public ActionResult BillIntroduced()
        {
            GetSessionVal();
            return View();
        }
        [HttpPost]
        public JsonResult BillIntroduced_Result(string sessionVal)
        {
            List<GetAll_IntroducedbillViewModel> data = new List<GetAll_IntroducedbillViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetAllBillIntroduced_Sessionwise?sessionno=" + sessionVal;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_IntroducedbillViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["IntroduceBill"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bill Passed(During Session)
        public ActionResult BillPassed()
        {
            GetSessionVal();
            return View();
        }
        [HttpPost]
        public JsonResult BillPass_Result(string sessionVal)
        {
            List<GetAll_PassbillViewModel> data = new List<GetAll_PassbillViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetBill_Sessionwise?sessionno=" + sessionVal;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_PassbillViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PassBill"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bills Search (Advanced)
        public ActionResult BillSearch()
        {
            List<MinistryNameViewModel> data = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseTerm = client.GetAsync("api_new/question/GetAllministary").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.MinistryList = new SelectList(data, "MIN_CODE", "MIN_NAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public JsonResult BillSearchResult(string house, string type, string status)
        {
            List<GetAll_BillSearchViewModel> data = new List<GetAll_BillSearchViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetBill_All_byDefault";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_BillSearchViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["BillSearchData"] = data;
                //foreach(var item in data)
                //{
                //    if(item.House== house || house== "Both Houses")
                //    {
                //        if(item.type == type || type == "All")
                //        {
                //            if(item.status == status || status == "All")
                //            {

                //            }
                //        }
                //    }
                //}
                return Json(data.Where(x => x.House == house && x.type == type && x.status == status), JsonRequestBehavior.AllowGet);
                //return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: SPECIAL MENTIONS
        [HttpGet]
        public ActionResult SM_Search_Form()
        {
            GetSessionVal();
            GetMemberList();
            GetMinistryList();
            return View();
        }
        [HttpPost]
        public ActionResult SM_Search_Form(GetSMListViewModel model)
        {
            GetSMResult(model.sess_no.ToString(), model.mp_code, model.MIN_NO);
            return RedirectToAction("SM_Searchs_Form");
        }
        [HttpGet]
        public ActionResult SM_Searchs_Form()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetRecord(string Id)
        {
            TempData["SM_Id"] = Id;
            return PartialView("GetSMRecordPartialViewModel");
        }
        public ActionResult GetSMRecordPartialViewModel()
        {
            return View();
        }
        public void GetSMResult(string sessionVal, int mp_code, string min_code)
        {
            int count = 0;
            TempData["mp_code"] = mp_code;
            TempData["min_code"] = min_code;
            List<GetSMListViewModel> data = new List<GetSMListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/business/Get_special_mention_search?sessionno=" + sessionVal;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSMListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                for (var i = 0; i < data.Count; i++)
                {
                    if (data[i].mp_code == Convert.ToInt32(mp_code) || mp_code == 0)
                    {
                        if (data[i].MIN_NO == min_code || min_code == null)
                        {
                            Session["SM_Res"] = data;
                            count++;
                            checkData = "data";
                        }
                    }
                }
                TempData["SM_Count"] = count;
                //return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            //return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}