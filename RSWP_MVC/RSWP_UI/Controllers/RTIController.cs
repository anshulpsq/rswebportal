﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    public class RTIController : Controller
    {
        // GET: RTI
        public ActionResult RightToInfo()
        {
            return View();
        }

        public ActionResult StatementUnderSec()
        {
            return View();
        }

        public ActionResult StatusRTIApplication()
        {
            return View();
        }
    }
}