﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    public class GalleryController : Controller
    {
        // GET: Gallery
        public ActionResult GalleryView()
        {
            return View();
        }

        public ActionResult CurrentYearPhotos()
        {
            return View();
        }

        public ActionResult Archive()
        {
            return View();
        }
    }
}