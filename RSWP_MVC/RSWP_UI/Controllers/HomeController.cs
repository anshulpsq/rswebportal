﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSWP_BAL.Repository;
using RSWP_DAL;
using RSWP_DOM.Common;
using RSWP_UI.Common;
using RSWP_UI.Models;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class HomeController : BaseController
    {
        string careersfilePath = Convert.ToString(ConfigurationManager.AppSettings["careersfilePath"]);
        string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["dateFormat"]);


        // GET: Home

        public ActionResult Index()
        {
            RS_DBEntities db = new RS_DBEntities();
            List<tblRecruitmentCell> lstWebLinks = db.tblRecruitmentCells.Where(t => t.Type == "Web Links").ToList();
            return View(lstWebLinks);
        }


        #region previous code
        //PRESIDING OFFICERS
        public ActionResult PresidingOfficer_Index()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "PresidingOfficer_Index");
            POVCModelView pOVCModelView = new POVCModelView();

            using (RS_DBEntities db = new RS_DBEntities())
            {
                var panelOfVC = db.tblPanelOfVCs.ToList();
                foreach (var item in panelOfVC)
                {
                    pOVCModelView.CName = item.CName;
                    pOVCModelView.CLink = item.CLink;
                    pOVCModelView.DCName = item.DCName;
                    pOVCModelView.DLink = item.DLink;
                }
                pOVCModelView.VCMmembers = db.tblPanelOfVCMembers.ToList();
            }

            return View(pOVCModelView);
        }
                
        public ActionResult SecretaryGeneral_Index()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "SecretaryGeneral_Index");
            var sGModelView = new List<SGModelView>();

            using (RS_DBEntities db = new RS_DBEntities())
            {
                sGModelView = db.tblSGs.Select(r => new SGModelView
                {
                    From = r.From,
                    Name = r.Name,
                    To = r.To,
                    IsCurrent = r.IsCurrent,
                    ImgPath = r.ImgPath,
                    Profile = r.Profile
                }).ToList();
            }

            return View(sGModelView);
        }

        public ActionResult SecretaryGeneral_Menu(string sgType = "")
        {
            var model = new List<SGModelView>();
            switch (sgType)
            {
                case "present":
                    ViewBag.flag = 1;
                    model = SGPresent();
                    break;
                case "former":
                    ViewBag.flag = 2;
                    model = SGFormer();
                    break;
                default:
                    ViewBag.flag = true;
                    break;
            }
            return View(model);
        }

        public List<SGModelView> SGPresent()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "present");
            var list = new List<SGModelView>();

            using (RS_DBEntities db = new RS_DBEntities())
            {
                list = db.tblSGs.Select(r => new SGModelView
                {
                    From = r.From,
                    Name = r.Name,
                    To = r.To,
                    IsCurrent = r.IsCurrent,
                    ImgPath = r.ImgPath,
                    Profile = r.Profile
                }).Where(a => a.IsCurrent == true).ToList();
            }

            return list;
        }

        public List<SGModelView> SGFormer()
        {
            var list = new List<SGModelView>();
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "former");

            using (RS_DBEntities db = new RS_DBEntities())
            {
                list = db.tblSGs.Select(r => new SGModelView
                {
                    From = r.From,
                    Name = r.Name,
                    To = r.To,
                    IsCurrent = r.IsCurrent,
                    ImgPath = r.ImgPath,
                    Profile = r.Profile
                }).Where(a => a.IsCurrent == false).ToList();
            }

            return list;
        }
        #endregion



        #region New Code
        public ActionResult CouncilOfStates()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "CouncilOfStates");
            var list = new List<DocumentModelView>();
            using (RS_DBEntities db = new RS_DBEntities())
            {
                list = db.Documents.Select(r => new DocumentModelView
                {
                    Name = r.Name,
                    FileType = r.FileType,
                    Section = r.Section,
                    SubSection = r.SubSection,
                    FileLocation = r.FileLocation,
                    FileUrl = r.FileUrl,
                    IsArchived = r.IsArchived,
                    isPublished = r.isPublished,
                    Description = r.Description
                })
                .Where(r => r.Section == "Adminstration" && r.SubSection == "CouncilOfStates" && r.IsArchived == false && r.isPublished == true)
                .ToList();
            }
            return View(list);
        }

        public ActionResult Chairman()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "Chairman");
            
            return View();
        }

        public ActionResult FormerChairman()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "FormerChairman");

            return View();
        }

        public ActionResult ChairmanDeputy()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "ChairmanDeputy");

            return View();
        }

        public ActionResult FormerDeputyChairman()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "FormerDeputyChairman");

            return View();
        }

        public ActionResult LeaderOfHouse()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "LeaderOfHouse");

            return View();
        }

        public ActionResult FormerLeaderOfHouse()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "FormerLeaderOfHouse");

            return View();
        }

        public ActionResult LeaderOfOpposition()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "LeaderOfOpposition");

            return View();
        }

        public ActionResult FormerLeaderOfOpposition()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "FormerLeaderOfOpposition");

            return View();
        }

        public ActionResult ImpParliamentaryTerm()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "ImpParliamentaryTerm");
            var list = new List<DocumentModelView>();
            using (RS_DBEntities db = new RS_DBEntities())
            {
                list = db.Documents.Select(r => new DocumentModelView
                {
                    Name = r.Name,
                    //FileSize = r.FileSize,
                    //CreatedBy = r.CreatedBy,
                    //CreatedOn = r.CreatedOn,
                    FileType = r.FileType,
                    Section = r.Section,
                    SubSection = r.SubSection,
                    FileLocation = r.FileLocation,
                    FileUrl = r.FileUrl,
                    IsArchived = r.IsArchived,
                    isPublished = r.isPublished,
                    Description = r.Description
                })
                .Where(r => r.Section == "LarrdisSection" && r.SubSection == "ImpParliamentaryTerm" && r.IsArchived == false && r.isPublished == true)
                .ToList();
            }

            return View(list);
        }

        // to render an HTML file in view
        [ChildActionOnly]
        public ActionResult GetHtmlPage(string path)
        {
            return new FilePathResult(path, "text/html");
        }

        public ActionResult SecretaryGeneral()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "SecretaryGeneral");
            var sGModelView = new List<SGModelView>();

            sGModelView = SGPresent();

            return View(sGModelView);
        }

        public ActionResult VisitParliament()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "VisitParliament");

            return View();
        }

        public ActionResult PannelOfViceChairmen()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Home", "PannelOfViceChairmen");
            POVCModelView pOVCModelView = new POVCModelView();

            using (RS_DBEntities db = new RS_DBEntities())
            {
                var panelOfVC = db.tblPanelOfVCs.ToList();
                foreach (var item in panelOfVC)
                {
                    pOVCModelView.CName = item.CName;
                    pOVCModelView.CLink = item.CLink;
                    pOVCModelView.DCName = item.DCName;
                    pOVCModelView.DLink = item.DLink;
                }
                pOVCModelView.VCMmembers = db.tblPanelOfVCMembers.ToList();
            }

            return View(pOVCModelView);
        }


        #endregion

        public ActionResult ChangeLanguage(string lang)
        {
            new LanguageMang().SetLanguage(lang);
            return RedirectToAction("Index", "Home");
        }
    }
}