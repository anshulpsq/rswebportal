﻿using Newtonsoft.Json;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.GraphicalViewModel;
using RSWP_UI.Models.MemberViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    public class GraphicalController : Controller
    {
        #region Get Method of DataPoint
        // For Graphic Code 
        [DataContract]
        public class DataPoint
        {
            public DataPoint(string label, double y)
            {
                this.Label = label;
                this.Y = y;
            }

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "label")]
            public string Label = "";

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "y")]
            public Nullable<double> Y = null;
        }
        // Gor Res. Chart
        [DataContract]
        public class DataPoint1
        {
            public DataPoint1(string label, double y)
            {
                this.Label = label;
                this.Y = y;
            }

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "label")]
            public string Label = "";

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "y")]
            public Nullable<double> Y = null;
        }
        // Line Chart
        [DataContract]
        public class DataPoint2
        {
            public DataPoint2(string label, double y)
            {
                this.Label = label;
                this.Y = y;
            }

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "label")]
            public string Label = "";

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "y")]
            public Nullable<double> Y = null;
        }
        #endregion
        #region Graphical Method
        // GET: Graphical(Party Wise)
        public ActionResult Graphical_Analysis()
        {
            Party();
            List<DataPoint> dataPoints = new List<DataPoint>();
            foreach (var item in (List<GetPartyViewModel>)Session["party"])
            {
                dataPoints.Add(new DataPoint(item.PARTY_CODE, item.partycount));
            }
            //dataPoints.Add(new DataPoint("NXP", 14));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);

            return View();
        }
        // GET: State Wise
        public ActionResult State_Graphical_Analysis()
        {
            State();
            List<DataPoint> dataPoints = new List<DataPoint>();
            foreach (var item in (List<GetStateViewModel>)Session["state"])
            {
                dataPoints.Add(new DataPoint(item.STATE_CODE, item.STATE_COUNT));
            }
            //dataPoints.Add(new DataPoint("NXP", 14));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);

            return View();
        }
        // GET: Gender Wise
        public ActionResult Gender_Graphical_Analysis()
        {
            AllMember_Result();
            List<DataPoint> dataPoints = new List<DataPoint>();
            var maleCount = 0;
            var femaleCount = 0;
            foreach (var item in (List<GetAlphabeticalMembersViewModel>)Session["MemberDetails"])
            {
                //if (item.GENDER == "Male")
                //{
                //    maleCount = maleCount + 1;
                //}
                if (item.GENDER == "Female")
                {
                    femaleCount = femaleCount + 1;
                }
                else
                {
                    maleCount = maleCount + 1;
                }
            }

            dataPoints.Add(new DataPoint("Male", maleCount));
            dataPoints.Add(new DataPoint("Female", femaleCount));

            //dataPoints.Add(new DataPoint("NXP", 14));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);

            return View();
        }
        // GET: Profession Wise
        public ActionResult ProfessionWise_Graphical_Analysis()
        {
            ProfessionCount();
            List<DataPoint> dataPoints = new List<DataPoint>();
            foreach (var item in (List<GetProfessionWiseCountViewModel>)Session["ProfessionCount"])
            {
                dataPoints.Add(new DataPoint(item.PROF_NAME, item.count));
            }
            //dataPoints.Add(new DataPoint("NXP", 14));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);

            return View();
        }
        // GET: ExpenditureIncurred
        public ActionResult ExpenditureIncurred_Graphical_Analysis()
        {
            List<GetAllMemberDetailsViewModel> data = new List<GetAllMemberDetailsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.List = new SelectList(data, "MP_CODE", "MPFULLNAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public ActionResult ExpendituresIncurred_Graphical_Analysis()
        {
            return View();
        }
        // Total expenditure all members Financial Year
        public ActionResult ExpenditureAllMembers_Graphical_Analysis()
        {
            return View();
        }
        public ActionResult ExpenditureAllMember_Graphical_Analysis()
        {
            return View();
        }
        // GET: Education 
        public ActionResult Education_Graphical_Analysis()
        {
            GetEducationRes();
            //List<DataPoint> dataPoints = new List<DataPoint>();
            //foreach (var item in (List<GetProfessionWiseCountViewModel>)Session["ProfessionCount"])
            //{
            //    dataPoints.Add(new DataPoint(item.PROF_NAME, item.count));
            //}
            //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            return View();
        }
        // GET: Ministry wise distribution of assurances laid during 2010-2018
        public ActionResult AssurancesLaid_Graphical_Analysis()
        {
            List<GetAllMinistryViewModel> data = new List<GetAllMinistryViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/graphical/GetMinistry").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMinistryViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.List = new SelectList(data, "min_code", "min_name");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public ActionResult AssurancesLaids_Graphical_Analysis()
        {
            return View();
        }
        // GET: Yearly statement of assurances pending
        public ActionResult AssurancesPending_Graphical_Analysis()
        {
            GetResultAssurancesPending();
            List<DataPoint2> dataPoints1 = new List<DataPoint2>();
            List<DataPoint2> dataPoints2 = new List<DataPoint2>();
            List<DataPoint2> dataPoints3 = new List<DataPoint2>();
            foreach (var item in (List<GetAssurancesPendingViewModel>)Session["AssurancePendingCount"])
            {
                dataPoints1.Add(new DataPoint2(item.year1.ToString(), item.drop_count));
            }
            foreach (var item in (List<GetAssurancesPendingViewModel>)Session["AssurancePendingCount"])
            {
                dataPoints2.Add(new DataPoint2(item.year1.ToString(), item.pending_count));
            }
            foreach (var item in (List<GetAssurancesPendingViewModel>)Session["AssurancePendingCount"])
            {
                dataPoints3.Add(new DataPoint2(item.year1.ToString(), item.assurance_count));
            }
            ViewBag.DataPoints1 = JsonConvert.SerializeObject(dataPoints1);
            ViewBag.DataPoints2 = JsonConvert.SerializeObject(dataPoints2);
            ViewBag.DataPoints3 = JsonConvert.SerializeObject(dataPoints3);
            return View();
        }
        // GET: Yearly statement of dropped assurances
        public ActionResult DroppedAssurances_Graphical_Analysis()
        {
            GetResultAssurancesPending();
            List<DataPoint1> dataPoints = new List<DataPoint1>();
            foreach (var item in (List<GetAssurancesPendingViewModel>)Session["AssurancePendingCount"])
            {
                dataPoints.Add(new DataPoint1(item.year1.ToString(), item.drop_count));
            }
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            return View();
        }
        // GET: Questions(Notices submitted (starred/unstarred)/Notices admitted (listed)/Notices disallowed)
        public ActionResult NoticeDetails_Graphical_Analysis()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: 
        public ActionResult NoticeDetail_Graphical_Analysis()
        {
            return View();
        }
        // GET: Date according to session
        [HttpPost]
        public JsonResult Get_Date_Page(SessionDate obj)
        {
            List<SessionDate> data = new List<SessionDate>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/GetQuestion_sessionDate?session=" + obj.session_date;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionDate>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                ViewBag.Message = "Success";
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bills introduced in the Rajya Sabha
        public ActionResult Bills_Introduce()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public ActionResult Bill_Introduce()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Bills_Introduce(string sess)
        {
            List<DataPoint1> dataPoints = new List<DataPoint1>();
            List<GetBillsViewModel> data = new List<GetBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/graphical/Get_BillIntroducedinRS?session=" + sess;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetBillsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["SessNo"] = sess;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint1("Government Bills", item.GovtBill));
                    dataPoints.Add(new DataPoint1("Private Bills", item.PrivateBill));
                }
                TempData["GetBillIntro"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bills pending in the Rajya Sabha
        public ActionResult Bills_Pending()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public ActionResult Bill_Pending()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Bills_Pending(string sess)
        {
            List<DataPoint1> dataPoints = new List<DataPoint1>();
            List<GetBillsViewModel> data = new List<GetBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/graphical/Get_BillIntroducedinRS?session=" + sess;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetBillsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["SesNo"] = sess;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint1("Government Bills", item.GovtBill));
                    dataPoints.Add(new DataPoint1("Private Bills", item.PrivateBill));
                }
                TempData["GetBillIntros"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bills passed by the Rajya Sabha
        public ActionResult Bills_Passsed()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public ActionResult Bill_Passed()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Bills_Passed(string sess)
        {
            List<DataPoint1> dataPoints = new List<DataPoint1>();
            List<GetBillsViewModel> data = new List<GetBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/graphical/Get_BillPassedinRS?session=" + sess;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetBillsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["SesNo"] = sess;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint1("Government Bills", item.GovtBill));
                    dataPoints.Add(new DataPoint1("Private Bills", item.PrivateBill));
                }
                TempData["GetBillIntros"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Comman Method of Graphical Method
        // Party Wise Data
        public void Party()
        {
            List<GetPartyViewModel> data = new List<GetPartyViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetPartlist";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetPartyViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["party"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State Wise Data
        public void State()
        {
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["state"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get All Member
        public void AllMember_Result()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }

        }
        // Get Profession Count
        public void ProfessionCount()
        {
            List<GetProfessionWiseCountViewModel> data = new List<GetProfessionWiseCountViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Graphical/Getprofessionwisecount";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetProfessionWiseCountViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["ProfessionCount"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Expenditure Incurred
        [HttpPost]
        public JsonResult ExpenditureIncurred_Result(int mpcode, string duration1, string duration2, string memDur)
        {
            AllMember_Result();
            List<DataPoint> dataPoints = new List<DataPoint>();
            List<GetExpenditureIncurredViewModel> data = new List<GetExpenditureIncurredViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetExpenditureOccured?mpcode=" + mpcode + "&duration1=" + duration1 + "&duration2=" + duration2;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetExpenditureIncurredViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                foreach (var dt in (List<GetAlphabeticalMembersViewModel>)Session["MemberDetails"])
                {
                    if (dt.MP_CODE == mpcode)
                    {
                        TempData["MpFullName"] = dt.MPFULLNAME;
                        break;
                    }
                }
                TempData["MpDuration"] = memDur;
                //var total = 0;
                foreach (var item in data)
                {
                    var total = item.pasal + item.tada + item.mpsal + item.constituencyallow + item.officeallow;
                    dataPoints.Add(new DataPoint(item.billmonthtext, total));
                }
                //dataPoints.Add(new DataPoint("NXP", 14));
                TempData["DataPoints"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Expenditure Incurred All MP Result
        [HttpPost]
        public JsonResult ExpenditureIncurredAllMP_Result(string duration1, string duration2, string memDur)
        {
            List<DataPoint1> dataPoints = new List<DataPoint1>();
            List<GetExpenditureOccuredemberwiseViewModel> data = new List<GetExpenditureOccuredemberwiseViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetExpenditureOccuredemberwise?mpcode=0&duration1=" + duration1 + "&duration2=" + duration2;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetExpenditureOccuredemberwiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["MpDurationMP"] = memDur;
                //var total = 0;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint1(item.billmonthtext, item.totalexpenditure));
                }
                //dataPoints.Add(new DataPoint("NXP", 14));
                TempData["DataPointsMP"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // A Get Result Ministry wise distribution of assurances laid during 2010-2018
        [HttpPost]
        public JsonResult GetAssuranceLaid_Result(string mincode)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            List<DataPoint1> dataPoint = new List<DataPoint1>();
            List<GetAssuranceLaidViewModel> data = new List<GetAssuranceLaidViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/graphical/GetCGAMinstrywisecount?min_code=" + mincode;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAssuranceLaidViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //TempData["MpDurationMP"] = mincode;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint(item.year1.ToString(), item.assurance_count));
                }
                TempData["GetAssuranceLaid"] = JsonConvert.SerializeObject(dataPoints);
                foreach (var item in data)
                {
                    dataPoint.Add(new DataPoint1(item.year1.ToString(), item.assurance_count));
                }
                TempData["GetAssuranceLaid1"] = JsonConvert.SerializeObject(dataPoint);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Get Education of all Member
        public void GetEducationRes()
        {

        }
        // Get Result Yearly statement of assurances pending
        public void GetResultAssurancesPending()
        {
            List<GetAssurancesPendingViewModel> data = new List<GetAssurancesPendingViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/graphical/GetCGAMinstrywisecount";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAssurancesPendingViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["AssurancePendingCount"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Notice Data
        [HttpPost]
        public JsonResult GetNoticeDetails(string sess, string date1, string date2)
        {
            DateTime dat1 = Convert.ToDateTime(date1);
            string dateres1 = dat1.ToString("yyyy-MM-dd");
            DateTime dat2 = Convert.ToDateTime(date2);
            string dateres2 = dat2.ToString("yyyy-MM-dd");
            List<DataPoint> dataPoints = new List<DataPoint>();
            List<GetAllNoticesViewModel> data = new List<GetAllNoticesViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/graphical/Get_Notices_Submitted?session=" + sess + "&date1=" + dateres1 + "&date2=" + dateres2;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllNoticesViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["SessionNo"] = sess;
                TempData["DateFrom"] = date1;
                TempData["DateTo"] = date2;
                foreach (var item in data)
                {
                    dataPoints.Add(new DataPoint("Starred Notices", item.strarrednoticessubited));
                    dataPoints.Add(new DataPoint("Unstarrred Notices", item.unstrarrednoticessubmitted));
                    dataPoints.Add(new DataPoint("Starred Notices Admitted", item.strarrednoticesadmitted));
                    dataPoints.Add(new DataPoint("Unstarrred Notices Admitted", item.unstrarrednoticesadmitted));
                    dataPoints.Add(new DataPoint("Notices Disallowed", item.unstrarrednoticesdisallowed));
                    dataPoints.Add(new DataPoint("Lapsed", item.Lapsed));
                }
                TempData["GetNoticesRes"] = JsonConvert.SerializeObject(dataPoints);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}