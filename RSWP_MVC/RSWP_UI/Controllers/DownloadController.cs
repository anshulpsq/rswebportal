﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Download
        public ActionResult DownloadLink()
        {
            return View();
        }
        public ActionResult ApplicationForm()
        {
            return View();
        }
        public ActionResult FormForNewMembers()
        {
            return View();
        }
    }
}