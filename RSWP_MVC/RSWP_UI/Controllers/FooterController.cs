﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    public class FooterController : Controller
    {
        // GET: Footer
        public ActionResult ImportantOfficialWebsite()
        {
            return View();
        }

        public  ActionResult Feedback()
        {
            return View();
        }
        public ActionResult ViewFeedback()
        {
            return View();
        }

        public ActionResult SiteMap()
        {
            return View();
        }
        public ActionResult UseFulLink()
        {
            return View();
        }
        public ActionResult NewsPaper()
        {
            return View();
        }
        public ActionResult WebsitePolicy()
        {
            return View();
        }
        public ActionResult TermandCondition()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult Help()
        {
            return View();
        }
    }
}