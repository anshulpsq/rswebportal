﻿using RSWP_BAL.Repository;
using RSWP_DOM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Globalization;
using System.Diagnostics;
using RSWP_UI.Common;
using RSWP_UI.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace RSWP_UI.Controllers
{
    /// <summary>
    /// Responsible for all operations on Questions
    /// </summary>
    [WebLinksFilter]
    public class QuestionsController : BaseController
    {
        string filePath = Convert.ToString(ConfigurationManager.AppSettings["filePath"]);
        string selectedLang = CultureInfo.CurrentCulture.Name;
        RS_DBEntities db;
        string[] months;

        public QuestionsController()
        {
            db = new RS_DBEntities();
            months = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        }
        /// <summary>
        /// Responsible for all operations on General Questions
        /// </summary>
        public ActionResult Index(string questionsTypes)
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();
            try
            {
                TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "Index");
                switch (questionsTypes)
                {
                    case "Introduction":
                        ViewBag.flag = false;
                        ViewBag.switchName = "Introduction";
                        model = Introduction();
                        break;
                    case "RulesOfProcedure":
                        ViewBag.flag = false;
                        ViewBag.switchName = "RulesOfProcedure";
                        model = RulesOfProcedure();
                        break;
                    case "GroupingOfMinistries":
                        ViewBag.flag = false;
                        ViewBag.switchName = "GroupingOfMinistries";
                        model = GroupingOfMinistries();
                        break;
                    case "TypesOfQuestions":
                        ViewBag.flag = false;
                        ViewBag.switchName = "TypesOfQuestions";
                        model = TypesOfQuestions();
                        break;
                    case "AdmissibilityOfQuestions":
                        ViewBag.flag = false;
                        ViewBag.switchName = "AdmissibilityOfQuestions";
                        model = AdmissibilityOfQuestions();
                        break;
                    case "QuestionCalender":
                        ViewBag.flag = true;
                        ViewBag.switchName = "QuestionsCalender";
                        model = QuestionsCalender();
                        break;
                    case "QuestionCharts":
                        ViewBag.flag = true;
                        ViewBag.switchName = "QuestionCharts";
                        model = QuestionsCharts();
                        break;
                    default:
                        ViewBag.flag = true;
                        ViewBag.switchName = "QuestionCharts";
                        model = QuestionsCharts();
                        //ViewBag.switchName = "";
                        //model = Default();
                        break;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Questions/Index", line);
                return View(model);
            }



        }

        //// Question List

        public ActionResult GetJsonSessionSittings(int sessionNo, string section, string subSection)
        {
            return Json(GetSessionWiseSitting(sessionNo, section, subSection), JsonRequestBehavior.AllowGet);
        }
        public List<SelectListItem> GetSessionWiseSittings(int sessionNo, string section, string subSection, string Filetype)
        {
            List<SelectListItem> sittingMonths = new List<SelectListItem>();
            List<int> selectedMonths = new List<int>();
            List<sittingMaster> sessionWiseSittings = db.sittingMasters.Where(x => x.sessionNo == sessionNo).ToList();
            foreach (var item in sessionWiseSittings)
            {
                if (!selectedMonths.Contains(item.SittingDate.Month))
                {
                    selectedMonths.Add(item.SittingDate.Month);
                    sittingMonths.Add(new SelectListItem
                    {
                        Value = item.SittingDate.Month.ToString(),
                        Text = months[item.SittingDate.Month - 1]
                    });
                }
            }
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var month in sittingMonths)
            {
                List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo.ToString() && x.FileType == Filetype.ToString() &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
                if (documents.Any(x => x.selectedDate.Value.Month == Convert.ToInt32(month.Value)))
                    result.Add(month);
            }
            return result.OrderByDescending(x => Convert.ToInt32(x.Value)).ToList();
        }
        public List<SelectListItem> GetSessionWiseSitting(int sessionNo, string section, string subSection)
        {
            List<SelectListItem> sittingMonths = new List<SelectListItem>();
            List<int> selectedMonths = new List<int>();
            List<sittingMaster> sessionWiseSittings = db.sittingMasters.Where(x => x.sessionNo == sessionNo).ToList();
            foreach (var item in sessionWiseSittings)
            {
                if (!selectedMonths.Contains(item.SittingDate.Month))
                {
                    selectedMonths.Add(item.SittingDate.Month);
                    sittingMonths.Add(new SelectListItem
                    {
                        Value = item.SittingDate.Month.ToString(),
                        Text = months[item.SittingDate.Month - 1]
                    });
                }
            }
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var month in sittingMonths)
            {
                List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo.ToString() &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
                if (documents.Any(x => x.selectedDate.Value.Month == Convert.ToInt32(month.Value)))
                    result.Add(month);
            }
            return result.OrderByDescending(x => Convert.ToInt32(x.Value)).ToList();
        }
        public ActionResult GetJsonDocuments(int month, string sessionNo, string section, string subSection)
        {
            return Json(GetDocuments(month, sessionNo, section, subSection), JsonRequestBehavior.AllowGet);
        }
        public List<Document> GetDocuments(int month, string sessionNo, string section, string subSection)
        {
            List<Document> documents = db.Documents
                                       .Where(x => x.Section.Trim() == section.Trim() && x.SubSection.Trim() == subSection.Trim() && x.session == sessionNo &&
                                        x.isApproved == true && x.isPublished == true && x.IsArchived == false)
                                       .ToList();
            List<Document> docsByMonth = new List<Document>();
            foreach (var doc in documents)
            {
                if (doc.selectedDate.HasValue && doc.selectedDate.Value.Month == month)
                    docsByMonth.Add(doc);
            }
            List<Document> resultDocs = new List<Document>();
            if (section.Trim() == "TableOffice" && subSection.Trim() == "TableBusiness")
            {
                List<string> FileTypes = new List<string> { "LOB", "RLOB", "SLOB", "SLOB-2" };
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ThenBy(x => FileTypes.IndexOf(x.FileType)).ToList();
            }
            else if (section.Trim() == "Synopsis" && subSection.Trim() == "SynopsisUpload")
            {
                List<string> FileTypes = new List<string> { "synopsis", "supplement-1", "supplement-2" };
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ThenBy(x => FileTypes.IndexOf(x.FileType)).ToList();
            }
            else
                resultDocs = docsByMonth.OrderBy(x => x.selectedDate).ToList();

            return resultDocs;
        }
        public ActionResult QuestionListStarred()
        {
            BussinessDocumentModel model = new BussinessDocumentModel();
            var sittingMasters = db.sittingMasters.ToList();
            foreach (var item in sittingMasters)
            {
                if (!model.Sessions.Contains(item.sessionNo))
                    model.Sessions.Add(item.sessionNo);
            }
            if (sittingMasters.Count > 0)
            {
                model.Sessions = model.Sessions.OrderByDescending(x => x).ToList();
                model.SittingMonths = GetSessionWiseSittings(model.Sessions.First(), "Questions", "QuestionsList", "Starred");
                if (model.SittingMonths.Count > 0)
                {
                    model.Documents = GetDocuments(Convert.ToInt32(model.SittingMonths.First().Value), model.Sessions.First().ToString(), "Questions", "QuestionsList");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Responsible for all operations on Other Links Questions
        /// </summary>
        public ActionResult OthersLinks(string questionsTypes = "", string archive = "")
        {
            List<Documents> model = new List<Documents>();
            try
            {
                TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "Index");
                switch (questionsTypes)
                {
                    case "Archive":
                        ViewBag.flag = 1;
                        model = Archive();
                        break;
                    case "MinistryWiseSubjects":
                        ViewBag.flag = 2;
                        model = MinistryWiseSubjects(archive);
                        break;
                    case "MinistryWiseNodalOfficers":
                        ViewBag.flag = 3;
                        model = MinistryWiseNodalOfficers(archive);
                        break;
                    case "SessionWiseStatisticalReport":
                        ViewBag.flag = 4;
                        model = SessionWiseStatisticalReport(archive);
                        break;
                   
                    default:
                        ViewBag.flag = true;
                        //model = Default();
                        break;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Questions/OthersLinks", line);
                return View(model);
            }



        }

        /// <summary>
        /// Responsible for Ministry Wise Nodal Officers operations
        /// </summary>
        public ActionResult MinistryWiseNodalOfficers()
        {
            List<MinistryNodalOfficers> model = new List<MinistryNodalOfficers>();
            try
            {
                model = MinistryWiseNodalOfficersList();
                return View(model);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Questions/MinistryWiseNodalOfficers", line);
                return View(model);
            }

        }

        /// <summary>
        /// Responsible for default Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow Default()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "Default");
            return model;
        }


        /// <summary>
        /// Responsible for Introduction Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow Introduction()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.isPublished = true;
            documents.IsArchived = false;
            documents.Type = "Introduction";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            DataSet dsGetQuestionsGeneral = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();

            if (getQuestionsDocument.GetQuestionsGeneral(ref dsGetQuestionsGeneral, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsGeneral.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["Description"]),
                        FileUrl =  Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "Introduction", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "Introduction");
            return model;
        }


        /// <summary>
        /// Responsible for Rules of Procedure Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow RulesOfProcedure()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            DataSet dsGetQuestionsGeneral = new DataSet();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.isPublished = true;
            documents.IsArchived = false;
            documents.Type = "Rules of Procedure";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            string ErrorMsg = string.Empty;

            if (getQuestionsDocument.GetQuestionsGeneral(ref dsGetQuestionsGeneral, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsGeneral.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "RulesOfProcedure", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "RulesOfProcedure");
            return model;
        }

        /// <summary>
        /// Responsible for Grouping of Ministries Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow GroupingOfMinistries()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();

            DataSet dsGetQuestionsGeneral = new DataSet();
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.isPublished = true;
            documents.IsArchived = false;
            documents.Type = "Grouping of Ministries";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            string ErrorMsg = string.Empty;
            if (getQuestionsDocument.GetQuestionsGeneral(ref dsGetQuestionsGeneral, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsGeneral.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "GroupingOfMinistries", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "GroupingOfMinistries");
            return model;
        }


        /// <summary>
        /// Responsible for Types of Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow TypesOfQuestions()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();

            DataSet dsGetQuestionsGeneral = new DataSet();
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.isPublished = true;
            documents.IsArchived = false;
            documents.Type = "Types of Questions";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            string ErrorMsg = string.Empty;
            if (getQuestionsDocument.GetQuestionsGeneral(ref dsGetQuestionsGeneral, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsGeneral.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "TypesOfQuestions", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "TypesOfQuestions");
            return model;
        }


        /// <summary>
        /// Responsible for Admissibility of Questions operations
        /// </summary>
        [NonAction]
        public QuestionDocumentsToShow AdmissibilityOfQuestions()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();

            DataSet dsGetQuestionsGeneral = new DataSet();
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.isPublished = true;
            documents.IsArchived = false;
            documents.Type = "Admissibility of Questions";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            string ErrorMsg = string.Empty;
            if (getQuestionsDocument.GetQuestionsGeneral(ref dsGetQuestionsGeneral, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsGeneral.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsGeneral.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "AdmissibilityOfQuestions", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "AdmissibilityOfQuestions");
            return model;
        }

        /// <summary>
        /// Responsible for Archive Questions operations
        /// </summary>
        [NonAction]
        public List<Documents> Archive()
        {
            List<Documents> list = new List<Documents>();

            DataSet dsGetQuestionsArchivesGroupingOfMinistries = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsGeneral";
            documents.Type = "Grouping of Ministries";
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            documents.isPublished = true;
            documents.IsArchived = true;
            if (getQuestionsDocument.GetQuestionsArchivesGroupingOfMinistries(ref dsGetQuestionsArchivesGroupingOfMinistries, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsArchivesGroupingOfMinistries.Tables[0].Rows.Count > 0)
                {
                    Documents objDocuments;
                    for (int i = 0; i < dsGetQuestionsArchivesGroupingOfMinistries.Tables[0].Rows.Count; i++)
                    {
                        objDocuments = new Documents()
                        {
                            session = Convert.ToString(dsGetQuestionsArchivesGroupingOfMinistries.Tables[0].Rows[i]["session"]),
                            Name = Convert.ToString(dsGetQuestionsArchivesGroupingOfMinistries.Tables[0].Rows[i]["Name"]),
                            FileUrl = filePath + Convert.ToString(dsGetQuestionsArchivesGroupingOfMinistries.Tables[0].Rows[i]["Name"])
                        };
                        list.Add(objDocuments);
                    }

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "Archive", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "Archive");
            return list;
        }
        [NonAction]
        public QuestionDocumentsToShow QuestionsCalender()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsCalender";
            documents.isPublished = true;
            documents.IsArchived = false;
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            DataSet dsGetQuestionsCalender = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();

            if (getQuestionsDocument.GetQuestionsCalender(ref dsGetQuestionsCalender, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsCalender.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsCalender.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsCalender.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsCalender.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "QuestionsCalender", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "QuestionsCalender");
            return model;
        }



        /// <summary>
        /// Responsible for Ministry Wise Subjects operations
        /// </summary>
        [NonAction]
        public List<Documents> MinistryWiseSubjects(string archive)
        {
            List<Documents> list = new List<Documents>();

            DataSet dsGetQuestionsOtherLinks = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsSubject";
            documents.isPublished = true;
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            if (archive == "")
                documents.IsArchived = false;
            else
                documents.IsArchived = true;
            if (getQuestionsDocument.GetQuestionsOtherLinks(ref dsGetQuestionsOtherLinks, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsOtherLinks.Tables[0].Rows.Count > 0)
                {
                    Documents objDocuments;
                    for (int i = 0; i < dsGetQuestionsOtherLinks.Tables[0].Rows.Count; i++)
                    {
                        objDocuments = new Documents()
                        {
                            Ministry = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Ministry"]),
                            Metadata = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Metadata"]),
                            Tiltle = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Tiltle"]),
                            FileUrl = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileUrl"]),
                            FileType = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileSize"]) + "MB)"
                            //FileSize = "(" + Convert.ToString(Convert.ToInt32(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileSize"]) / 1000) + "MB)"
                        };
                        list.Add(objDocuments);
                    }

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "MinistryWiseSubjects", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "MinistryWiseSubjects");
            return list;
        }


        /// <summary>
        /// Responsible for Ministry Wise Nodal Officers archive documents operations
        /// </summary>
        [NonAction]
        public List<Documents> MinistryWiseNodalOfficers(string archive)
        {
            List<Documents> list = new List<Documents>();

            DataSet dsGetQuestionsOtherLinks = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsNodal";
            documents.isPublished = true;
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            if (archive == "")
                documents.IsArchived = false;
            else
                documents.IsArchived = true;
            if (getQuestionsDocument.GetQuestionsOtherLinks(ref dsGetQuestionsOtherLinks, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsOtherLinks.Tables[0].Rows.Count > 0)
                {
                    Documents objDocuments;
                    for (int i = 0; i < dsGetQuestionsOtherLinks.Tables[0].Rows.Count; i++)
                    {
                        objDocuments = new Documents()
                        {
                            session = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["session"]),
                            Ministry = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Ministry"]),
                            Metadata = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Metadata"]),
                            Tiltle = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Tiltle"]),
                            FileUrl = filePath + Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["Name"]),
                            FileType = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsOtherLinks.Tables[0].Rows[0]["FileSize"]) + "MB)"
                            //FileSize = "(" + Convert.ToString(Convert.ToInt32(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileSize"])/1000) + "MB)"
                        };
                        list.Add(objDocuments);
                    }

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "MinistryWiseSubjects", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "MinistryWiseNodalOfficers");
            return list;
        }


        /// <summary>
        /// Responsible for Ministry Wise Nodal Officers operations
        /// </summary>
        [NonAction]
        public List<MinistryNodalOfficers> MinistryWiseNodalOfficersList()
        {
            List<MinistryNodalOfficers> list = new List<MinistryNodalOfficers>();

            DataSet dsMinistryNodalOfficers = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();

            if (getQuestionsDocument.GetMinistryNodalOfficers(ref dsMinistryNodalOfficers, ref ErrorMsg))
            {
                if (dsMinistryNodalOfficers.Tables[0].Rows.Count > 0)
                {
                    MinistryNodalOfficers objMinistryNodalOfficers;
                    for (int i = 0; i < dsMinistryNodalOfficers.Tables[0].Rows.Count; i++)
                    {
                        objMinistryNodalOfficers = new MinistryNodalOfficers()
                        {
                            Id = i + 1,
                            Ministry = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["Ministry"]),
                            NameOfOfficer = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["NameOfOfficer"]),
                            Designation = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["Designation"]),
                            OfficeTelephoneNo = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["OfficeTelephoneNo"]),
                            ResidenceTelephoneNo = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["ResidenceTelephoneNo"]),
                            Email = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["Email"]),
                            OfficeAddress = Convert.ToString(dsMinistryNodalOfficers.Tables[0].Rows[i]["OfficeAddress"])
                        };
                        list.Add(objMinistryNodalOfficers);
                    }

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "MinistryWiseNodalOfficers", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "MinistryWiseNodalOfficers");
            return list;
        }


        /// <summary>
        /// Responsible for Session Wise Statistical Report operations
        /// </summary>
        [NonAction]
        public List<Documents> SessionWiseStatisticalReport(string archive)
        {
            List<Documents> list = new List<Documents>();

            DataSet dsGetQuestionsOtherLinks = new DataSet();
            string ErrorMsg = string.Empty;
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsStatistical";
            documents.isPublished = true;
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            if (archive == "")
                documents.IsArchived = false;
            else
                documents.IsArchived = true;
            if (getQuestionsDocument.GetQuestionsOtherLinks(ref dsGetQuestionsOtherLinks, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsOtherLinks.Tables[0].Rows.Count > 0)
                {
                    Documents objDocuments;
                    for (int i = 0; i < dsGetQuestionsOtherLinks.Tables[0].Rows.Count; i++)
                    {
                        objDocuments = new Documents()
                        {
                            session = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["session"]),
                            FileUrl = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileUrl"]),
                            FileType = Convert.ToString(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileSize"]) + "MB)"
                            //FileSize = "(" + Convert.ToString(Convert.ToInt32(dsGetQuestionsOtherLinks.Tables[0].Rows[i]["FileSize"]) / 1000) + "MB)"
                        };
                        list.Add(objDocuments);
                    }

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "SessionWiseStatisticalReport", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "SessionWiseStatisticalReport");
            return list;
        }
        [NonAction]
        public QuestionDocumentsToShow QuestionsCharts()
        {
            QuestionDocumentsToShow model = new QuestionDocumentsToShow();

            DataSet dsGetQuestionsChart = new DataSet();
            GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
            Documents documents = new Documents();
            documents.Section = "Questions";
            documents.SubSection = "QuestionsChart";
            documents.isPublished = true;
            if (selectedLang == "en")
                documents.Language = "English";
            else
                documents.Language = "Hindi";
            string ErrorMsg = string.Empty;
            if (getQuestionsDocument.GetQuestionsChart(ref dsGetQuestionsChart, documents, ref ErrorMsg))
            {
                if (dsGetQuestionsChart.Tables[0].Rows.Count > 0)
                {
                    QuestionDocumentsToShow questionDocumentsToShow;
                    questionDocumentsToShow = new QuestionDocumentsToShow()
                    {
                        Description = Convert.ToString(dsGetQuestionsChart.Tables[0].Rows[0]["Description"]),
                        FileUrl = Convert.ToString(dsGetQuestionsChart.Tables[0].Rows[0]["FileUrl"]),
                        FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsChart.Tables[0].Rows[0]["FileSize"]) + "MB)"
                    };
                    model = questionDocumentsToShow;

                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "Questionschart", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "QuestionCharts");
            return model;
        }
        // Iframe binding for Chart
        [HttpPost]
        public ActionResult QuestionChartPDF()
        {
            List<MinistryNodalOfficers> model = new List<MinistryNodalOfficers>();
            try
            {
                GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
                Documents documents = new Documents();
                documents.Section = "Questions";
                documents.SubSection = "QuestionsChart";
                documents.isPublished = true;
                documents.IsArchived = false;
                if (selectedLang == "en")
                    documents.Language = "English";
                else
                    documents.Language = "Hindi";
                
                DataSet dsGetQuestionsChart = new DataSet();
                string ErrorMsg = string.Empty;
                if (getQuestionsDocument.GetQuestionsChart(ref dsGetQuestionsChart, documents, ref ErrorMsg))
                {
                    if (dsGetQuestionsChart.Tables[0].Rows.Count > 0)
                    {
                        QuestionDocumentsToShow questionDocumentsToShow;
                        questionDocumentsToShow = new QuestionDocumentsToShow()
                        {
                            Description = Convert.ToString(dsGetQuestionsChart.Tables[0].Rows[0]["Description"]),
                            FileUrl = Convert.ToString(dsGetQuestionsChart.Tables[0].Rows[0]["FileUrl"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetQuestionsChart.Tables[0].Rows[0]["FileSize"]) + "MB)"
                        };
                        return Json(new { data = questionDocumentsToShow, JsonRequestBehavior.AllowGet });
                    }
                }
                else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
                {
                    LogGenerator.GenerateLog(ErrorMsg, "Questions/QuestionChartPDF", "");
                }
                return Json(new { data = "", JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Questions/QuestionChartPDF", line);
                return View(model);
            }
        }
        /// <summary>
        /// Responsible for Ministry Wise Nodal Officers PDF
        /// </summary>
        [HttpPost]
        public ActionResult MinistryWiseNodalOfficersPDF()
        {
            List<MinistryNodalOfficers> model = new List<MinistryNodalOfficers>();
            try
            {
                GetQuestionsDocument getQuestionsDocument = new GetQuestionsDocument();
                Documents documents = new Documents();
                documents.Section = "Questions";
                documents.SubSection = "QuestionsNodal";
                documents.isPublished = true;
                documents.IsArchived = false;
                DataSet dsMinistryWiseNodalOfficersPDF = new DataSet();
                string ErrorMsg = string.Empty;
                if (getQuestionsDocument.MinistryWiseNodalOfficersPDF(ref dsMinistryWiseNodalOfficersPDF, documents, ref ErrorMsg))
                {
                    if (dsMinistryWiseNodalOfficersPDF.Tables[0].Rows.Count > 0)
                    {
                        QuestionDocumentsToShow questionDocumentsToShow;
                        questionDocumentsToShow = new QuestionDocumentsToShow()
                        {
                            Description = Convert.ToString(dsMinistryWiseNodalOfficersPDF.Tables[0].Rows[0]["Description"]),
                            FileUrl = Convert.ToString(dsMinistryWiseNodalOfficersPDF.Tables[0].Rows[0]["FileUrl"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsMinistryWiseNodalOfficersPDF.Tables[0].Rows[0]["FileSize"]) + "MB)"
                        };
                        return Json(new { data = questionDocumentsToShow, JsonRequestBehavior.AllowGet });
                    }
                }
                else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
                {
                    LogGenerator.GenerateLog(ErrorMsg, "Questions/MinistryWiseNodalOfficersPDF", "");
                }
                return Json(new { data = "", JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Questions/MinistryWiseNodalOfficersPDF", line);
                return View(model);
            }
        }

        public ActionResult SearchQuestions(string id)
        {
            List<sittingMaster> sittingMasterList = new List<sittingMaster>();
            ViewBag.Param = id;
            switch (id)
            {
                case "DateWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "DateWise");
                    using (RS_DBEntities db = new RS_DBEntities())
                    {
                        sittingMasterList = db.sittingMasters.ToList();
                        TempData["sessionNo"] = sittingMasterList.OrderByDescending(r => r.sessionNo).Select(r => r.sessionNo).Distinct().ToList();
                    }
                    break;
                case "QuestionNoWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "QuestionNoWise");
                    using (RS_DBEntities db = new RS_DBEntities())
                    {
                        sittingMasterList = db.sittingMasters.ToList();
                        TempData["sessionNo"] = sittingMasterList.OrderByDescending(r => r.sessionNo).Select(r => r.sessionNo).Distinct().ToList();
                    }
                    break;
                case "TypeWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "TypeWise");
                    using (RS_DBEntities db = new RS_DBEntities())
                    {
                        sittingMasterList = db.sittingMasters.ToList();
                        TempData["sessionNo"] = sittingMasterList.OrderByDescending(r => r.sessionNo).Select(r => r.sessionNo).Distinct().ToList();
                    }
                    break;
                case "SessionWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "SessionWise");
                    using (RS_DBEntities db = new RS_DBEntities())
                    {
                        sittingMasterList = db.sittingMasters.ToList();
                        TempData["sessionNo"] = sittingMasterList.OrderByDescending(r => r.sessionNo).Select(r => r.sessionNo).Distinct().ToList();
                    }
                    break;
                case "MinistryWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "MinistryWise");
                    using (RS_DBEntities db = new RS_DBEntities())
                    {
                        sittingMasterList = db.sittingMasters.ToList();
                        TempData["sessionNo"] = sittingMasterList.OrderByDescending(r => r.sessionNo).Select(r => r.sessionNo).Distinct().ToList();
                    }
                    break;
                default:
                    break;
            }
            return View(sittingMasterList);
        }

        public ActionResult Questions(string id)
        {
            ViewBag.Param = id;
            switch (id)
            {
                case "BallotList":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "BallotList");
                    break;
                case "PendingQStatus":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "PendingQStatus");
                    break;
                case "ShortNoticeQuestions":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Questions", "ShortNoticeQuestions");
                    break;
                default:
                    break;
            }
            return View();
        }
        /// <summary>
        /// Question Webservice module Old DB David
        /// </summary>
        /// <returns></returns>
        #region Search Questions 03/10/2019
        // Get Session for DropDown
        public void GetSessionDD()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                TempData["SessionList"] = new SelectList(data, "session_code", "Session_value");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        //Member Wise Search
        public ActionResult MemberWiseSearch()
        {
            List<GetAllMemberDetailsViewModel> data = new List<GetAllMemberDetailsViewModel>();
            HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("http://164.100.47.5");
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.List = new SelectList(data, "MP_CODE", "MPFULLNAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult MemberWiseSearch(int mpcode)
        {
            List<GetMemberQuestionsViewModel> data = new List<GetMemberQuestionsViewModel>();
            List<GetAllMemberDetailsViewModel> data1 = new List<GetAllMemberDetailsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/Question/GetMeber_Question?mpcode=" + mpcode;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMemberQuestionsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Count = data.Count;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        //Question Deails By Members QuestionNumber
        [HttpPost]
        public ActionResult CheckQuestionNumber(string qno)
        {
            List<GetMemberQuestionsViewModel> fileList = new List<GetMemberQuestionsViewModel>();
            GetMemberQuestionsViewModel fileResult = new GetMemberQuestionsViewModel();
            fileList = (List<GetMemberQuestionsViewModel>)Session["MemberDetails"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.min_name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                    break;
                }
            }
            TempData["Message"] = "MemberWise";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        public ActionResult QuestionDetails()
        {
            return View();
        }
        //Date Wise Search
        public ActionResult DateWiseSearch()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult DateWise(SessionDate obj)
        {
            List<SessionDate> data = new List<SessionDate>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/GetQuestion_sessionDate?session=" + obj.session_date;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionDate>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        [HttpPost]
        public JsonResult BallotDateWise(SessionDate obj)
        {
            List<SessionDate> data = new List<SessionDate>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/Get_Sitting_Date?session=" + obj.session_date + "&qtype=starred";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionDate>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        [HttpPost]
        public JsonResult DateWiseSearch(string session, string ansdate)
        {
            List<GetMemberDateWiseViewModel> data = new List<GetMemberDateWiseViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/Get_Question_DateWise?session=" + session + "&ansdate=" + ansdate;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMemberDateWiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["DateWiseMemberDetails"] = data;
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        public ActionResult CheckQuestionNumberDateWise(string qno)
        {
            List<GetMemberDateWiseViewModel> fileList = new List<GetMemberDateWiseViewModel>();
            GetMemberDateWiseViewModel fileResult = new GetMemberDateWiseViewModel();
            fileList = (List<GetMemberDateWiseViewModel>)Session["DateWiseMemberDetails"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "DateWise";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Question Number Wise Search
        public ActionResult QuestionNumberWiseSearch()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult QuestionRange(string session, string qtype)
        {
            List<QuestionNumber> data = new List<QuestionNumber>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Question/GetTotalQuestioncount?session=" + session + "&qtype=" + qtype;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<QuestionNumber>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        [HttpPost]
        public JsonResult QuestionNumberWiseSearch(string sessionno, string qtype, string qno)
        {
            List<QuestionNumberWiseViewModel> data = new List<QuestionNumberWiseViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var url = "/api_new/question/Get_Question_QuestionWise?sessionno=" + sessionno + "&qno=" + qno + "&qtype=" + qtype;
            HttpResponseMessage responseTerm1 = client.GetAsync(url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<QuestionNumberWiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["QuestionNumberMemberDetails"] = data;
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckQuestionNumberWise(string qno)
        {
            List<QuestionNumberWiseViewModel> fileList = new List<QuestionNumberWiseViewModel>();
            QuestionNumberWiseViewModel fileResult = new QuestionNumberWiseViewModel();
            fileList = (List<QuestionNumberWiseViewModel>)Session["QuestionNumberMemberDetails"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "QuestionNumber";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Question Type Wise Search
        public ActionResult QuestionTypeWiseSearch()
        {
            GetSessionDD();
            return View();
        }
        [HttpPost]
        public ActionResult QuestionTypeWiseSearch(string session, string sessionNo)
        {
            GetSessionDD();
            List<GetAllQuestionTypeWiseViewModel> data = new List<GetAllQuestionTypeWiseViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Question/Get_Question_QuestionType?qtype=" + session + "&ses_no=" + sessionNo;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllQuestionTypeWiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["Message"] = "Success";
                Session["QuestionType"] = data;
                TempData["QuesTypeMessage"] = session;
                TempData["SessionNoMessage"] = sessionNo;
                //return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            //return Json(data, JsonRequestBehavior.AllowGet);
            return View(data);
        }
        public ActionResult QuestionTypeWiseSearchResult()
        {
            GetSessionDD();
            List<GetAllQuestionTypeWiseViewModel> data = new List<GetAllQuestionTypeWiseViewModel>();
            data = (List<GetAllQuestionTypeWiseViewModel>)Session["QuestionType"];
            ViewBag.QTypeResult = data;
            return View();
        }
        [HttpPost]
        public ActionResult CheckQuesTypeQuestionNumber(string qno)
        {
            List<GetAllQuestionTypeWiseViewModel> fileList = new List<GetAllQuestionTypeWiseViewModel>();
            GetAllQuestionTypeWiseViewModel fileResult = new GetAllQuestionTypeWiseViewModel();
            fileList = (List<GetAllQuestionTypeWiseViewModel>)Session["QuestionType"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "QuestionsType";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Session Wise Search
        public ActionResult SessionWiseSearch()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public ActionResult SessionWiseSearch(SessionViewModel obj)
        {
            List<GetAllSessionWiseViewModel> data = new List<GetAllSessionWiseViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Question/Get_Question_SessionWise?session=" + obj.Session_value;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllSessionWiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                Session["sessionList"] = data1;
                Session["SessionRes"] = data;
                TempData["Message"] = "Success";
            }
            else
            {
                TempData["Message"] = "Error";
            }
            //return Json(data, JsonRequestBehavior.AllowGet);
            return View(data);
        }
        public ActionResult SessionWiseSearchResult()
        {
            List<GetAllSessionWiseViewModel> data = new List<GetAllSessionWiseViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            data = (List<GetAllSessionWiseViewModel>)Session["SessionRes"];
            data1 = (List<SessionViewModel>)Session["sessionList"];
            ViewBag.SessionRes = data;
            ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
            return View();
        }
        public ActionResult CheckSessionWiseQuestionNumber(string qno)
        {
            List<GetAllSessionWiseViewModel> fileList = new List<GetAllSessionWiseViewModel>();
            GetAllSessionWiseViewModel fileResult = new GetAllSessionWiseViewModel();
            fileList = (List<GetAllSessionWiseViewModel>)Session["SessionRes"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "SessionWiseRes";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Ministry Wise Search
        public ActionResult MinistryWiseSearch()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            List<MinistryNameViewModel> data1 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetAllministary").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                ViewBag.MinistryList = new SelectList(data1, "MIN_CODE", "MIN_NAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult MinistryWiseSearch(MinistryNameViewModel obj)
        {
            List<GetAllMinistryWiseViewModel> data = new List<GetAllMinistryWiseViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            List<MinistryNameViewModel> data2 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Question/GetMinistry_Question?sessionno=" + obj.Session_value + "&minid=" + obj.MIN_CODE;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetAllministary").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMinistryWiseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MinistryWise"] = data;
                ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        public ActionResult CheckQuestionMinistryWise(string qno)
        {
            List<GetAllMinistryWiseViewModel> fileList = new List<GetAllMinistryWiseViewModel>();
            GetAllMinistryWiseViewModel fileResult = new GetAllMinistryWiseViewModel();
            fileList = (List<GetAllMinistryWiseViewModel>)Session["MinistryWise"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "MinistryWise";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Ballot List
        public ActionResult BallotList()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult BallotList(string session, string date, string qtype)
        {
            List<BallotListViewModel> data = new List<BallotListViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/Get_Ballot_position_oral?session=" + session + "&date=" + date + "&qtype=" + qtype;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<BallotListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Short Notice Question
        public ActionResult ShortNoticeQuestions()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult ShortNoticeQuestions(string session)
        {
            List<ShortNoticeQuestionViewModel> data = new List<ShortNoticeQuestionViewModel>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/GetShorNoticeQuestion?session=" + session;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<ShortNoticeQuestionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                Session["ShortNoticeWise"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        public ActionResult CheckQuestionShortNoticeWise(string qno)
        {
            List<ShortNoticeQuestionViewModel> fileList = new List<ShortNoticeQuestionViewModel>();
            ShortNoticeQuestionViewModel fileResult = new ShortNoticeQuestionViewModel();
            fileList = (List<ShortNoticeQuestionViewModel>)Session["ShortNoticeWise"];
            //var list1 = fileList.Where(item => item.qno==float.Parse(qno));
            foreach (var item in fileList)
            {
                if (item.qno == float.Parse(qno))
                {
                    TempData["min_name"] = item.name;
                    TempData["qno"] = item.qno;
                    TempData["ans_date"] = item.ans_date;
                    TempData["qtitle"] = item.qtitle;
                    TempData["name"] = item.name;
                    TempData["shri"] = item.shri;
                    TempData["qn_text"] = item.qn_text;
                    TempData["ans_text"] = item.ans_text;
                }
            }
            TempData["Message"] = "ShortNotice";
            return Json(qno, JsonRequestBehavior.AllowGet);
        }
        //Pending Questions Status
        public ActionResult PendingQuestionsStatus()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public ActionResult PendingQuestionsStatus(string session)
        {
            if (session != null)
            {
                Session["SessionValue"] = session;
                return Json(session, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        public ActionResult PendingQuestionsStatusResult()
        {
            return View();
        }
        public ActionResult PendingQuesMinistryWise()
        {
            List<MinistryNameViewModel> data = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 
            HttpResponseMessage responseTerm1 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetAllministary").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.MinistryList = new SelectList(data, "MIN_CODE", "MIN_NAME");
                //ViewBag.SessionValue = TempData["SessionValue"];
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult PendingQuesMinistryWise(string min_code)
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/question/Get_pending_Question_minwise?session=" + Session["SessionValue"] + "&min_code=" + min_code;
            //API Used of Term  
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<PandingQuestionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                if (data.Count == 0)
                {
                    TempData["res"] = "No Record..";
                }
                else
                {
                    Session["ResultAllMin"] = data;
                    Session["MCode"] = data[0].minname;
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PendingQuesMinistryWiseResult()
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            if (TempData["res"] != null)
            {
                ViewBag.message = "The record is empty.";
            }
            else
            {
                data = (List<PandingQuestionViewModel>)Session["ResultAllMin"];
                ViewBag.Result = data;
                ViewBag.message = "The record is not empty.";
                return View();
            }
            return View();
        }
        public ActionResult PendingQuesDateWise()
        {
            List<SessionDate> data = new List<SessionDate>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/GetQuestion_sessionDate?session=" + Session["SessionValue"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionDate>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult PendingQuesDateWise(string date)
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/question/Get_pending_Question_sittingdatewise?session=" + Session["SessionValue"] + "&date=" + date;
            //API Used of Term  
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<PandingQuestionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                if (data.Count == 0)
                {
                    TempData["res"] = "No Record..";
                }
                else
                {
                    Session["ResultAllDate"] = data;
                    TempData["PDate"] = data[0].ans_date;
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PendingQuesDateWiseResult()
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            if (TempData["res"] != null)
            {
                ViewBag.message = "The record is empty.";
            }
            else
            {
                data = (List<PandingQuestionViewModel>)Session["ResultAllDate"];
                ViewBag.Result = data;
                ViewBag.message = "The record is not empty.";
                return View();
            }
            return View();
        }
        public ActionResult PendingQuesMinistryWiseSummary()
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/question/Get_pending_Question_minwisesummary?session=" + Session["SessionValue"];
            //API Used of Term  
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<PandingQuestionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                if (data.Count == 0)
                {
                    TempData["res"] = "The record is empty.";
                }
                else
                {
                    ViewBag.Result = data;
                    ViewBag.message = "The record is not empty.";
                    return View();
                }
            }
            return View();
        }
        public ActionResult PendingQuesDepartmentWiseSummary()
        {
            List<PandingQuestionViewModel> data = new List<PandingQuestionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/question/Get_pending_Question_Deptwise?session=" + Session["SessionValue"];
            //API Used of Term  
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<PandingQuestionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                if (data.Count == 0)
                {
                    TempData["res"] = "The record is empty.";
                }
                else
                {
                    ViewBag.Result = data;
                    ViewBag.message = "The record is not empty.";
                    return View();
                }
            }
            return View();
        }
        //For Pending Question modal popup
        [HttpPost]
        public JsonResult CheckDateSTARRED(string ans_date)
        {
            List<PandingQuestionViewModel> data1 = new List<PandingQuestionViewModel>();
            CommanQuestionViewModel data = new CommanQuestionViewModel();
            if (TempData["res"] != null)
            {
                ViewBag.message = "The record is empty.";
            }
            else
            {
                data1 = (List<PandingQuestionViewModel>)Session["ResultAllMin"];
                ViewBag.Result = data1;
                ViewBag.message = "The record is not empty.";
                data.ans_date = ans_date;
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CheckDateUNSTARRED(string ansdate)
        {
            return Json(ansdate, JsonRequestBehavior.AllowGet);
        }
        //Supplementary Questions
        public ActionResult SupplementaryQuestion()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            List<GetAllMemberDetailsViewModel> data1 = new List<GetAllMemberDetailsViewModel>();
            List<MinistryNameViewModel> data2 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetAllministary").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
                ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult Get_Date_Page(SessionDate obj)
        {
            List<SessionDate> data = new List<SessionDate>();
            List<SessionViewModel> data1 = new List<SessionViewModel>();
            List<GetSupplementrySessionViewModel> data2 = new List<GetSupplementrySessionViewModel>();
            List<PageNumberCountViewModel> result = new List<PageNumberCountViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/question/GetQuestion_sessionDate?session=" + obj.session_date;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/Get_Supplementry?session=" + obj.session_date).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionDate>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.DateList = new SelectList(data, "session_date", "session_date");
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data1, "session_code", "Session_value");
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<GetSupplementrySessionViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.Message = "Success";
                foreach (var item in data2)
                {
                    data.Add(new SessionDate { qno = item.qno, ses_no = item.ses_no });
                }
                ViewBag.SessionPageList = new SelectList(result, "ses_no", "qno");
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
            //return View();
        }
        [HttpPost]
        public JsonResult SupplementaryQuestion(string session)
        {
            List<GetSupplementrySessionViewModel> data = new List<GetSupplementrySessionViewModel>();
            List<SessionViewModel> data3 = new List<SessionViewModel>();
            List<GetAllMemberDetailsViewModel> data1 = new List<GetAllMemberDetailsViewModel>();
            List<MinistryNameViewModel> data2 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Question/Get_Supplementry?session=" + session;
            HttpResponseMessage responseTerm = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm1 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetData/getmemberall").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync(CommanConstant.urlContent + "/api_new/question/GetAllministary").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSupplementrySessionViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data3 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data3, "session_code", "Session_value");
                ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
                ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Integrated Search Form
        public ActionResult IntegratedSearchForm()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            List<GetAllMemberDetailsViewModel> data1 = new List<GetAllMemberDetailsViewModel>();
            List<MinistryNameViewModel> data2 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm2 = client.GetAsync("/api_new/MemberGetData/getmemberall").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync("/api_new/question/GetAllministary").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data, "session_code", "Session_value");
                ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
                ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult IntegratedSearchForm(string session,string mpCode,string minCode,string sesType,string QesNumber)
        {
            var str = "";
            if(mpCode != "")
            {
                str = str + " mp_code=" + mpCode;
            }
           
            if (minCode != "")
            {
                str = str + " and min_code=" + minCode;
            }
            if (session != "")
            {
                str = str + " and session=" + session;
            }
            if (sesType != "ANYTYPE")
            {
                str = str + " and qtype='" + sesType + "'";
            }
            if (QesNumber != "")
            {
                str = str + " and qno=" + QesNumber;
            }
            List<GetAllIntegratedSearchViewModel> data = new List<GetAllIntegratedSearchViewModel>();
            List<SessionViewModel> data3 = new List<SessionViewModel>();
            List<GetAllMemberDetailsViewModel> data1 = new List<GetAllMemberDetailsViewModel>();
            List<MinistryNameViewModel> data2 = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var get_url = "api_new/Question/Search_Question?whereclause=" + str;
            //API Used of Term 1
            //HttpResponseMessage responseTerm = client.GetAsync("api_new/Question/Search_Question?whereclause=1=1").Result;
            HttpResponseMessage responseTerm_1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm1 = client.GetAsync("/api_new/question/GetQuestion_session").Result;
            HttpResponseMessage responseTerm2 = client.GetAsync("/api_new/MemberGetData/getmemberall").Result;
            HttpResponseMessage responseTerm3 = client.GetAsync("/api_new/question/GetAllministary").Result;
            //if (responseTerm.IsSuccessStatusCode)
            //{
            //    ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
            //    data = JsonConvert.DeserializeObject<List<GetAllIntegratedSearchViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
            //    {
            //        NullValueHandling = NullValueHandling.Ignore
            //    });
            //    ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
            //    data3 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
            //    {
            //        NullValueHandling = NullValueHandling.Ignore
            //    });
            //    ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
            //    data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
            //    {
            //        NullValueHandling = NullValueHandling.Ignore
            //    });
            //    ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
            //    data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
            //    {
            //        NullValueHandling = NullValueHandling.Ignore
            //    });
            //    ViewBag.SessionList = new SelectList(data3, "session_code", "Session_value");
            //    ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
            //    ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
            //    return Json(data, JsonRequestBehavior.AllowGet);
            //}
            if(responseTerm_1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm_1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllIntegratedSearchViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data3 = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.resultTerm3 = responseTerm3.Content.ReadAsStringAsync().Result;
                data2 = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm3, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                ViewBag.SessionList = new SelectList(data3, "session_code", "Session_value");
                ViewBag.List = new SelectList(data1, "MP_CODE", "MPFULLNAME");
                ViewBag.MinistryList = new SelectList(data2, "MIN_CODE", "MIN_NAME");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}