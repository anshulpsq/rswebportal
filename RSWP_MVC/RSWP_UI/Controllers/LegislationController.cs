﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.LegislationViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class LegislationController : BaseController
    {
        //public LegislationController()
        //{
        //    TempData["LegislativeIntroductionUrl"] = new BusinessController().GetLegislativeIntroductionUrl();
        //    TempData.Keep("LegislativeIntroductionUrl");
        //}
        // GET: Session Value For DropDown
        public void GetSessionVal()
        {
            List<SessionViewModel> data = new List<SessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/question/GetQuestion_session").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<SessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                TempData["SessionList"] = new SelectList(data, "session_code", "Session_value");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Bill Introduced(During Session)
        public ActionResult BillIntroduced()
        {
            GetSessionVal();
            return View();
        }
        [HttpPost]
        public JsonResult BillIntroduced_Result(string sessionVal)
        {
            List<GetAll_IntroducedbillViewModel> data = new List<GetAll_IntroducedbillViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetAllBillIntroduced_Sessionwise?sessionno=" + sessionVal;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_IntroducedbillViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["IntroduceBill"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bill Passed(During Session)
        public ActionResult BillPassed()
        {
            GetSessionVal();
            return View();
        }
        [HttpPost]
        public JsonResult BillPass_Result(string sessionVal)
        {
            List<GetAll_PassbillViewModel> data = new List<GetAll_PassbillViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetBill_Sessionwise?sessionno=" + sessionVal;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_PassbillViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PassBill"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region Latest Bills

        public ActionResult LatestBills()
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
               
                using (RS_DBEntities db = new RS_DBEntities())
                {
                  model.DocumentList=  db.Documents.Where(x => x.Section == CommanConstant.BillOffice && x.SubSection == CommanConstant.LatestBills && x.IsArchived == false && x.isPublished == true&&x.Language==LanguageMang.CurrentCultureLanguage).OrderByDescending(x=>x.selectedDate)
                        .Select(x=>new DocumentModelView() {  Name=x.Name, selectedDate=x.selectedDate, FileSize=x.FileSize, FileUrl=x.FileUrl, FileLocation=x.FileLocation, Tiltle=x.Tiltle})
                        .ToList();
                    if (model.DocumentList == null)
                        model.DocumentList = new List<DocumentModelView>();
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Business/LatestBills", line);
            }
            return View(model);
        }
        #endregion
        // GET: Bills with Committees
        public ActionResult BillCommittee()
        {
            return View();
        }
        [HttpPost]
        public JsonResult BillCommittee_Result()
        {
            List<GetAll_BillCommitteViewModel> data = new List<GetAll_BillCommitteViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/Committee_Web/GetBill_press";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_BillCommitteViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["BillComm"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult BillCommittee_PressRelese_Result()
        {
            List<GetAll_PressReleseViewModel> data = new List<GetAll_PressReleseViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetPress_relese_committee";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_PressReleseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PressRelese"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Ministry Data
        public void GetMinistryData()
        {
            List<MinistryNameViewModel> data = new List<MinistryNameViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseTerm = client.GetAsync("api_new/question/GetAllministary").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<MinistryNameViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["MinistryList"] = new SelectList(data, "MIN_CODE", "MIN_NAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Get Bill Search Result
        //[HttpPost]
        public JsonResult BillSearchResult(string house,string type,string status)
        {
            List<GetAll_BillSearchViewModel> data = new List<GetAll_BillSearchViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/bill/GetBill_All_byDefault";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAll_BillSearchViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["BillSearchData"] = data;
                //foreach(var item in data)
                //{
                //    if(item.House== house || house== "Both Houses")
                //    {
                //        if(item.type == type || type == "All")
                //        {
                //            if(item.status == status || status == "All")
                //            {

                //            }
                //        }
                //    }
                //}
                return Json(data.Where(x=>x.House==house && x.type==type && x.status==status), JsonRequestBehavior.AllowGet);
                //return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Bills Search (Advanced)
        public ActionResult BillSearch()                
        {
            GetMinistryData();
            return View();
        }
    }
}