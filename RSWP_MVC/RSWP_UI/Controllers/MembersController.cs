﻿using RSWP_DOM.Common;
using RSWP_UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSWP_UI.Models;
using System.Web.UI;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using RSWP_UI.Models.MemberViewModel;
using System.Drawing;
using System.IO;
using System.Web.UI.WebControls;
using RSWP_BAL.Repository;
using System.Configuration;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class MembersController : BaseController
    {
        // GET: Members_Temp
        public ActionResult Index(string id, int MenuId = -1,bool archieve=false)
        {
            DocumentModelView model = new DocumentModelView();
            Helpers helpers;
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Index");
            ViewBag.Param = id;
            ViewBag.TitlePostfix = "(Alphabetical)";
            switch (id)
            {
                case "TermWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "TermWise");
                    ViewBag.TitlePostfix = "(Term Wise)";
                    break;
                case "State":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "StateWise");
                    ViewBag.TitlePostfix = "(State Wise)";
                    break;
                case "Party":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "PartyWise");
                    ViewBag.TitlePostfix = "(Party Wise)";
                    break;
                case "PartyPosition":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "PartyPosition");

                    GetDocument(MenuId, out helpers, out model, archieve);
                    helpers.ProcessFile(model);
                    break;
                case "Email":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Email");
                    break;
                case "BilingualList":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "BilingualList");
                    break;
                case "BirthdayWise":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "BirthdayWise");
                    break;
                case "CouncilMinisters":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "CouncilMinisters");
                    break;
                case "EntitlementOfSittingMembers":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "EntitlementOfSittingMembers");

                    GetDocument(MenuId, out helpers, out model, archieve);

                    if (model.Id > 0)
                    {
                        Console.WriteLine("File exists...");
                    }
                    else
                    {
                        model = null;
                    }

                    break;
                case "EntitlementOfFormerMembers":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "EntitlementOfFormerMembers");
                    GetDocument(MenuId, out helpers, out model, archieve);
                    if (model.Id > 0)
                    {

                        Console.WriteLine("File exists...");
                    }
                    else
                    {
                        model = null;
                    }
                    break;
                case "CodeOfConduct":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "CodeOfConduct");
                    GetDocument(MenuId, out helpers, out model, archieve);
                    if (model.Id == 0)
                    {
                        model = null;
                    }
                    break;

                case "DeclarationofAssetsandLiabilities":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "DeclarationofAssetsandLiabilities");
                    GetDocument(MenuId, out helpers, out model, archieve);
                    //RS_DBEntities _entites1 = new RS_DBEntities();
                    //var lastDecAssRecord = _entites1.Documents.Where(x => x.MenuId == 891 && x.IsArchived == false).OrderByDescending(x=>x.FileUrl).FirstOrDefault();
                    //ViewBag.records = lastDecAssRecord;

                    if (model.Id == 0)
                    {
                        model = null;
                    }
                    break;

                case "GeneralInformation":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "GeneralInformation");
                    RS_DBEntities _entites = new RS_DBEntities();
                    //var YearList = _entites.Documents.Select(x => x.year ).Distinct().ToList();
                    var YearList = _entites.Documents.Where(x => x.MenuId == 887 && x.IsArchived == false).Select(x=>x.year).Distinct().ToList();
                    ViewBag.YearList = new SelectList(YearList, "year");
                    GetDocument(MenuId, out helpers, out model, archieve);
                    if (model.Id == 0)
                    {
                        model = null;
                    }
                    break;
                case "Women":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Women");
                    break;
                case "Address_Alpha":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Address_Alpha");
                    break;
                case "Address_Party":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Address_Party");
                    break;
                case "Address_State":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Address_State");
                    break;
                case "Former":
                    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "Former");
                    break;

                default:
                    break;
            }
            return View(model);
        }

        public ActionResult DisqualificationofMembers()
        {
            RS_DBEntities _entites = new RS_DBEntities();
            List<DocumentModelView> lstDocument = new List<DocumentModelView>();
            var getFileUrl = _entites.Documents.Where(x => x.MenuId == 889).ToList();
            if(getFileUrl != null)
            {
                DocumentModelView obj = new DocumentModelView();
                obj.FileUrl = getFileUrl[0].FileUrl;
                lstDocument.Add(obj);
            }
            return View(lstDocument);
        }

        public ActionResult GetFileByYear(int year)
        {
            RS_DBEntities _entites = new RS_DBEntities();
            var YearList = _entites.Documents.Where(x=>x.year == year).Select(m=>m.FileUrl).FirstOrDefault();
            return Json(YearList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeclarationOfAssetList()
        {
            RS_DBEntities _entites = new RS_DBEntities();

            var declarationList = _entites.Documents.Where(x => x.MenuId == 891  && x.IsArchived == false).ToList();
            ViewBag.list = declarationList;
            return View(declarationList);
        }

        #region Member Wise Action 04_Nov
        // Email Image
        public FileResult WriteTextAsImage(string id, int? width, int? height)
        {
            id = id.Replace(".", "[dot]").Replace("@", "[at]");
            int textSize = 10;
            if (!width.HasValue)
            {
                width = 355;
            }

            if (!height.HasValue)
            {
                height = 20;
            }

            Response.ContentType = "image/jpeg";
            string textToWrite = id;
            //string[] s = textToWrite.Split('|');
            //textToWrite = textToWrite.Replace("|", "\n");


            if (textToWrite.Trim().Length > 0)
            {
                Bitmap image = new Bitmap(width.Value, height.Value);
                Graphics g = null;
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        g = Graphics.FromImage(image);
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        Font f = new Font("Verdana", textSize, FontStyle.Regular);
                        SolidBrush b = new SolidBrush(Color.White);
                        g.FillRectangle(b, 0, 0, width.Value, height.Value);
                        g.DrawString(textToWrite, f, Brushes.Blue, 2, 3);
                        //g.DrawLine(new Pen(Color.Black, 1), new Point(5, 25), new Point(100, 0));

                        f.Dispose();
                        image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        return File(stream.ToArray(), "image/jpeg");
                    }
                }
                catch (Exception ee)
                {
                    throw;
                }
                finally
                {
                    image.Dispose();
                    g.Dispose();
                }
            }
            else
            {
                return File("", "");
            }
        }
        // Party DropDown
        public void Party()
        {
            List<GetPartyViewModel> data = new List<GetPartyViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetPartlist";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetPartyViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["party"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State DropDown
        public void State()
        {
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["state"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State DropDown Count Vac.
        public int StateCountVac(string name)
        {
            int result = 0;
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                foreach (var item in data)
                {
                    if (item.STATE_NAME == name)
                    {
                        result = item.NO_SEATS;
                        break;
                    }
                }
                return result;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return result;
        }
        // Alphabetical Wise Search
        public ActionResult Alphabetical()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Alphabetical_Result()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Alphabet_Result(char name)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                return Json(data.Where(item => item.MP_NAME[0] == name), JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Term Wise Member
        public ActionResult TermWise()
        {
            return View();
        }
        public ActionResult TermsWise()
        {
            return View();
        }
        [HttpPost]
        public JsonResult TermWise_Result(int term)
        {
            List<GetTermWiseMemberViewModel> data = new List<GetTermWiseMemberViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "";
            TempData["res"] = term;
            if (term == 0)
            {
                get_url = "api_new/MemberGetdata/GetMPTermWise?term=" + 1;
            }
            else
            {
                get_url = "api_new/MemberGetdata/GetMPTermWise?term=" + term;
            }
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetTermWiseMemberViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["TermWiseResult"] = data;

                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Party Wise Member
        public ActionResult PartyWise()
        {
            Party();
            return View();
        }
        [HttpPost]
        public JsonResult PartyWise_Result(string partycode)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                if (partycode == "--Please Select--")
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(data.Where(item => item.PARTY_CODE == partycode), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // State Wise Member
        public ActionResult StateWise()
        {
            State();
            return View();
        }
        public JsonResult StateWise_Result(string statecode)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                TempData["Ex"] = StateCountVac(statecode);
                if (statecode == "--Please Select--")
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(data.Where(item => item.STATE_NAME == statecode), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Age Wise Member
        public ActionResult AgeWise()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AgeWise_Result()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private static void GetDocument(int MenuId, out Helpers helpers, out DocumentModelView model, bool archive = false)
        {
            string language = LanguageMang.CurrentCultureLanguage;
            Documents document = new Documents();
            document.MenuId = $"{MenuId}";
            document.Language = language;
            document.IsArchived = archive;
            helpers = new Helpers();
            model = helpers.GetDocument(document);
            if (model != null)
            {
                helpers.ProcessFile(model);

                if (model.FileUrl != null && !model.FileUrl.Contains("http"))

                {
                    model.FileUrl = $"{ConfigurationManager.AppSettings["DocUrl"]}{model.FileUrl}";
                }
            }
        }


        private static void GetDocumentsBasedOnOrientation(int MenuId, out Helpers helpers, out List<DocumentModelView> model,bool archeive=false)
        {
            string language = LanguageMang.CurrentCultureLanguage;
            Documents document = new Documents();
            document.MenuId = $"{MenuId}";
            document.Language = language;
            document.IsArchived = archeive;
            helpers = new Helpers();
            model = helpers.GetOrientationDocuments(document);
        }

        private static void GetDocuments(int MenuId, out Helpers helpers, out List<DocumentModelView> model, bool archeive = false)
        {
            string language = LanguageMang.CurrentCultureLanguage;
            Documents document = new Documents();
            document.MenuId = $"{MenuId}";
            document.Language = language;
            document.IsArchived = archeive;
            helpers = new Helpers();
            model = helpers.GetDocuments(document);
        }

        // Email Address Wise Member
        public ActionResult EmailAddress()
        {
            Party();
            State();
            return View();
        }
        // Average Age of Member
        public ActionResult AverageAge()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
            }
            int count = 0;
            int TotalAge = 0;
            int dob = 0;
            foreach (var item in data)
            {
                if (item.DATE_BIRTH != "")
                {
                    if (item.DATE_BIRTH != null)
                    {
                        int age = 0;
                        count++;
                        string[] ageList = item.DATE_BIRTH.Split('/');
                        dob = Convert.ToInt32(ageList[2]);
                        age = DateTime.Now.Year - dob;
                        TotalAge = TotalAge + age;
                    }
                }
            }
            float avgage = (float)TotalAge / count;
            ViewBag.AgeResult = Math.Round(avgage, 2);
            ViewBag.MemberAgeCount = count;
            ViewBag.MemberCount = data.Count;
            return View();
        }
        // Billingual List of Member
        public ActionResult BillingualList()
        {
            return View();
        }
        // InCouncil of Ministers
        public ActionResult InCouncilMinisters()
        {
            return View();
        }
        [HttpPost]
        public JsonResult InCouncilMinisters_Result()
        {
            List<InCouncilMinistersViewModel> data = new List<InCouncilMinistersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/GetCouncilMinister";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<InCouncilMinistersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Women Members
        public ActionResult WomenMembers()
        {
            return View();
        }
        [HttpPost]
        public JsonResult WomenMembers_Result()
        {
            List<GetWomenMemberViewModel> data = new List<GetWomenMemberViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/GetWomenMember";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetWomenMemberViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Birthday Wise Members List
        public ActionResult BirthdayWise()
        {
            Party();
            State();
            return View();
        }
        public ActionResult BirthdaysWise()
        {
            Party();
            State();
            return View();
        }
        public ActionResult BirthdayWiseWithoutAdd()
        {
            Party();
            State();
            return View();
        }
        public ActionResult BirthdayWisesWithoutAdd()
        {
            Party();
            State();
            return View();
        }
        [HttpPost]
        public JsonResult DOBMonth_Result(string month)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MonthResult"] = data;
                TempData["month"] = month;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Retirement List(Alphabetical)
        public ActionResult RetirementList_Alphabetical()
        {
            Party();
            State();
            return View();
        }
        // Retirement List(Month/Year)
        public ActionResult RetirementList_Month_Year()
        {
            return View();
        }
        // Retirement List(State Wise)
        public ActionResult StateWiseRetirement()
        {
            State();
            return View();
        }
        // Member Address(Alphabetical Wise)
        public ActionResult AlphabeticalMemberAddress()
        {
            return View();
        }
        // Member Address(Party Wise)
        public ActionResult PartyWiseMemberAddress()
        {
            Party();
            return View();
        }
        // Member Address(State Wise)
        public ActionResult StateWiseMemberAddress()
        {
            State();
            return View();
        }
        // Nominated Member(Current)
        public ActionResult NominatedMember()
        {
            return View();
        }
        [HttpPost]
        public JsonResult NominatedMember_Result()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Nominated Member(Since 1952)
        public ActionResult NominatedMember_Since1952()
        {
            return View();
        }
        [HttpPost]
        public JsonResult NominatedMemberSince1952_Result()
        {
            List<NominatedMembers1952ViewModel> data = new List<NominatedMembers1952ViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetMembersince1952";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<NominatedMembers1952ViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Former Member(Alphabetical)
        public ActionResult AlphabeticalFormerMember()
        {
            List<GetFormerMemberViewModel> data = new List<GetFormerMemberViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/Getexmptermwise";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetFormerMemberViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["AlphabeticalFormer"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult AlphabetFormer_Result(char name)
        {
            List<GetFormerMemberViewModel> data = new List<GetFormerMemberViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/Getexmptermwise";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetFormerMemberViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                return Json(data.Where(item => item.MP_NAME[0] == name), JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Former Member(Term Wise)
        public ActionResult TermWiseFormerMember()
        {
            return View();
        }
        public ActionResult TermsWiseFormerMember()
        {
            return View();
        }
        [HttpPost]
        public JsonResult FormerMemberTerm_Result(int term)
        {
            List<GetFormerMemberViewModel> data = new List<GetFormerMemberViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/Getexmptermwise";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetFormerMemberViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["res"] = term;
                Session["FormerTerm"] = data;
                //return View(data);
                if (term != 0)
                {
                    return Json(data.Where(item => item.totalcount == term), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Former Member(3rd April 1952)
        //Former Member(brief bio-data)

        //Summary Report
        //State Wise
        public ActionResult StateWiseSummary()
        {
            State();
            return View();
        }
        //Party Wise
        public ActionResult PartyWiseSummary()
        {
            Party();
            return View();
        }
        //Advance Search
        public void EducationProfession()
        {
            List<GetEducationalProfessionViewModel> data = new List<GetEducationalProfessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/MemberGetdata/GetEducationalprofession").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetEducationalProfessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["EP"] = new SelectList(data, "PROF_CODE", "PROF_NAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void Educationallevel()
        {
            List<GetQualificationViewModel> data = new List<GetQualificationViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm = client.GetAsync(CommanConstant.urlContent + "/api_new/memberGetdata/Getqualification").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetQualificationViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["Level"] = new SelectList(data, "LEVEL_CODE", "LEVEL_NAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        [HttpPost]
        public JsonResult GetQualificationDegreeMaster(string quali)
        {
            List<GetQualiMasterDegreeViewModel> data = new List<GetQualiMasterDegreeViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetQualiDegreeMaster?quali=" + quali;
            HttpResponseMessage responseTerm = client.GetAsync(get_url).Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetQualiMasterDegreeViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQualificationSubjectMaster(string quali, string degree)
        {
            List<GetQualificationSubjectMasterViewModel> data = new List<GetQualificationSubjectMasterViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetQualiSubjectMaster?quali=" + quali + "&degree=" + degree;
            HttpResponseMessage responseTerm = client.GetAsync(get_url).Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetQualificationSubjectMasterViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AdvanceSearch()
        {
            State();
            Party();
            EducationProfession();
            Educationallevel();
            return View();
        }
        //Former Member Search
        public ActionResult FormerMemberSearch()
        {
            State();
            Party();
            EducationProfession();
            Educationallevel();
            return View();
        }
        //Consolidated Who's Who
        public ActionResult MembersAdvanceSearch()
        {
            State();
            Party();
            EducationProfession();
            Educationallevel();
            return View();
        }
        //Expenditure incurred
        public void LeaderofOpposition()
        {
            List<GetOppositionleaderViewModel> data = new List<GetOppositionleaderViewModel>();
            List<GetCurrentMemberBiodataViewModel> data1 = new List<GetCurrentMemberBiodataViewModel>();
            List<GetCurrentMemberBiodataViewModel> data2 = new List<GetCurrentMemberBiodataViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/memberGetdata/GetOppositionleader").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetOppositionleaderViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                int mpCode = 0;
                foreach (var item in data)
                {
                    mpCode = item.MP_CODE;
                    break;
                }
                HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/Memberweb/GetCurrentMember_Biodata?mpcode=" + mpCode).Result;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetCurrentMemberBiodataViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["AllData"] = data1;
                var dropdowndata = "";
                var image = "";
                var fullname = "";
                foreach (var item in data1)
                {
                    dropdowndata = item.MP_FNAME + item.MP_LNAME + "," + item.MP_INIT;
                    fullname = item.MP_INIT + item.MP_FNAME + item.MP_LNAME;
                    TempData["LeaderOPCode"] = item.MP_CODE;
                    ViewBag.stateName = item.STATE_NAME;
                    ViewBag.partyName = item.PARTY_NAME;
                    ViewBag.Ladd = item.C_LADDRESS + item.C_LPIN;
                    ViewBag.teliNo = item.C_LTELE;
                    ViewBag.Padd = item.C_PADDRESS + item.C_PPIN;
                    ViewBag.email = item.C_EMAIL_ID;
                    image = "http://164.100.47.5/newmembers/photos/P" + item.MP_CODE + ".jpg";
                }
                TempData["LeaderOP"] = dropdowndata;
                TempData["LeaderOPName"] = fullname;
                TempData["LeaderOPImage"] = image;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void HonbleDeputyChairman()
        {
            List<GetDeputyChairmanViewModel> data = new List<GetDeputyChairmanViewModel>();
            List<GetCurrentMemberBiodataViewModel> data1 = new List<GetCurrentMemberBiodataViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/memberGetdata/GetDeputyChairman").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetDeputyChairmanViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                int mpCode = 0;
                foreach (var item in data)
                {
                    mpCode = item.MP_CODE;
                    break;
                }
                HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/Memberweb/GetCurrentMember_Biodata?mpcode=" + mpCode).Result;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetCurrentMemberBiodataViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                var dropdowndata = "";
                var fullname = "";
                var image = "";
                foreach (var item in data1)
                {
                    dropdowndata = item.MP_FNAME + item.MP_LNAME + ", " + item.MP_INIT;
                    fullname = item.MP_INIT + item.MP_FNAME + item.MP_LNAME;
                    TempData["DeputyChairManCode"] = item.MP_CODE;
                    image = "http://164.100.47.5/newmembers/photos/P" + item.MP_CODE + ".jpg";
                }
                TempData["DeputyChairMan"] = dropdowndata;
                TempData["DeputyChairManName"] = fullname;
                TempData["DeputyChairManImage"] = image;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void LeaderofHouse()
        {
            List<GetOppositionleaderViewModel> data = new List<GetOppositionleaderViewModel>();
            List<GetCurrentMemberBiodataViewModel> data1 = new List<GetCurrentMemberBiodataViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("Api_new/MemberGetdata/GetLeaderofHouse").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetOppositionleaderViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                int mpCode = 0;
                foreach (var item in data)
                {
                    mpCode = item.MP_CODE;
                    break;
                }
                HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/Memberweb/GetCurrentMember_Biodata?mpcode=" + mpCode).Result;
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetCurrentMemberBiodataViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                var dropdowndata = "";
                var image = "";
                var fullname = "";
                foreach (var item in data1)
                {
                    dropdowndata = item.MP_FNAME + item.MP_LNAME + "," + item.MP_INIT;
                    fullname = item.MP_INIT + item.MP_FNAME + item.MP_LNAME;
                    TempData["LeaderHouseCode"] = item.MP_CODE;
                    image = "http://164.100.47.5/newmembers/photos/P" + item.MP_CODE + ".jpg";
                }
                TempData["LeaderHouse"] = dropdowndata;
                TempData["LeaderHouseName"] = fullname;
                TempData["LeaderHouseImage"] = image;
                TempData["ItemResult"] = data1;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void SecretaryGeneral()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/membergetdata/GetRS_SG_BIODATA").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["SecretaryGeneralData"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public ActionResult ExpenditureIncurred()
        {
            List<GetAllMemberDetailsViewModel> data = new List<GetAllMemberDetailsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            HttpResponseMessage responseTerm1 = client.GetAsync("api_new/MemberGetData/getmemberall").Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAllMemberDetailsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //ViewBag.Count = data.Count;
                ViewBag.List = new SelectList(data, "MP_CODE", "MPFULLNAME");
                LeaderofOpposition();
                HonbleDeputyChairman();
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult ExpenditureIncurred_Result(string mpcode, string duration1, string duration2)
        {
            List<GetExpenditureIncurredViewModel> data = new List<GetExpenditureIncurredViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetExpenditureOccured?mpcode=" + mpcode + "&duration1=" + duration1 + "&duration2=" + duration2;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetExpenditureIncurredViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // LOP Deputy
        public JsonResult ExpenditureIncurredOP_Result(string mpcode, string duration1, string duration2)
        {
            List<GetExpenditureOPViewModel> data = new List<GetExpenditureOPViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/memberGetdata/GetExpenditureOccuredLOPDeputy?mpcode=" + mpcode + "&duration1=" + duration1 + "&duration2=" + duration2;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetExpenditureOPViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Model Result
        //General Information
        [HttpPost]
        public JsonResult ModelGeneral_Result(int mpcode)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data.Where(x => x.MP_CODE == mpcode), JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Member Attendance
        public void sesDate(string sessNo)
        {
            List<GetSessionViewModel> data = new List<GetSessionViewModel>();
            List<GetSessionViewModel> data1 = new List<GetSessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetdata/GetSession";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            HttpResponseMessage responseTerm2 = client.GetAsync(CommanConstant.urlContent + "/api_new/MemberGetdata/GetSessionDate?session=" + sessNo).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                foreach (var item in data)
                {
                    if (item.sessionno == sessNo)
                    {
                        TempData["strSessionDate"] = item.period;
                        TempData["lstSessionDate"] = item.period2;
                        TempData["totaldays"] = item.totaldays;
                        break;
                    }
                }
                ViewBag.resultTerm2 = responseTerm2.Content.ReadAsStringAsync().Result;
                data1 = JsonConvert.DeserializeObject<List<GetSessionViewModel>>(ViewBag.resultTerm2, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                int count = 0;
                for (int i = 0; i < data1.Count; i++)
                {
                    count++;
                }
                TempData["sittingdays"] = count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public JsonResult selectSession()
        {
            List<GetSessionViewModel> data = new List<GetSessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetdata/GetSession";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["SNo"] = new SelectList(data, "sessionno", "sessionno");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult selectDate(string session)
        {
            List<GetSessionViewModel> data = new List<GetSessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetdata/GetSessionDate?session=" + session;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MemberAttendance()
        {
            selectSession();
            return View();
        }
        public JsonResult GetSessionDetails(string sesCode)
        {
            List<GetSessionViewModel> data = new List<GetSessionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetdata/GetMemberAttendance?session=" + sesCode;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSessionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                sesDate(sesCode);
                TempData["sessionCode"] = sesCode;
                Session["sessionWiseData"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SessionWiseResults()
        {
            return View();
        }
        //Officer of Rajya Sabha
        public ActionResult OfficersOfRajyaSabha()
        {
            LeaderofOpposition();
            HonbleDeputyChairman();
            LeaderofHouse();
            SecretaryGeneral();
            return View();
        }
        public ActionResult SC()
        {
            SecretaryGeneral();
            return View();
        }
        #endregion

        #region
        public ActionResult BriefBio_Index(int MenuId = -1, bool archieve = false)
        {
            if (MenuId <= 0)
            {
                return RedirectToAction("Index");
            }
            RS_DBEntities _entites = new RS_DBEntities();

            var _topRawBrifList = _entites.vwOrientationDocuments.Where(x => x.MenuId == 896 && x.RowType == "TopRowBrief," && x.IsArchived == false).ToList();
            ViewBag.topRawList = _topRawBrifList;

            var BIOSKETCHESList = _entites.vwOrientationDocuments.Where(x => x.MenuId == 896 && x.RowType == "BIOSKETCHES," && x.IsArchived == false).ToList();
            ViewBag.BioSkeetchList = BIOSKETCHESList;

            var MembersBriefList = _entites.vwOrientationDocuments.Where(x => x.MenuId == 896 && x.RowType == "MembersBrief," && x.IsArchived == false).ToList();
            ViewBag.MemberBrifList = MembersBriefList;
            return View();
        }

        public ActionResult GeneralInformation_Index(int MenuId = -1, bool archieve = false)
        {
            if (MenuId <= 0)
            {
                return RedirectToAction("Index");


            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "BreifBio");
            string viewName = GetViewName(MenuId);
            List<DocumentModelView> model = GetDocumentsData(viewName, MenuId, archieve);

            return View(viewName, model);
        }

        public ActionResult MPLADS_Index(int MenuId = -1, bool archieve = false)
        {
            if (MenuId <= 0)
            {
                return RedirectToAction("Index");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "MPLADS");
            string viewName = GetViewName(MenuId);
            List<DocumentModelView> model = GetData(viewName, MenuId, archieve);
            return View(viewName, model);
        }


        public ActionResult Orientation_Index(int MenuId = -1, bool archieve = false)
        {
            RS_DBEntities _entites = new RS_DBEntities();
            var OrientationPreceddingList = _entites.vwOrientationDocuments.Where(x => x.MenuId == 895 && x.RowType == "OrientationProgramProceeding").ToList();
            ViewBag.list = OrientationPreceddingList;

            var topRowList = _entites.vwOrientationDocuments.Where(x => x.MenuId == 895 && x.RowType == "TopRow").ToList();
            ViewBag.toprow = topRowList;
            return View();
        }
        public ActionResult SalaryActAndRules_Index(int MenuId = -1, bool archieve = false)
        {
            if (MenuId <= 0)
            {
                return RedirectToAction("Index");


            }
            RS_DBEntities _entites = new RS_DBEntities();

            var salaryActsRulesList = _entites.Documents.Where(x => x.MenuId == 894 && x.IsArchived == false).ToList();
            ViewBag.SalaryActList = salaryActsRulesList;
            return View();
            //TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("Members", "SalaryAct");
            //string viewName = GetViewName(MenuId);
            //List<DocumentModelView> model = GetData(viewName, MenuId, archieve);
        }


        private string GetViewName(int menuId)
        {
            //Helpers helpers = new Helpers();
            //var menu = helpers.GetDocumentMenuDetailsByInternalKey(menuId);
            //if (menu == null)
            //    return "Index";

            if (menuId == 25011)
                return "BriefBio_Index";
            if (menuId == 25010)
                return "Orientation_Index";
            if (menuId == 25012)
                return "MPLADS_Index";
            if (menuId == 25009)
                return "SalaryActAndRules_Index";
            if (menuId == 25002)
                return "GeneralInformation_Index";

            return "Index";
        }

        private List<DocumentModelView> GetData(string viewName, int menuId,bool archieve=false)
        {
            List<DocumentModelView> model = default(List<DocumentModelView>);
            Helpers helper = default(Helpers);
            GetDocumentsBasedOnOrientation(menuId, out helper, out model, archieve);
            return model;
        }

        private List<DocumentModelView> GetDocumentsData(string viewName, int menuId, bool archieve = false)
        {
            List<DocumentModelView> model = default(List<DocumentModelView>);
            Helpers helper = default(Helpers);
            GetDocuments(menuId, out helper, out model, archieve);
            return model;
        }
        #endregion


        #region MPLADS
        public ActionResult CommitteeOnMPLADS()
        {
            return View();
        }
        public ActionResult MPLADSVideo()
        {
            RS_DBEntities _entites = new RS_DBEntities();

            var mpladsVideo = _entites.vwOrientationDocuments.Where(x => x.MenuId == 897 && x.IsArchived == false).ToList();
            ViewBag.list = mpladsVideo;
            return View(mpladsVideo);
        }

        #endregion
    }

}