﻿using RSWP_BAL.Repository;
using RSWP_DOM.Common;
using RSWP_UI.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    /// <summary>
    /// Responsible for Recruitment Cells all operations
    /// </summary>
    [WebLinksFilter]
    public class CareersInRajyaSabhaSecretariatController : BaseController
    {
        string careersfilePath = Convert.ToString(ConfigurationManager.AppSettings["careersfilePath"]);
        string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["dateFormat"]);
        public ActionResult Index(string queryType = "RecruitmentRules")
        {
            List<RecruitmentCell> model = new List<RecruitmentCell>();
            try
            {
                TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "Index");
                switch (queryType)
                {
                    case "FOCExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "FOCExamination";
                        model = FExamination("FOCExamination");
                        break;
                    case "FDExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "FDExamination";
                        model = FExamination("FDExamination");
                        break;
                    case "SOCExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "SOCExamination";
                        model = SExamination("SOCExamination");
                        break;
                    case "SDExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "SDExamination";
                        model = SExamination("SDExamination");
                        break;
                    case "SYOCExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "SYOCExamination";
                        model = SYExamination("SYOCExamination");
                        break;
                    case "SYDExamination":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "SYDExamination";
                        model = SYExamination("SYDExamination");
                        break;
                    case "LatestAdvertisments":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "LatestAdvertisments";
                        model = LatestAdvertisments();
                        break;
                    case "StatusOfApplication":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "StatusOfApplication";
                        model = StatusOfApplication();
                        break;
                    case "OCareers":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "OCareers";
                        model = Careers("OCareers");
                        break;
                    case "DCareers":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "DCareers";
                        model = Careers("DCareers");
                        break;
                    case "DACard":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "DACard";
                        model = DownloadAdmit();
                        break;
                    case "ESchedule":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "ESchedule";
                        model = ExaminationSchedule();
                        break;
                    case "CInstruction":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "CInstruction";
                        model = CandidatesInstruction();
                        break;
                    case "Result":
                        //ViewBag.flag = false;
                        ViewBag.switchName = "Result";
                        model = Result();
                        break;
                    case "Disclaimer":
                        ViewBag.switchName = "Disclaimer";
                        break;
                    case "ContactUs":
                        ViewBag.switchName = "ContactUs";
                        break;
                    case "archiveRecruitmentRules":
                        ViewBag.switchName = "archiveRecruitmentRules";
                        model = ArchiveRecruitmentRules();
                        break;
                    default:
                        //ViewBag.flag = true;
                        ViewBag.switchName = "RecruitmentRules";
                        model = RecruitmentRules();
                        break;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                string line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "CareersInRajyaSabhaSecretariat/Index", line);
                return View(model);
            }
        }


        ///// <summary>
        ///// Responsible for Get Result operations
        ///// </summary>
        //[NonAction]
        //public ActionResult Disclaimer()
        //{
        //    TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "Disclaimer");
        //    return View();
        //}


        

        /// <summary>
        /// Responsible for Get Result operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> Result()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Result";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsResult = new DataSet();
            if (getRecruitmentCell.GetResult(ref dsResult, recruitmentCell, ref ErrorMsg))
            {
                if (dsResult.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsResult.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            AdvertisementsNo = Convert.ToString(dsResult.Tables[0].Rows[i]["AdvertisementsNo"]),
                            Title = Convert.ToString(dsResult.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsResult.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsResult.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsResult.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsResult.Tables[0].Rows[i]["FileName"]),
                            UploadDate = DateTime.Parse(dsResult.Tables[0].Rows[i]["UploadDate"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "Result", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "Result"); 
            return list;
        }

        /// <summary>
        /// Responsible for Get Candidates Instruction operations
        /// </summary>

        [NonAction]
        public List<RecruitmentCell> CandidatesInstruction()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Candidates Instruction";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsCandidatesInstruction = new DataSet();
            if (getRecruitmentCell.GetCandidatesInstruction(ref dsCandidatesInstruction, recruitmentCell, ref ErrorMsg))
            {
                if (dsCandidatesInstruction.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsCandidatesInstruction.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            AdvertisementsNo = Convert.ToString(dsCandidatesInstruction.Tables[0].Rows[i]["AdvertisementsNo"]),
                            Title = Convert.ToString(dsCandidatesInstruction.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsCandidatesInstruction.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsCandidatesInstruction.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsCandidatesInstruction.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsCandidatesInstruction.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "CandidatesInstruction", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "CandidatesInstruction");
            return list;
        }

        /// <summary>
        /// Responsible for Get Examination Schedule operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> ExaminationSchedule()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Examination Schedule";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsExaminationSchedule = new DataSet();
            if (getRecruitmentCell.GetExaminationSchedule(ref dsExaminationSchedule, recruitmentCell, ref ErrorMsg))
            {
                if (dsExaminationSchedule.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsExaminationSchedule.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            AdvertisementsNo = Convert.ToString(dsExaminationSchedule.Tables[0].Rows[i]["AdvertisementsNo"]),
                            Title = Convert.ToString(dsExaminationSchedule.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsExaminationSchedule.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsExaminationSchedule.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsExaminationSchedule.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsExaminationSchedule.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "ExaminationSchedule", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "ExaminationSchedule");
            return list;
        }


        /// <summary>
        /// Responsible for Get Admit Cards operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> DownloadAdmit()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Downloads Admit Card";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetAdmitCards = new DataSet();
            if (getRecruitmentCell.GetAdmitCards(ref dsGetAdmitCards, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetAdmitCards.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetAdmitCards.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            URL = Convert.ToString(dsGetAdmitCards.Tables[0].Rows[i]["URL"]),
                            Title = Convert.ToString(dsGetAdmitCards.Tables[0].Rows[i]["Title"]),
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "DownloadAdmit", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "DownloadAdmit");
            return list;
        }

        /// <summary>
        /// Responsible for Get Careers operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> Careers(string type)
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Career";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetCareers = new DataSet();
            if (getRecruitmentCell.GetCareers(ref dsGetCareers, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetCareers.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetCareers.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Flag = type,
                            ExamDescription = Convert.ToString(dsGetCareers.Tables[0].Rows[i]["ExamDescription"]),
                            ExamDescription2 = Convert.ToString(dsGetCareers.Tables[0].Rows[i]["ExamDescription2"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "Careers", "");
            }
            if (type == "OCareers")
            {
                TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "OCareers");
            }
            else
            {
                TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "DCareers");
            }
            
            return list;
        }

        /// <summary>
        /// Responsible for Get Status Of Application operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> StatusOfApplication()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Status of Application";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetStatusOfApplication = new DataSet();
            if (getRecruitmentCell.GetStatusOfApplication(ref dsGetStatusOfApplication, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetStatusOfApplication.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetStatusOfApplication.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            AdvertisementsNo = Convert.ToString(dsGetStatusOfApplication.Tables[0].Rows[i]["AdvertisementsNo"]),
                            Title = Convert.ToString(dsGetStatusOfApplication.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetStatusOfApplication.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetStatusOfApplication.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetStatusOfApplication.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetStatusOfApplication.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "StatusOfApplication", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "StatusOfApplication");
            return list;
        }

        /// <summary>
        /// Responsible for Get Latest Advertisments operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> LatestAdvertisments()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Latest Advertisments";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetLatestAdvertisments = new DataSet();
            if (getRecruitmentCell.GetLatestAdvertisments(ref dsGetLatestAdvertisments, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetLatestAdvertisments.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetLatestAdvertisments.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            AdvertisementsNo = Convert.ToString(dsGetLatestAdvertisments.Tables[0].Rows[i]["AdvertisementsNo"]),
                            Title = Convert.ToString(dsGetLatestAdvertisments.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetLatestAdvertisments.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetLatestAdvertisments.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetLatestAdvertisments.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetLatestAdvertisments.Tables[0].Rows[i]["FileName"]),
                            LastDateofApplication = DateTime.Parse(dsGetLatestAdvertisments.Tables[0].Rows[i]["LastDateofApplication"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "LatestAdvertisments", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "LatestAdvertisments");
            return list;
        }

        /// <summary>
        /// Responsible for Get Syllabus of Examination operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> SYExamination(string examType)
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Syllabus of Examinations";
            List<SelectListItem> breadcrumb;
            if (examType == "SYOCExamination")
            {
                recruitmentCell.SubType = "Open Competitive Examination";
                ViewBag.SyllabusOfExamination = Resources.Resource.OpenCompetativeExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "SYOCExamination");
            }
            else
            {
                recruitmentCell.SubType = "Departmental Examination";
                ViewBag.SyllabusOfExamination = Resources.Resource.DepartmentalExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "SYDExamination");
            }

            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetSyllabusOfExamination = new DataSet();
            if (getRecruitmentCell.GetSyllabusOfExamination(ref dsGetSyllabusOfExamination, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetSyllabusOfExamination.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetSyllabusOfExamination.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Title = Convert.ToString(dsGetSyllabusOfExamination.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetSyllabusOfExamination.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetSyllabusOfExamination.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetSyllabusOfExamination.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetSyllabusOfExamination.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "SYExamination", "");
            }
            TempData["BreadCrumb"] = breadcrumb;
            return list;
        }

        /// <summary>
        /// Responsible for Get Scheme of Examination operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> SExamination(string examType)
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Scheme of Examination";
            List<SelectListItem> breadcrumb;
            if (examType == "SOCExamination")
            {
                recruitmentCell.SubType = "Open Competitive Examination";
                ViewBag.SchemeOfExamination = Resources.Resource.OpenCompetativeExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "SOCExamination");
            }
            else
            {
                recruitmentCell.SubType = "Departmental Examination";
                ViewBag.SchemeOfExamination = Resources.Resource.DepartmentalExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "SDExamination");
            }

            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetSchemeOfExamination = new DataSet();
            if (getRecruitmentCell.GetSchemeOfExamination(ref dsGetSchemeOfExamination, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetSchemeOfExamination.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetSchemeOfExamination.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Title = Convert.ToString(dsGetSchemeOfExamination.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetSchemeOfExamination.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetSchemeOfExamination.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetSchemeOfExamination.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetSchemeOfExamination.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "SExamination", "");
            }
            TempData["BreadCrumb"] = breadcrumb;
            return list;
        }

        /// <summary>
        /// Responsible for Forthcoming Examination operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> FExamination(string examType)
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Forthcoming Examination";
            List<SelectListItem> breadcrumb;
            if (examType == "FOCExamination")
            {
                recruitmentCell.SubType = "Open Competitive Examination";
                ViewBag.ForthcomingExamination = Resources.Resource.OpenCompetativeExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "FOCExamination");
            }
            else
            {
                recruitmentCell.SubType = "Departmental Examination";
                ViewBag.ForthcomingExamination = Resources.Resource.DepartmentalExamination;
                breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "FDExamination");
            }
                
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;

            string ErrorMsg = string.Empty;
            

            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetForthcomingExamination = new DataSet();
            if (getRecruitmentCell.GetForthcomingExamination(ref dsGetForthcomingExamination, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetForthcomingExamination.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetForthcomingExamination.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Title = Convert.ToString(dsGetForthcomingExamination.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetForthcomingExamination.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetForthcomingExamination.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetForthcomingExamination.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetForthcomingExamination.Tables[0].Rows[i]["FileName"]),
                            NoofPosts = Convert.ToInt32(dsGetForthcomingExamination.Tables[0].Rows[i]["NoofPosts"]),
                            LastDateofApplication = DateTime.Parse(dsGetForthcomingExamination.Tables[0].Rows[i]["LastDateofApplication"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "FExamination", "");
            }
            TempData["BreadCrumb"] = breadcrumb;
            return list;
        }

        /// <summary>
        /// Responsible for Recruitment Rules operations
        /// </summary>
        [NonAction]
        public List<RecruitmentCell> RecruitmentRules()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Recruitment rules";
            recruitmentCell.IsPublished = true;
            recruitmentCell.IsArchived = false;
            
            string ErrorMsg = string.Empty;
            List<SelectListItem> breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "RecruitmentRules");

            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetRecruitmentRules = new DataSet();
            if (getRecruitmentCell.GetRecruitmentRules(ref dsGetRecruitmentRules, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetRecruitmentRules.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetRecruitmentRules.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Title = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetRecruitmentRules.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "RecruitmentRules", "");
            }
            TempData["BreadCrumb"] = breadcrumb;
            return list;
        }

        [NonAction]
        public List<RecruitmentCell> ArchiveRecruitmentRules()
        {
            List<RecruitmentCell> list = new List<RecruitmentCell>();
            RecruitmentCell recruitmentCell = new RecruitmentCell();
            recruitmentCell.Type = "Recruitment rules";
            recruitmentCell.IsPublished = false;
            recruitmentCell.IsArchived = true;

            string ErrorMsg = string.Empty;
            List<SelectListItem> breadcrumb = BreadCrumbSetter.GetBreadCrumb("CareersInRajyaSabhaSecretariat", "RecruitmentRules");

            GetRecruitmentCell getRecruitmentCell = new GetRecruitmentCell();
            DataSet dsGetRecruitmentRules = new DataSet();
            if (getRecruitmentCell.GetRecruitmentRules(ref dsGetRecruitmentRules, recruitmentCell, ref ErrorMsg))
            {
                if (dsGetRecruitmentRules.Tables[0].Rows.Count > 0)
                {
                    RecruitmentCell objRecruitmentCell;
                    for (int i = 0; i < dsGetRecruitmentRules.Tables[0].Rows.Count; i++)
                    {
                        objRecruitmentCell = new RecruitmentCell()
                        {
                            Title = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsGetRecruitmentRules.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = careersfilePath + Convert.ToString(dsGetRecruitmentRules.Tables[0].Rows[i]["FileName"])
                        };
                        list.Add(objRecruitmentCell);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "RecruitmentRules", "");
            }
            TempData["BreadCrumb"] = breadcrumb;
            return list;
        }
    }
}