﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_DOM.Common;
using RSWP_UI.Common;
using RSWP_UI.Models.RajyaSabhaSecretariatViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    /// <summary>
    /// Responsible for Rajya Sabha Secretariat all operations
    /// </summary>
    [WebLinksFilter]
    public class RajyaSabhaSecretariatController : BaseController
    {
        string RSSFilesPath = Convert.ToString(ConfigurationManager.AppSettings["RSSFilesPath"]);
        string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["dateFormat"]);
        public ActionResult Index(string queryType = "")
        {
            List<RSSecretariat> model = new List<RSSecretariat>();
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "Index");
            try
            {
                switch (queryType)
                {
                    case "DemandGrants":
                        ViewBag.pageTitle = Resources.Resource.DemandsForGrants;
                        ViewBag.switchName = "DemandGrants";
                        model = DemandGrants();
                        break;
                    case "TDirectories":
                        ViewBag.pageTitle = Resources.Resource.TelephoneDirectories_AlliedServices;
                        ViewBag.switchName = "TDirectories";
                        model = TelephoneDirectories();
                        break;
                    case "SPosition":
                        ViewBag.pageTitle = Resources.Resource.SanctionedAndInPositionInTheSecretariat;
                        ViewBag.switchName = "SPosition";
                        model = SenctionedPosition();
                        break;
                    case "MProcedure":
                        ViewBag.pageTitle = Resources.Resource.ManualOfofficeProcedure;
                        ViewBag.switchName = "MProcedure";
                        model = ManualOfficeProcedure();
                        break;
                    case "AAReports":
                        ViewBag.pageTitle = Resources.Resource.AnnualAdministrativeReports;
                        ViewBag.switchName = "AAReports";
                        model = AnnualAdministrativeReports();
                        break;
                    default:
                        ViewBag.pageTitle = "";
                        ViewBag.switchName = "Default";
                        model = Default();
                        break;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                string line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "RajyaSabhaSecretariat/Index", line);
                return View(model);
            }
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Annual Administrative Reports
        /// </summary>
        [NonAction]
        public List<RSSecretariat> AnnualAdministrativeReports()
        {
            List<RSSecretariat> list = new List<RSSecretariat>();
            RSSecretariat rssecretariat = new RSSecretariat();
            rssecretariat.Type = "Annual-Administrative Reports";
            rssecretariat.IsPublished = true;
            rssecretariat.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRSSecretariat getRSSecretariat = new GetRSSecretariat();
            DataSet dsAnnualAdministrativeReports = new DataSet();
            if (getRSSecretariat.GetAnnualAdministrativeReports(ref dsAnnualAdministrativeReports, rssecretariat, ref ErrorMsg))
            {
                if (dsAnnualAdministrativeReports.Tables[0].Rows.Count > 0)
                {
                    RSSecretariat objRSSecretariat;
                    for (int i = 0; i < dsAnnualAdministrativeReports.Tables[0].Rows.Count; i++)
                    {
                        objRSSecretariat = new RSSecretariat()
                        {
                            Year = Convert.ToString(dsAnnualAdministrativeReports.Tables[0].Rows[i]["Year"]),
                            FileName = Convert.ToString(dsAnnualAdministrativeReports.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsAnnualAdministrativeReports.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsAnnualAdministrativeReports.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = RSSFilesPath + Convert.ToString(dsAnnualAdministrativeReports.Tables[0].Rows[i]["FileName"]),
                            Date = DateTime.Parse(dsAnnualAdministrativeReports.Tables[0].Rows[i]["Date"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRSSecretariat);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "AnnualAdministrativeReports", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "AnnualAdministrativeReports");
            return list;
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Manual Office Procedure
        /// </summary>
        [NonAction]
        public List<RSSecretariat> ManualOfficeProcedure()
        {
            List<RSSecretariat> list = new List<RSSecretariat>();
            RSSecretariat rssecretariat = new RSSecretariat();
            rssecretariat.Type = "Manual of office Procedure";
            rssecretariat.IsPublished = true;
            rssecretariat.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRSSecretariat getRSSecretariat = new GetRSSecretariat();
            DataSet dsManualOfficeProcedure = new DataSet();
            if (getRSSecretariat.GetManualOfficeProcedure(ref dsManualOfficeProcedure, rssecretariat, ref ErrorMsg))
            {
                if (dsManualOfficeProcedure.Tables[0].Rows.Count > 0)
                {
                    RSSecretariat objRSSecretariat;
                    for (int i = 0; i < dsManualOfficeProcedure.Tables[0].Rows.Count; i++)
                    {
                        objRSSecretariat = new RSSecretariat()
                        {
                            Year = Convert.ToString(dsManualOfficeProcedure.Tables[0].Rows[i]["Year"]),
                            FileName = Convert.ToString(dsManualOfficeProcedure.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsManualOfficeProcedure.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsManualOfficeProcedure.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = RSSFilesPath + Convert.ToString(dsManualOfficeProcedure.Tables[0].Rows[i]["FileName"]),
                            Date = DateTime.Parse(dsManualOfficeProcedure.Tables[0].Rows[i]["Date"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRSSecretariat);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "ManualOfficeProcedure", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "ManualOfficeProcedure");
            return list;
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Senctioned Position
        /// </summary>
        [NonAction]
        public List<RSSecretariat> SenctionedPosition()
        {
            List<RSSecretariat> list = new List<RSSecretariat>();
            RSSecretariat rssecretariat = new RSSecretariat();

            rssecretariat.IsPublished = true;
            rssecretariat.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRSSecretariat getRSSecretariat = new GetRSSecretariat();
            DataSet dsSenctionedPosition = new DataSet();
            if (getRSSecretariat.GetSenctionedPosition(ref dsSenctionedPosition, rssecretariat, ref ErrorMsg))
            {
                if (dsSenctionedPosition.Tables[0].Rows.Count > 0)
                {
                    RSSecretariat objRSSecretariat;
                    for (int i = 0; i < dsSenctionedPosition.Tables[0].Rows.Count; i++)
                    {
                        objRSSecretariat = new RSSecretariat()
                        {
                            Title = Convert.ToString(dsSenctionedPosition.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsSenctionedPosition.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsSenctionedPosition.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsSenctionedPosition.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = RSSFilesPath + Convert.ToString(dsSenctionedPosition.Tables[0].Rows[i]["FileName"]),
                            PublishDate = DateTime.Parse(dsSenctionedPosition.Tables[0].Rows[i]["IsPublishedDate"].ToString()).ToString(@dateFormat)
                        };
                        list.Add(objRSSecretariat);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "SenctionedPosition", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "SenctionedPosition");
            return list;
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Telephone Directories
        /// </summary>
        [NonAction]
        public List<RSSecretariat> TelephoneDirectories()
        {
            List<RSSecretariat> list = new List<RSSecretariat>();
            RSSecretariat rssecretariat = new RSSecretariat();

            rssecretariat.Type = "MembersAmenitySection";
            rssecretariat.SubType = "TelephoneDirectory";
            rssecretariat.IsPublished = true;
            rssecretariat.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRSSecretariat getRSSecretariat = new GetRSSecretariat();
            DataSet dsTelephoneDirectories = new DataSet();
            if (getRSSecretariat.GetTelephoneDirectories(ref dsTelephoneDirectories, rssecretariat, ref ErrorMsg))
            {
                if (dsTelephoneDirectories.Tables[0].Rows.Count > 0)
                {
                    RSSecretariat objRSSecretariat;
                    for (int i = 0; i < dsTelephoneDirectories.Tables[0].Rows.Count; i++)
                    {
                        objRSSecretariat = new RSSecretariat()
                        {
                            Title = Convert.ToString(dsTelephoneDirectories.Tables[0].Rows[i]["Tiltle"]),
                            FileName = Convert.ToString(dsTelephoneDirectories.Tables[0].Rows[i]["Name"]),
                            FileType = Convert.ToString(dsTelephoneDirectories.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsTelephoneDirectories.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = RSSFilesPath + Convert.ToString(dsTelephoneDirectories.Tables[0].Rows[i]["Name"])
                        };
                        list.Add(objRSSecretariat);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "TelephoneDirectories", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "TelephoneDirectories"); 
            return list;
        }


        /// <summary>
        /// Responsible for Rajya Sabha Secretariat Demand Grants
        /// </summary>
        [NonAction]
        public List<RSSecretariat> DemandGrants()
        {
            List<RSSecretariat> list = new List<RSSecretariat>();
            RSSecretariat rssecretariat = new RSSecretariat();
            
            rssecretariat.IsPublished = true;
            rssecretariat.IsArchived = false;

            string ErrorMsg = string.Empty;


            GetRSSecretariat getRSSecretariat = new GetRSSecretariat();
            DataSet dsDemandGrants = new DataSet();
            if (getRSSecretariat.GetDemandGrants(ref dsDemandGrants, rssecretariat, ref ErrorMsg))
            {
                if (dsDemandGrants.Tables[0].Rows.Count > 0)
                {
                    RSSecretariat objRSSecretariat;
                    for (int i = 0; i < dsDemandGrants.Tables[0].Rows.Count; i++)
                    {
                        objRSSecretariat = new RSSecretariat()
                        {
                            Title = Convert.ToString(dsDemandGrants.Tables[0].Rows[i]["Title"]),
                            FileName = Convert.ToString(dsDemandGrants.Tables[0].Rows[i]["FileName"]),
                            FileType = Convert.ToString(dsDemandGrants.Tables[0].Rows[i]["FileType"]),
                            FileSize = "(" + FileSizeCalculator.FileSize(dsDemandGrants.Tables[0].Rows[i]["FileSize"]) + "MB)",
                            FilePath = RSSFilesPath + Convert.ToString(dsDemandGrants.Tables[0].Rows[i]["FileName"]),
                            UploadDate = DateTime.Parse(dsDemandGrants.Tables[0].Rows[i]["UploadDate"].ToString()).ToString(@dateFormat),
                            FinancialYear = Convert.ToString(dsDemandGrants.Tables[0].Rows[i]["FinancialYear"])
                        };
                        list.Add(objRSSecretariat);
                    }
                }
            }
            else if (ErrorMsg != "" && ErrorMsg != "SUCCESS")
            {
                LogGenerator.GenerateLog(ErrorMsg, "DemandGrants", "");
            }
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "DemandGrants");
            return list;
        }


        [NonAction]
        public List<RSSecretariat> Default()
        {
            TempData["BreadCrumb"] = BreadCrumbSetter.GetBreadCrumb("RajyaSabhaSecretariat", "Index"); 
            return new List<RSSecretariat>();
        }

        #region RAJYA SABHA SECRETARIAT
        // GET: Secretary General
        public ActionResult SecretaryGeneral()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetSecretryGeneral";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["SGeneral"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Secretary
        public ActionResult Secretary()
        {
            List<GetSecretryViewModel> data = new List<GetSecretryViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretryViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Secretry"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Additional Secretaries
        public ActionResult Additional_Secretaries()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetAdditionalSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["AddSecretry"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Joint Secretaries
        public ActionResult Joint_Secretaries()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetJointSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["JointSec"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD LARRDIS
        public ActionResult LARRDIS()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetLardis";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Larrids"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD SIS
        public ActionResult SIS()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetSIS";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Sis"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD E&T
        public ActionResult ENT()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetENT";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Ent"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD Verbatim Reporting
        public ActionResult Verbatim_Reporting()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetVerbatim";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Verbatim"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD P&P
        public ActionResult PNP()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetPrintingPublication";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PrintingPublication"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Director/JD/DD PSS
        public ActionResult Parliament_Security()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetPSS";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PSS"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Section
        public ActionResult Sections()
        {
            List<GetSectionViewModel> data = new List<GetSectionViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/Getsection";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSectionViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Sections"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Organization Setup
        public ActionResult Organization_Setup()
        {
            SG();
            Sec();
            AddSec();
            JoinSec();
            GetorgSetup();
            return View();
        }
        // Comman method
        public void SG()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetSecretryGeneral";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["SGen"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void Sec()
        {
            List<GetSecretryViewModel> data = new List<GetSecretryViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretryViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["Sec"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void AddSec()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetAdditionalSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["AddSec"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void JoinSec()
        {
            List<GetSecretaryGeneralViewModel> data = new List<GetSecretaryGeneralViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/GetJointSecretry";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSecretaryGeneralViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["JoinSec"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public void GetorgSetup()
        {
            List<GetorgSetupViewModel> data = new List<GetorgSetupViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/RssOrgChart/Getorgsetup";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetorgSetupViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["setup"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        #endregion
    }
}