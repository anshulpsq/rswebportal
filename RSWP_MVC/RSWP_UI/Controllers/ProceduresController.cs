﻿using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class ProceduresController : BaseController
    {
        RS_DBEntities db;
        public ProceduresController()
        {
            db = new RS_DBEntities();
        }
        // GET: Procedures
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetProcedureForSubmissionUrl()
        {
            string procedureForSubmissionUrl = null;
            try
            {
                procedureForSubmissionUrl = db.Documents.Where(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.ProcedureForSubmission)
                                          && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage).FirstOrDefault()?.FileUrl;
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/GetProcedureForSubmissionUrl", line);
            }
            return Json(new { url = procedureForSubmissionUrl ?? "#" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HandBookForMembers()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                var document = db.Documents.FirstOrDefault(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.HandBookForMembers)
                                             && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage);
                if (document != null)
                {
                    model.FileUrl = document.FileUrl;
                    model.Name = document.Name;
                    model.Tiltle = document.Tiltle;
                    model.Language = document.Language;

                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/HandBookForMembers", line);
            }
            return View(model);
        }
        public ActionResult PracticeAndProcedure()
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.DocumentList = db.Documents.Where(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.PracticeAndProcedure)
                                               && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, SNo = x.SNo }).OrderBy(x => x.SNo).AsQueryable().ToList();
                if (model.DocumentList == null)
                {
                    model.DocumentList = new List<DocumentModelView>();

                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/HandBookForMembers", line);
            }
            return View(model);
        }

        public ActionResult GovernmentInstructions()
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.DocumentList = db.Documents.Where(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.GovernmentInstructions)
                                               && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, SNo = x.SNo, Subject = x.Subject }).OrderBy(x => x.SNo).AsQueryable().ToList();
                if (model.DocumentList == null)
                {
                    model.DocumentList = new List<DocumentModelView>();

                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/HandBookForMembers", line);
            }
            return View(model);
        }
        public ActionResult PrivilegeDigest()
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.DocumentList = db.Documents.Where(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.PrivilegeDigest)
                                               && t.IsArchived == false && t.isApproved == true && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, SNo = x.SNo, Subject = x.Subject, Source = x.Source }).OrderBy(x => x.SNo).AsQueryable().ToList();
                if (model.DocumentList == null)
                {
                    model.DocumentList = new List<DocumentModelView>();

                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/HandBookForMembers", line);
            }
            return View(model);
        }


        public ActionResult RulingsAndObservation()
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.DocumentList = db.Documents.Where(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.RulingsAndObservation)
                                               && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView()
                                               {
                                                   Name = x.Name,
                                                   FileSize = x.FileSize,
                                                   FileUrl = x.FileUrl,
                                                   Tiltle = x.Tiltle,
                                                   Language = x.Language,
                                                   Subject = x.Subject,
                                                   PageFrom = x.PageNoFrom,
                                                   PageTo = x.PageNoTo
                                               }).OrderBy(x => x.PageFrom).AsQueryable().ToList();
                if (model.DocumentList == null)
                {
                    model.DocumentList = new List<DocumentModelView>();

                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/RulingsAndObservation", line);
            }
            return View(model);
        }

        public ActionResult RajyaSabhaAtWork()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                var document = db.Documents.FirstOrDefault(t => t.Section.Equals(CommanConstant.Procedure) && t.SubSection.Equals(CommanConstant.RajyaSabhaAtWork)
                                             && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage);
                if (document != null)
                {
                    model.FileUrl = document.FileUrl;
                    model.Name = document.Name;
                    model.Tiltle = document.Tiltle;
                    model.Language = document.Language;
                    model.PageFrom = document.PageNoFrom;
                    model.PageTo = document.PageNoTo;
                    model.EditedBy = document.EditedBy;
                    model.FileSize = document.FileSize;
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/RajyaSabhaAtWork", line);
            }
            return View(model);
        }

        public ActionResult LegislativeRules()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                var document = db.Documents.FirstOrDefault(t => t.Section.Equals(CommanConstant.Section_Legislative) && t.SubSection.Equals(CommanConstant.SubSection_LegislativeRules)
                                             && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage);
                if (document != null)
                {
                    model.FileUrl = document.FileUrl;
                    model.Name = document.Name;
                    model.Tiltle = document.Tiltle;
                    model.Language = document.Language;
                    model.FileSize = document.FileSize;
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/LegislativeRules", line);
            }
            return View(model);
        }

        public ActionResult LegislativeDirection()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                var document = db.Documents.FirstOrDefault(t => t.Section.Equals(CommanConstant.Larrdis_Section) && t.SubSection.Equals(CommanConstant.SubSection_LegislativeDirection)
                                             && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage);
                if (document != null)
                {
                    model.FileUrl = document.FileUrl;
                    model.Name = document.Name;
                    model.Tiltle = document.Tiltle;
                    model.Language = document.Language;
                    model.FileSize = document.FileSize;
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/LegislativeDirection", line);
            }
            return View(model);
        }
        public ActionResult Archive(string section, string subSection)
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.Archive = true;
                model.Section = section;
                model.SubSection = subSection;
                if (subSection == CommanConstant.HandBookForMembers)
                    model.DocumentList = db.Documents.Where(t => t.Section.Equals(section) && t.SubSection.Equals(subSection)
                                                   && t.IsArchived == true && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, SNo = x.SNo }).OrderBy(x => x.SNo).AsQueryable().ToList();
                else if(subSection==CommanConstant.RajyaSabhaAtWork)
                    model.DocumentList = db.Documents.Where(t => t.Section.Equals(section) && t.SubSection.Equals(subSection)
                                                  && t.IsArchived == true && t.Language == LanguageMang.CurrentCultureLanguage).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, PageFrom = x.PageNoFrom,PageTo=x.PageNoTo,EditedBy=x.EditedBy }).OrderBy(x => x.PageFrom).AsQueryable().ToList();
                else
                    model.DocumentList = db.Documents.Where(t => t.Section.Equals(section) && t.SubSection.Equals(subSection)
                                                     && t.IsArchived == true && t.Language == LanguageMang.CurrentCultureLanguage).OrderByDescending(x => x.ArchivedOn).Select(x => new DocumentModelView() { Name = x.Name, FileSize = x.FileSize, FileUrl = x.FileUrl, Tiltle = x.Tiltle, Language = x.Language, PageFrom = x.PageNoFrom, PageTo = x.PageNoTo, EditedBy = x.EditedBy }).AsQueryable().ToList();

                if (model.DocumentList == null)
                {
                    model.DocumentList = new List<DocumentModelView>();
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Procedures/Archive", line);
            }
            return View(model);
        }

    }
}