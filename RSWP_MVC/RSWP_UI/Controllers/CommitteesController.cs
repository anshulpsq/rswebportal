﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.CommiteeViewModel;
using RSWP_UI.Models.MemberViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class CommitteesController : BaseController
    {
        
        // GET: Commitees
        public ActionResult Index()
        {
            return View();
        }
        #region Commitees 09 Dec 2019
        #region Comman Method For Commitees
        // Get Master Committe By 1
        public void GetMTCommittee_1()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=1";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMTCommittee_1"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Master Committe By 2
        public void GetMTCommittee_2()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=2";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMTCommittee_2"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Master Committe By 3
        public void GetMTCommittee_3()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=3";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMTCommittee_3"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Master Committe By 4
        public void GetMTCommittee_4()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=4";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMTCommittee_4"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Master Committe By 6
        public void GetMTCommittee_6()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=6";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMTCommittee_6"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Chairman
        public void GetChairMan()
        {
            List<GetChairmanViewModel> data = new List<GetChairmanViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeChairmanDetail?maincomid=" + TempData["ChairmanMainID"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetChairmanViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                if (data.Count != 0)
                {
                    foreach (var item in data)
                    {
                        TempData["ChairmanName"] = item.vchair_name;
                        TempData["ChairmanImage"] = item.chrimage;
                        TempData["Chairmanddoj"] = item.ddoj.ToString("dd-MMM-yyyy");
                        break;
                    }
                }
                else
                {
                    TempData["ChairmanName"] = "";
                    TempData["ChairmanImage"] = "ImageNotFound";
                    TempData["Chairmanddoj"] = "";
                }

                //Session["ChairmanDetails"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Introduction , Rules & ContactUs
        public void getDetailCommitee(int id)
        {
            List<GetCommiteeDetailsViewModel> data = new List<GetCommiteeDetailsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetDetails_comid?comid=" + id;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommiteeDetailsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["CommiteesValues"] = data;
                foreach (var item in data)
                {
                    TempData["ChairmanMainID"] = item.nmaincomid;
                    break;
                }
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Composition
        public void getCompositions(int nmaincomid)
        {
            List<GetCompositionsViewModel> data = new List<GetCompositionsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeMemberList_committeeWise?maincomid=" + nmaincomid;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCompositionsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                foreach (var item in data)
                {
                    TempData["GetName"] = item.vmember_name;
                    TempData["GetComName"] = item.vmaincomname;
                    TempData["GetMPCode"] = item.vmember_id;
                    break;
                }
                TempData["GetMPCount"] = data.Count;
                Session["CompositionValues"] = data;
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get More Info
        public void GetMoreInfo(int id)
        {
            List<GetMoreInfoViewModel> data = new List<GetMoreInfoViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeMoreinfo";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMoreInfoViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MoreInfoValues"] = data;
                TempData["MoreInfoId"] = id;
                TempData["MoreInfoCount"] = data.Count;
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Main Commitee Id For ID=1
        public void GetMainCommiteeId(int id)
        {
            List<GetMainCommiteeIdViewModel> data = new List<GetMainCommiteeIdViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeList_category?catid=1";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMainCommiteeIdViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MainComValue"] = data;
                foreach (var item in data)
                {
                    if (item.nMstcomid == id)
                    {
                        TempData["MainComId"] = item.nmaincomid;
                        TempData["MainMasterComId"] = item.nMstcomid;
                        break;
                    }
                }
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Main Commitee Id For ID=2
        public void GetMainCommiteeId_2(int id)
        {
            List<GetMainCommiteeIdViewModel> data = new List<GetMainCommiteeIdViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeList_category?catid=2";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMainCommiteeIdViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MainComValue_2"] = data;
                foreach (var item in data)
                {
                    if (item.nMstcomid == id)
                    {
                        TempData["MainMasterComId_2"] = item.nMstcomid;
                        break;
                    }
                }
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Committee Bills_1
        public void GetBills_1()
        {
            List<GetCommitteBillsViewModel> data = new List<GetCommitteBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetBill_pressByid?mstcomid=" + TempData["MainMasterComId"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteBillsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MainComData_1"] = data;
                TempData["MainComCount_1"] = data.Count;
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Committee Bills_2
        public void GetBills_2()
        {
            List<GetCommitteBillsViewModel> data = new List<GetCommitteBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetBill_pressByid?mstcomid=" + TempData["MainMasterComId_2"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteBillsViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MainComData_2"] = data;
                TempData["MainComCount_2"] = data.Count;
                ////return View(data);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Meeting List By Main Commitee ID
        public void GetMeetingList()
        {
            List<GetMeetingListViewModel> data = new List<GetMeetingListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMeetingListByid?comid=" + TempData["MainComId"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetMeetingListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MeetingList"] = data;
                TempData["MeetingListCount"] = data.Count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Commitee Subject
        public void GetSubject()
        {
            List<GetCommiteeSubjectViewModel> data = new List<GetCommiteeSubjectViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeSubjectByid?maincomid=" + TempData["MainComId"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommiteeSubjectViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["ConSubjectResult"] = data;
                TempData["SubjectCount"] = data.Count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Press Release
        public void GetPressRelease(int id)
        {
            List<GetPressReleaseViewModel> data = new List<GetPressReleaseViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetPress_relese_committeeByid?id=" + id;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetPressReleaseViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["PressReleaseResult"] = data;
                TempData["PressReleaseCount"] = data.Count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Committee Reopt
        public void GetReport(int id)
        {
            List<GetCommitteeReoprtViewModel> data = new List<GetCommitteeReoprtViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeReportByid?mstcomid=" + id;
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeReoprtViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["ReportResult"] = data;
                TempData["ReportCount"] = data.Count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get Committee Engagements
        public void GetTour()
        {
            List<GetCommitteeEngagementViewModel> data = new List<GetCommitteeEngagementViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetTourDetail?maincomid=" + TempData["MainComId"];
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeEngagementViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["TourResult"] = data;
                TempData["TourCount"] = data.Count;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        #endregion
        #region Standing Committee
        // Business Advisory
        public ActionResult BusinessAdvisory(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Provision of Computer Equipment to Members of Rajya Sabha
        public ActionResult ProvisionofCE(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Ethics
        public ActionResult Ethics(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // General Purposes Committee
        public ActionResult GeneralPurposesCommittee(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Government Assurances
        public ActionResult GovernmentAssurances(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // 	House Committee
        public ActionResult HouseCommittee(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // MPLADS
        public ActionResult MPLADS(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        //Papers Laid on the table
        public ActionResult PapersLaid(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Petitions
        public ActionResult Petitions(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Privileges
        public ActionResult Privileges(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Rules
        public ActionResult Rules(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        // Subordinate Legislation
        public ActionResult SubordinateLegislation(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetBills_1();
            return View();
        }
        #endregion
        #region Department Related Parliamentary Standing Committee (RS)
        // Committee Commerce
        public ActionResult Commerce(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Health and Family Welfare
        public ActionResult HFWelfare(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Home Affairs
        public ActionResult HomeAffairs(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Human Resource Development
        public ActionResult HumanResourceDevelopment(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Industry
        public ActionResult Industry(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Personnel, Public Grievances, Law and Justice
        public ActionResult PPGLJustice(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // Science and Technology, Environment, Forests and Climate Change
        public ActionResult STEFCClimatehange(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        // 	Transport, Tourism and Culture
        public ActionResult TTCulture(int id)
        {
            getDetailCommitee(id);
            getCompositions(id);
            GetMoreInfo(id);
            GetMainCommiteeId(id);
            GetMeetingList();
            GetSubject();
            GetPressRelease(id);
            GetReport(id);
            GetTour();
            GetChairMan();
            GetMainCommiteeId_2(id);
            GetBills_2();
            return View();
        }
        #endregion
        #region Select Committee
        public void SelCommitteResult(int id)
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=6";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                foreach (var item in data)
                {
                    if (item.nMstcomid == id)
                    {
                        TempData["SelectComName"] = item.vMstcomName;
                        break;
                    }
                }
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        public ActionResult SelectCommittee()
        {
            List<GetSelectCommitteListViewModel> data = new List<GetSelectCommitteListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMastercommittee?catid=6";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetSelectCommitteListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["SelectCommitteeDetails"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        [HttpPost]
        public JsonResult SelectCommitteesResult(int comId)
        {
            //TempData["SelectComName"] = SelectCom;
            SelCommitteResult(comId);

            getDetailCommitee(comId);
            getCompositions(comId);
            GetMoreInfo(comId);
            GetMainCommiteeId(comId);
            GetMeetingList();
            GetSubject();
            GetPressRelease(comId);
            GetReport(comId);
            GetTour();
            GetChairMan();
            GetBills_1();
            return Json(comId, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectCommittees()
        {
            return View();
        }
        #endregion
        #region At a Glance
        // Get General Information
        public ActionResult GeneralInforamtion()
        {
            GetMTCommittee_1();
            GetMTCommittee_2();
            GetMTCommittee_3();
            GetMTCommittee_4();
            GetMTCommittee_6();
            return View();
        }
        // Get General Information(Standing Committee)
        public void GenStandingComm(int comID)
        {
            getDetailCommitee(comID);
            getCompositions(comID);
            GetMoreInfo(comID);
            GetMainCommiteeId(comID);
            GetMeetingList();
            GetSubject();
            GetPressRelease(comID);
            GetReport(comID);
            GetTour();
            GetChairMan();
            GetBills_1();
        }
        public ActionResult GenInfoSC()
        {
            return View();
        }
        // Get General Information(Department Related Parliamentary Standing Committee (RS))
        public void GenStandingComm_RS(int comID)
        {
            getDetailCommitee(comID);
            getCompositions(comID);
            GetMoreInfo(comID);
            GetMainCommiteeId(comID);
            GetMeetingList();
            GetSubject();
            GetPressRelease(comID);
            GetReport(comID);
            GetTour();
            GetChairMan();
            GetBills_1();
        }
        public ActionResult GenInfoSC_RS()
        {
            return View();
        }
        // Get Chairman
        public ActionResult GenInfoChairman()
        {
            List<GetInfoChairmanViewModel> data = new List<GetInfoChairmanViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeChairman";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetInfoChairmanViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetInfoChairmanDetails"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        public JsonResult GetCommCategoryDetails(string GetComValue)
        {
            List<GetInfoChairmanViewModel> data = new List<GetInfoChairmanViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeChairman";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetInfoChairmanViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                //Session["GetInfoChairmanDetails"] = data;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Get Composition
        public ActionResult GenInfoComposition()
        {
            List<GetCommitteeMemberListViewModel> data = new List<GetCommitteeMemberListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeMemberList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeMemberListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetCommMemListDetails"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // Committees Report
        public ActionResult CommitteesReport()
        {
            List<GetCommitteeReportViewModel> data = new List<GetCommitteeReportViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeReport";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeReportViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetCommReportDetails"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        #endregion
        #region Other Tabs in Committees
        // Committee List
        public void GetCommitteeList()
        {
            List<GetCommitteeSubCommListViewModel> data = new List<GetCommitteeSubCommListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeSubCommListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["GetCommList"] = new SelectList(data, "nMstcomid", "vmaincomname");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Member List
        public void GetMemberList()
        {
            List<GetCommMemberListViewModel> data = new List<GetCommMemberListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeMemberList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommMemberListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["GetCommMemList"] = new SelectList(data, "nmemid", "vmember_name");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Party DropDown
        public void Party()
        {
            List<GetPartyViewModel> data = new List<GetPartyViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetPartlist";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetPartyViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["GetPartyList"] = new SelectList(data, "partycount", "PARTY_CODE");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State DropDown
        public void State()
        {
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                TempData["GetStateList"] = new SelectList(data, "STATE_COUNT", "STATE_NAME");
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // Get all MemberSearch Result
        public void GetAllMemberSearch()
        {
            List<GetCommitteeMemberListViewModel> data = new List<GetCommitteeMemberListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetCommitteeMemberList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeMemberListViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetAllMemberSearch"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        //Committe Member Search
        public ActionResult MemberSearch()
        {
            GetCommitteeList();
            GetMemberList();
            Party();
            State();
            return View();
        }
        public ActionResult MemberSearhResult()
        {
            GetAllMemberSearch();
            return View();
        }
        // Get Date Wise Committee Meeting Schedule
        public JsonResult DateWise_Result(string dt1,string dt2,string ddlval)
        {
            List<GetCommitteeMeetingViewModel> data = new List<GetCommitteeMeetingViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/committee_web/GetMeetingList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetCommitteeMeetingViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetMeetCommMeetList"] = data;
                TempData["Date1"] = dt1;
                TempData["Date2"] = dt2;
                TempData["SelComm"] = ddlval;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // Committe Meeting Schedule
        public ActionResult MeetingSchedule()
        {
            GetCommitteeList();
            return View();
        }
        // Committee Meeting Schedule Result
        public ActionResult MeetingScheduleResult()
        {
            return View();
        }
        #endregion

        #endregion

        public ActionResult GetCommitteeIntroductionUrl()
        {
            string introductionUrl = null;
            try
            {
                using (RS_DBEntities db = new RS_DBEntities())
                {
                    introductionUrl = db.Documents.Where(t => t.Section.Equals(CommanConstant.Committee) && t.SubSection.Equals(CommanConstant.Introduction)
                                              && t.IsArchived == false &&t.Language==LanguageMang.CurrentCultureLanguage).FirstOrDefault()?.FileUrl;
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went Wrong";
            }
            return Json(new { IntroductionUrl = introductionUrl ?? "#" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SummaryOFWork(bool archive=false)
        {
            DucumentListVM model = new DucumentListVM();
            try
            {
                model.Archive = archive;
                using (RS_DBEntities db = new RS_DBEntities())
                {
                    if (archive == false)
                    {
                        DocumentModelView document = db.Documents.Where(t => t.Section.Equals(CommanConstant.Committee) && t.SubSection.Equals(CommanConstant.SummaryOfWork)
                                                   && t.IsArchived == false && t.Language == LanguageMang.CurrentCultureLanguage && t.isPublished == true).Select(x => new DocumentModelView() { Name = x.Name, isPublished = x.isPublished, Language = x.Language, Tiltle = x.Tiltle, FileSize = x.FileSize, FileUrl = x.FileUrl, startDate = x.startDate, endDate = x.endDate }).FirstOrDefault();
                        if (document != null)
                        {
                            model.DocumentList.Add(document);
                        }
                    }
                    else
                    {
                        model.DocumentList= db.Documents.Where(t => t.Section.Equals(CommanConstant.Committee) && t.SubSection.Equals(CommanConstant.SummaryOfWork)
                                                   && t.IsArchived == false && t.isApproved==false&&t.isPublished==false && t.Language == LanguageMang.CurrentCultureLanguage ).Select(x => new DocumentModelView() { Name = x.Name, isPublished = x.isPublished, Language = x.Language, Tiltle = x.Tiltle, FileSize = x.FileSize, FileUrl = x.FileUrl, startDate = x.startDate, endDate = x.endDate }).AsQueryable().ToList();

                    }
                    if (model.DocumentList == null)
                        model.DocumentList = new List<DocumentModelView>();
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Committees/SummaryOFWork", line);
            }
            return View(model);
        }
    }
}