﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class DebatesController : BaseController
    {
        // GET: Debates
        RS_DBEntities db;
        public DebatesController()
        {
            db = new RS_DBEntities();
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}

        #region Official Debates
        public ActionResult OfficialDebatesDateWise()
        {
            OfficialDebatesListVM model = new OfficialDebatesListVM();
            try
            {
                model.Sessions = GetDebatesSessionList(CommanConstant.Debates, CommanConstant.OfficialDebatesDatewise);
                if (model.OfficialDebatesList == null)
                    model = new OfficialDebatesListVM();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/OfficialDebatesDateWise", line);
            }
            return View(model);
        }
        public ActionResult GetOfficialDebatesGridData(int sessionNo, DateTime? selectedDate)
        {
            OfficialDebatesListVM model = new OfficialDebatesListVM();
            try
            {
                model.OfficialDebatesList = GetOfficialDebatesData(sessionNo, selectedDate);
                if (model.OfficialDebatesList == null)
                    model.OfficialDebatesList = new List<OfficialDebatesViewModel>();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetOfficialDebatesGridData", line);
            }
            return PartialView("_officialDebatesDatewiseGrid", model);
        }

        private List<OfficialDebatesViewModel> GetOfficialDebatesData(int sessionNo, DateTime? selectedDate)
        {
            List<OfficialDebatesViewModel> model = new List<OfficialDebatesViewModel>();
            try
            {
                List<Document> documentList = new List<Document>();
                if (selectedDate == null)
                {
                    documentList = db.Documents.Where(x => x.Section == CommanConstant.Debates && x.SubSection == CommanConstant.OfficialDebatesDatewise && x.session == sessionNo.ToString() && x.IsArchived != true).ToList();
                }
                else
                {
                    documentList = db.Documents.Where(x => x.Section == CommanConstant.Debates && x.SubSection == CommanConstant.OfficialDebatesDatewise && x.session == sessionNo.ToString() && x.selectedDate == selectedDate && x.IsArchived != true).ToList();
                }
                var data = documentList.GroupBy(x => x.selectedDate).ToList();
                foreach (var item in data)
                {
                    OfficialDebatesViewModel innerData = new OfficialDebatesViewModel();
                    foreach (var innerItem in item)
                    {
                        innerData.SelectedDate = innerItem.selectedDate.Value;
                        innerData.SessionNo = Convert.ToInt32(innerItem.session);
                        if (innerItem.Version == Common.CommanConstant.Hindi)
                        {
                            innerData.HindiVersionDocument.Name = innerItem.Name;
                            innerData.HindiVersionDocument.session = innerItem.session;
                            innerData.HindiVersionDocument.selectedDate = innerItem.selectedDate;
                            innerData.HindiVersionDocument.FileUrl = innerItem.FileUrl;
                            innerData.HindiVersionDocument.Version = innerItem.Version;
                            innerData.HindiVersionDocument.FileSize = innerItem.FileSize;
                        }
                        else
                        {
                            innerData.FloorVersionDocument.Name = innerItem.Name;
                            innerData.FloorVersionDocument.session = innerItem.session;
                            innerData.FloorVersionDocument.selectedDate = innerItem.selectedDate;
                            innerData.FloorVersionDocument.FileUrl = innerItem.FileUrl;
                            innerData.FloorVersionDocument.Version = innerItem.Version;
                            innerData.FloorVersionDocument.FileSize = innerItem.FileSize;
                        }

                    }
                    model.Add(innerData);
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetOfficialDebatesGridData", line);
            }

            return model;
        }
        #endregion

        #region Verbatim Debates
        public ActionResult VerbatimDebates()
        {
            VerbatimDebatesViewModel model = new VerbatimDebatesViewModel();
            try
            {
                model.Sessions = GetDebatesSessionList(CommanConstant.Debates, CommanConstant.VerbatimDebates);
                if (model.Sessions != null && model.Sessions.Count > 0)
                {
                    int latestSession = model.Sessions.Max(x => Convert.ToInt32(x.Value));
                    var result = GetDebatesSittingDatesList(latestSession.ToString(), CommanConstant.Debates, CommanConstant.VerbatimDebates).Data;
                    model.Dates = JsonConvert.DeserializeObject<List<SelectListItem>>(JsonConvert.SerializeObject(result));
                    DateTime? maxDate = model.Dates.Max(x => DateTime.ParseExact(x.Value, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    model.VerbatimDebatesList = GetVerbatimDebatesData(latestSession, maxDate);
                    model.SelectedDate = maxDate.Value;
                }

                if (model.VerbatimDebatesList == null)
                    model.VerbatimDebatesList = new List<DocumentModelView>();

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/VerbatimDebates", line);
            }
            return View(model);
        }
        public ActionResult GetVerbatimDebatesGridData(int sessionNo, DateTime? selectedDate)
        {
            VerbatimDebatesViewModel model = new VerbatimDebatesViewModel();
            try
            {
                model.VerbatimDebatesList = GetVerbatimDebatesData(sessionNo, selectedDate);
                model.SelectedDate = selectedDate.Value;
                if (model.VerbatimDebatesList == null)
                    model.VerbatimDebatesList = new List<DocumentModelView>();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetVerbatimDebatesGridData", line);
            }
            return PartialView("_verbatimDebatesGrid", model);
        }

        private List<DocumentModelView> GetVerbatimDebatesData(int sessionNo, DateTime? selectedDate)
        {
            List<DocumentModelView> model = new List<DocumentModelView>();
            try
            {

                if (selectedDate == null)
                {
                    model = db.Documents.Where(x => x.Section == CommanConstant.Debates && x.SubSection == CommanConstant.VerbatimDebates && x.session == sessionNo.ToString() && x.IsArchived != true && x.isApproved == true)
                        .Select(x => new DocumentModelView() { Id = x.Id, session = x.session, selectedDate = x.selectedDate, FileUrl = x.FileUrl, FileSize = x.FileSize, Name = x.Name, FileType = x.FileType, Time = x.Time }).ToList();
                }
                else
                {
                    model = db.Documents.Where(x => x.Section == CommanConstant.Debates && x.SubSection == CommanConstant.VerbatimDebates && x.session == sessionNo.ToString() && x.selectedDate == selectedDate && x.IsArchived != true && x.isApproved == true)
                       .Select(x => new DocumentModelView() { Id = x.Id, session = x.session, selectedDate = x.selectedDate, FileUrl = x.FileUrl, FileSize = x.FileSize, Name = x.Name, FileType = x.FileType, Time = x.Time }).ToList();
                }


            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetVerbatimDebatesData", line);
            }

            return model;
        }
        #endregion

        #region common
        public List<SelectListItem> GetDebatesSessionList(string section, string subSection)
        {
            List<SelectListItem> sessionList = new List<SelectListItem>();
            try
            {
                List<string> sessions = new List<string>();
                if (subSection != CommanConstant.Discussions)
                {
                    List<Document> docList = new List<Document>();
                    if (subSection != CommanConstant.VerbatimDebates)
                    {
                        docList = db.Documents.Where(x => x.IsArchived != true &&
                    x.Section == section && x.SubSection == subSection).OrderByDescending(x => x.session).AsQueryable().ToList();
                    }
                    else
                    {
                        //VERBATIM DEBATE
                        docList = db.Documents.Where(x => x.IsArchived != true && x.isApproved == true &&
                    x.Section == section && x.SubSection == subSection).OrderByDescending(x => x.session).AsQueryable().ToList();

                    }
                   
                }
                else
                {
                    //Discussion case
                    var discussionList = db.Discussions.Where(x => x.IsDeleted == false&&x.Language==LanguageMang.CurrentCultureLanguage).OrderByDescending(x => x.SessionNo).AsQueryable().ToList();
                    sessions = discussionList.Select(x => x.SessionNo.ToString()).Distinct().ToList();
                }
               
                sessionList = sessions.Select(x => new SelectListItem()
                {
                    Text = x,
                    Value = x
                }).ToList();
                if (sessionList == null)
                    sessionList = new List<SelectListItem>();
                if (subSection != CommanConstant.VerbatimDebates)
                    sessionList.Insert(0, new SelectListItem() { Text = Resource.PleaseSelect, Value = "" });
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetDebatesSessionList", line);
            }
            return sessionList;
        }

        public JsonResult GetDebatesSittingDatesList(string sessionNo, string section, string subSection)
        {
            List<SelectListItem> sittingDatesList = new List<SelectListItem>();
            try
            {
                List<DateTime?> dates = new List<DateTime?>();
                if (subSection != CommanConstant.Discussions)
                {
                    List<Document> docList = new List<Document>();
                    if (subSection != CommanConstant.VerbatimDebates)
                    {
                        docList = db.Documents.Where(x => x.IsArchived != true && x.session == sessionNo &&
                       x.Section == section && x.SubSection == subSection)
                        .OrderByDescending(x => x.selectedDate).AsQueryable().ToList();
                    }
                    else
                    {
                        //VERBATIM DEBATE
                        docList = db.Documents.Where(x => x.IsArchived != true && x.isApproved == true && x.session == sessionNo &&
                      x.Section == section && x.SubSection == subSection)
                       .OrderByDescending(x => x.selectedDate).AsQueryable().ToList();
                    }
                    if (docList == null)
                        docList = new List<Document>();

                    dates = docList.Select(x => x.selectedDate).Distinct().ToList();
                }
                else
                {
                    //Discussions case 
                    int session = Convert.ToInt32(sessionNo);
                    var discussionList = db.Discussions.Where(x => x.SessionNo == session && x.IsDeleted == false && x.Language == LanguageMang.CurrentCultureLanguage)
                        .OrderByDescending(x => x.SelectedDate).AsQueryable().ToList();

                    dates = discussionList.Select(x => (DateTime?)x.SelectedDate).Distinct().ToList();
                }
                sittingDatesList = dates.Select(x => new SelectListItem()
                {
                    Text = x.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                    Value = x.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                }).ToList();


                if (sittingDatesList == null)
                    sittingDatesList = new List<SelectListItem>();
                if (subSection != CommanConstant.VerbatimDebates)
                    sittingDatesList.Insert(0, new SelectListItem() { Text = Resource.PleaseSelect, Value = "" });

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetDebatesSessionList", line);
            }
            return Json(sittingDatesList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Discussion
        public ActionResult Discussion()
        {
            DiscussionsListVM model = new DiscussionsListVM();
            try
            {
                model.SessionList = GetDebatesSessionList(CommanConstant.Debates,CommanConstant.Discussions);
                
                model.FirstMemberList = GetFirstMemberListList();
                model.TypeList = GetDiscussionTypesList();
                if (model.SessionList == null)
                    model.SessionList = new List<SelectListItem>();
                if (model.TypeList == null)
                    model.TypeList = new List<SelectListItem>();
                if (model.FirstMemberList == null)
                    model.FirstMemberList = new List<SelectListItem>();
                if (model.AssociatedMemberList == null)
                    model.AssociatedMemberList = new List<SelectListItem>();
                AdvanceSearchViewModel filter = new AdvanceSearchViewModel();
                filter.DisplayAllRecords = true;
                model.DiscussionsList = GetDiscussionsData(filter);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/Discussion", line);

            }
            return View(model);
        }

        private List<DiscussionsViewModel> GetDiscussionsData(AdvanceSearchViewModel filter)
        {
            List<DiscussionsViewModel> discussionsData = new List<DiscussionsViewModel>();
            try
            {
                using (RS_DBEntities db = new RS_DBEntities())
                {
                    SqlParameter paramDisplayRecord = new SqlParameter("@DisplayAllRecords", filter.DisplayAllRecords);
                    SqlParameter paramLanguage = new SqlParameter("@Language", (object)LanguageMang.CurrentCultureLanguage ?? DBNull.Value);
                    SqlParameter paramSessionNo = new SqlParameter("@SessionNo", (object)filter.SessionNo ?? DBNull.Value);
                    SqlParameter paramFromDate = new SqlParameter("@FromDate", (object)filter.From ?? DBNull.Value);
                    SqlParameter paramToDate = new SqlParameter("@ToDate", (object)filter.To ?? DBNull.Value);

                    SqlParameter paramDiscussionType = new SqlParameter("@DiscussionType", (object)filter.DiscussionType ?? DBNull.Value);
                    paramDiscussionType.SqlDbType = System.Data.SqlDbType.NVarChar;
                    SqlParameter paramTitleToSearch = new SqlParameter("@TitleToSearch", (object)filter.SearchTitle ?? DBNull.Value);
                    paramTitleToSearch.SqlDbType = System.Data.SqlDbType.NVarChar;
                    paramTitleToSearch.Size = -1;//Max

                    SqlParameter paramAllMember = new SqlParameter("@AllMember", (object)filter.AllMember ?? DBNull.Value);
                    paramAllMember.SqlDbType = System.Data.SqlDbType.NVarChar;
                    paramAllMember.Size = -1;//Max

                    discussionsData = db.Database.SqlQuery<DiscussionsViewModel>("sp_GetDiscussionsRecords @DisplayAllRecords,@Language,@SessionNo,@FromDate,@ToDate,@TitleToSearch,@DiscussionType,@AllMember",
                         paramDisplayRecord, paramLanguage, paramSessionNo, paramFromDate, paramToDate, paramTitleToSearch, paramDiscussionType, paramAllMember).ToList();
                    if (discussionsData == null)
                        discussionsData = new List<DiscussionsViewModel>();
                    else
                    {
                        discussionsData.ForEach(x => x.SelectedAssociatedMember = string.Join(",", db.AssociatedMembers.Where(y => y.DiscussionId == x.Id && y.IsDeleted == false).Select(y => y.AssociatedMemberName).ToList()));

                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetDiscussionsData", line);

            }
            return discussionsData;
        }

        public ActionResult GetDiscussionsGridData(AdvanceSearchViewModel filterModel)
        {
            DiscussionsListVM model = new DiscussionsListVM();
            try
            {
                model.DiscussionsList = GetDiscussionsData(filterModel);
                if (model.DiscussionsList == null)
                    model.DiscussionsList = new List<DiscussionsViewModel>();
            }
            catch (Exception ex)
            {
                // model.Message = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetDiscussionsGridData", line);
            }
            return PartialView("_DiscussionGridView", model);
        }

        private List<SelectListItem> GetDiscussionTypesList()
        {
            List<SelectListItem> typeList = new List<SelectListItem>();
            try
            {
                typeList.Add(new SelectListItem() { Text = CommanConstant.CallingAttention, Value = CommanConstant.CallingAttention });
                typeList.Add(new SelectListItem() { Text = CommanConstant.ShortDurationDiscussion, Value = CommanConstant.ShortDurationDiscussion });
                typeList.Add(new SelectListItem() { Text = CommanConstant.MattersRaisedWithPermission_ZeroHour, Value = CommanConstant.MattersRaisedWithPermission_ZeroHour });
                typeList.Add(new SelectListItem() { Text = CommanConstant.PrivateMembersResolution, Value = CommanConstant.PrivateMembersResolution });
                typeList.Insert(0, new SelectListItem() { Text = Resource.PleaseSelect, Value = "" });
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetDiscussionTypesList", line);

            }
            return typeList;
        }

        private List<SelectListItem> GetFirstMemberListList()
        {
            List<SelectListItem> firstMemberList = new List<SelectListItem>();
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string apiUrlGetSectionName = ConfigurationManager.AppSettings["ApiUrl"] + "MemberGetData/GetMemberAll";

                HttpResponseMessage response = client.GetAsync(apiUrlGetSectionName).Result;
                if (response != null && response.IsSuccessStatusCode)
                {
                    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(response.Content.ReadAsStringAsync().Result, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    if (data != null)
                    {
                        if (LanguageMang.CurrentCultureLanguage == CommanConstant.English)
                            firstMemberList = data.Select(x => new SelectListItem() { Text = x.MP_NAME, Value = x.MP_NAME }).ToList();//English
                        else
                            firstMemberList = data.Select(x => new SelectListItem() { Text = x.hMP_NAME, Value = x.hMP_NAME }).ToList();//Hindi
                    }
                    if (firstMemberList == null)
                        firstMemberList = new List<SelectListItem>();
                    firstMemberList.Insert(0, new SelectListItem() { Text = CommanConstant.PleaseSelect, Value = "" });

                }

            }
            catch (Exception ex)
            {

                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Debates/GetFirstMemberListList", line);
            }
            return firstMemberList;
        }

       
        #endregion
    }
}