﻿using Newtonsoft.Json;
using RSWP_BAL.Repository;
using RSWP_UI.Common;
using RSWP_UI.Models;
using RSWP_UI.Models.MemberViewModel;
using RSWP_UI.Models.TodayInRSViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace RSWP_UI.Controllers
{
    [WebLinksFilter]
    public class TodayController : BaseController
    {
        RS_DBEntities db;
        public TodayController()
        {
            db = new RS_DBEntities();
        }
        // GET: TodayRS
        public ActionResult Index()
        {
            return View();
        }
        #region API Integration for Committe Meeting 
        // GET: Get All Meeting List Method
        public void GetMeetingList()
        {
            List<AllCommitteeMeetingListViewModel> data = new List<AllCommitteeMeetingListViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseTerm = client.GetAsync("api_new/committee_web/GetMeetingList").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<AllCommitteeMeetingListViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["AllMeetingList"] = data;
                Session["MeetingList"] = data.GroupBy(l => l.comName)
                      .Select(g => g.FirstOrDefault())
                      .ToList();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // GET: Today
        public ActionResult MeetingList_Today()
        {
            GetMeetingList();
            return View();
        }
        // GET: This Week
        public ActionResult MeetingList_ThisWeek()
        {
            GetMeetingList();
            return View();
        }
        // GET: This Month
        public ActionResult MeetingList_ThisMonth()
        {
            GetMeetingList();
            return View();
        }
        #region Member Birthday
        // Party DropDown
        public void Party()
        {
            List<GetPartyViewModel> data = new List<GetPartyViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetPartlist";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetPartyViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["party"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State DropDown
        public void State()
        {
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["state"] = data;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
        }
        // State DropDown Count Vac.
        public int StateCountVac(string name)
        {
            int result = 0;
            List<GetStateViewModel> data = new List<GetStateViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "Api_new/MemberGetdata/GetStateList";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetStateViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                foreach (var item in data)
                {
                    if (item.STATE_NAME == name)
                    {
                        result = item.NO_SEATS;
                        break;
                    }
                }
                return result;
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return result;
        }
        [HttpPost]
        public JsonResult Alphabetical_Result()
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                //return View(data);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult StateWise_Result(string statecode)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                TempData["Ex"] = StateCountVac(statecode);
                if (statecode == "--Please Select--")
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(data.Where(item => item.STATE_NAME == statecode), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult PartyWise_Result(string partycode)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MemberDetails"] = data;
                if (partycode == "--Please Select--")
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(data.Where(item => item.PARTY_CODE == partycode), JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DOBMonth_Result(string month)
        {
            List<GetAlphabeticalMembersViewModel> data = new List<GetAlphabeticalMembersViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //API Used of Term 1
            var get_url = "api_new/MemberGetData/getmemberall";
            HttpResponseMessage responseTerm1 = client.GetAsync(get_url).Result;
            if (responseTerm1.IsSuccessStatusCode)
            {
                //ViewBag.result = response.Content.ReadAsAsync<IEnumerable<>>().Result;
                ViewBag.resultTerm1 = responseTerm1.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetAlphabeticalMembersViewModel>>(ViewBag.resultTerm1, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["MonthResult"] = data;
                TempData["month"] = month;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Today Member Birthday
        public ActionResult BirthdayToday()
        {
            return View();
        }
        // GET: This Month Member Birthday
        public ActionResult BirthdayList()
        {
            Party();
            State();
            return View();
        }
        public ActionResult BirthdaysList()
        {
            Party();
            State();
            return View();
        }
        #endregion
        // GET: Government Bills
        public ActionResult Government_Bills()
        {
            List<GetGovBillsViewModel> data = new List<GetGovBillsViewModel>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(CommanConstant.urlContent);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseTerm = client.GetAsync("api_new/bill/GetBillInfo").Result;
            if (responseTerm.IsSuccessStatusCode)
            {
                ViewBag.resultTerm = responseTerm.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<GetGovBillsViewModel>>(ViewBag.resultTerm, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                Session["GetGovBills"] = data;
                return View();
            }
            else
            {
                ViewBag.resultTerm1 = "Error";
            }
            return View();
        }
        // GET: Private Members Bills
        public ActionResult Private_Bills()
        {
            return View();
        }
        #endregion

        public ActionResult TodayInGlance()
        {
            TodayAtGlanceViewModel model = new TodayAtGlanceViewModel();
            try
            {
                //Current Day
                model.TodayAtGlanceDocumentList.Add(CommanConstant.ListOfBusiness + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBusiness, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.PaperToBeLaid + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.PaperLaidInRajyaSabha, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.TableBulletin1 + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBulletin1, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Bulletin2 + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.Bulletin2, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Synopsis + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.Synopsis, CommanConstant.SynopsisUpload, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnCorrectedDebates + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.Debates, CommanConstant.VerbatimDebates, CommanConstant.Current));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.StarredQuestionList + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Current, CommanConstant.Starred));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnStarredQuestionList + "_" + CommanConstant.Current, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Current, CommanConstant.UnStarred));
                //End

                //Next Day
                model.TodayAtGlanceDocumentList.Add(CommanConstant.ListOfBusiness + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBusiness, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.PaperToBeLaid + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.PaperLaidInRajyaSabha, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.TableBulletin1 + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBulletin1, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Bulletin2 + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.Bulletin2, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Synopsis + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.Synopsis, CommanConstant.SynopsisUpload, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnCorrectedDebates + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.Debates, CommanConstant.VerbatimDebates, CommanConstant.Next));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.StarredQuestionList + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Next, CommanConstant.Starred));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnStarredQuestionList + "_" + CommanConstant.Next, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Next, CommanConstant.UnStarred));
                //End

                //Previous Day
                model.TodayAtGlanceDocumentList.Add(CommanConstant.ListOfBusiness + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBusiness, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.PaperToBeLaid + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.PaperLaidInRajyaSabha, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.TableBulletin1 + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.TableBulletin1, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Bulletin2 + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.TableOffice, CommanConstant.Bulletin2, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.Synopsis + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.Synopsis, CommanConstant.SynopsisUpload, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnCorrectedDebates + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.Debates, CommanConstant.VerbatimDebates, CommanConstant.Previous));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.StarredQuestionList + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Previous, CommanConstant.Starred));
                model.TodayAtGlanceDocumentList.Add(CommanConstant.UnStarredQuestionList + "_" + CommanConstant.Previous, GetDocumentForTodayAtGlance(CommanConstant.Questions, CommanConstant.QuestionsList, CommanConstant.Previous, CommanConstant.UnStarred));
                //End

            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/TodayInGlance", line);
            }
            return View(model);
        }

        public DocumentModelView GetDocumentForTodayAtGlance(string section, string subection, string type, string fileTypeForQuestion = null)
        {
            DocumentModelView model = new DocumentModelView();
            Document fetchedRecord = new Document();
            DateTime currentDay = DateTime.Now.Date;
            DateTime nextDay = DateTime.Now.AddDays(1).Date;
            DateTime previousDay = DateTime.Now.AddDays(-1).Date;
            if (type == CommanConstant.Current)
            {
                //current day
                if (section == CommanConstant.Questions && subection == CommanConstant.QuestionsList)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.FileType == fileTypeForQuestion && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == currentDay);

                }
                else if (section == CommanConstant.Debates && subection == CommanConstant.VerbatimDebates)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.selectedDate == currentDay);

                }
                else if (subection == CommanConstant.TableBulletin1 || subection == CommanConstant.TableBusiness || subection == CommanConstant.SynopsisUpload)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == currentDay);

                }
                else
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == currentDay);

                }
            }
            else if (type == CommanConstant.Next)
            {
                //next day
                if (section == CommanConstant.Questions && subection == CommanConstant.QuestionsList)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.FileType == fileTypeForQuestion && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == nextDay);
                }
                else if (section == CommanConstant.Debates && subection == CommanConstant.VerbatimDebates)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.selectedDate == nextDay);

                }
                else if (subection == CommanConstant.TableBulletin1 || subection == CommanConstant.TableBusiness || subection == CommanConstant.SynopsisUpload)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == nextDay);

                }
                else
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == nextDay);
                }
            }
            else
            {
                //previous day
                if (section == CommanConstant.Questions && subection == CommanConstant.QuestionsList)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.FileType == fileTypeForQuestion && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == previousDay);
                }
                else if (section == CommanConstant.Debates && subection == CommanConstant.VerbatimDebates)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.selectedDate == previousDay);

                }
                else if (subection == CommanConstant.TableBulletin1 || subection == CommanConstant.TableBusiness || subection == CommanConstant.SynopsisUpload)
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.isApproved == true && x.isPublished == true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == previousDay);
                }
                else
                {
                    fetchedRecord = db.Documents.FirstOrDefault(x => x.Section == section && x.SubSection == subection && x.IsArchived != true && x.Language == LanguageMang.CurrentCultureLanguage && x.selectedDate == previousDay);
                }
            }

            if (fetchedRecord != null)
            {
                model.Name = fetchedRecord.Name;
                model.selectedDate = fetchedRecord.selectedDate;
                model.FileUrl = fetchedRecord.FileUrl;
                model.FileSize = fetchedRecord.FileSize;
                model.session = fetchedRecord.session;
            }
            return model;
        }


        private DocumentModelView GetLatestDocumentForTodayInRS(string section, string subSection,bool isApprovedDocumentRequired=false,string fileTypeForQuestion=null)
        {
            DocumentModelView latestDocument = new DocumentModelView();
            try
            {
                if (subSection == CommanConstant.VerbatimDebates)
                {
                    //then no filter of language
                    latestDocument = db.Documents.Where(x => x.Section == section && x.SubSection == subSection  && x.IsArchived != true).OrderByDescending(x => x.selectedDate).Select(x =>
                        new DocumentModelView()
                        {
                            Name = x.Name,
                            FileUrl = x.FileUrl,
                            FileSize = x.FileSize,
                            selectedDate = x.selectedDate,
                            session = x.session,
                             Section = x.Section,
                            SubSection = x.SubSection
                        }).FirstOrDefault();
                }
                else if(section==CommanConstant.Questions&&subSection==CommanConstant.QuestionsList)
                {
                    latestDocument = db.Documents.Where(x => x.Section == section && x.SubSection == subSection &&x.FileType==fileTypeForQuestion&& x.Language == LanguageMang.CurrentCultureLanguage && x.IsArchived != true && x.isApproved == true && x.isPublished == true).OrderByDescending(x => x.selectedDate).Select(x =>
                          new DocumentModelView()
                          {
                              Name = x.Name,
                              FileUrl = x.FileUrl,
                              FileSize = x.FileSize,
                              selectedDate = x.selectedDate,
                              session = x.session,
                              Section = x.Section,
                              SubSection = x.SubSection
                          }).FirstOrDefault();
                }
                else if(isApprovedDocumentRequired)
                {
                    latestDocument = db.Documents.Where(x => x.Section == section && x.SubSection == subSection && x.Language == LanguageMang.CurrentCultureLanguage && x.IsArchived != true&&x.isApproved==true &&x.isPublished ==true).OrderByDescending(x => x.selectedDate).Select(x =>
                      new DocumentModelView()
                      {
                          Name = x.Name,
                          FileUrl = x.FileUrl,
                          FileSize = x.FileSize,
                          selectedDate = x.selectedDate,
                          session = x.session,
                          Section=x.Section,
                          SubSection=x.SubSection
                      }).FirstOrDefault();
                }
                else
                {
                    latestDocument = db.Documents.Where(x => x.Section == section && x.SubSection == subSection && x.Language == LanguageMang.CurrentCultureLanguage && x.IsArchived != true).OrderByDescending(x => x.selectedDate).Select(x =>
                     new DocumentModelView()
                     {
                         Name = x.Name,
                         FileUrl = x.FileUrl,
                         FileSize = x.FileSize,
                         selectedDate = x.selectedDate,
                         session = x.session,
                         Section = x.Section,
                         SubSection = x.SubSection
                     }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if(latestDocument==null)
            {
                latestDocument = new DocumentModelView();
            }
            return latestDocument;
        }
        public ActionResult BusinessList()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.TableOffice,CommanConstant.TableBusiness,true);
            }
            catch(Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/BusinessList", line);

            }
            return View(model);
        }
        public ActionResult PaperToBeLaid()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.TableOffice, CommanConstant.PaperLaidInRajyaSabha, false);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/PaperToBeLaid", line);

            }
            return View(model);
        }
        public ActionResult BulletinOne()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.TableOffice, CommanConstant.TableBulletin1, true);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/BulletinOne", line);

            }
            return View(model);
        }
        public ActionResult BulletinTwo()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.TableOffice, CommanConstant.Bulletin2, false);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/BulletinTwo", line);

            }
            return View(model);
        }
        public ActionResult Synopis()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.Synopsis, CommanConstant.SynopsisUpload, true);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/Synopis", line);

            }
            return View(model);
        }
        public ActionResult UnCorrectedDebates()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.Debates, CommanConstant.VerbatimDebates, true);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/UnCorrectedDebates", line);

            }
            return View(model);
        }
        //public ActionResult CommitteeReport()
        //{
        //    //DocumentModelView model = new DocumentModelView();
        //    //try
        //    //{
        //    //    model = GetLatestDocumentForTodayInRS(CommanConstant.TableOffice, CommanConstant.TableBusiness, true);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    TempData["Message"] = "Something went wrong";
        //    //    var st = new StackTrace(ex, true);
        //    //    var frame = st.GetFrame(0);
        //    //    var line = frame.GetFileLineNumber().ToString();
        //    //    LogGenerator.GenerateLog(ex.Message, "Today/BusinessList", line);

        //    //}
        //    //return View(model);
        //}
        public ActionResult Starred()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.Questions, CommanConstant.QuestionsList, true,CommanConstant.Starred);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/Starred", line);

            }
            return View(model);
        }
        public ActionResult UnStarred()
        {
            DocumentModelView model = new DocumentModelView();
            try
            {
                model = GetLatestDocumentForTodayInRS(CommanConstant.Questions, CommanConstant.QuestionsList, true, CommanConstant.UnStarred);
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Something went wrong";
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber().ToString();
                LogGenerator.GenerateLog(ex.Message, "Today/UnStarred", line);

            }
            return View(model);
        }
    }
}